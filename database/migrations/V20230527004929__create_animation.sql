create table animation(
    id text primary key,
    name text not null
);

create table avatar_animation(
    id serial8 primary key,
    animation text references animation(id),
    avatar int8 references avatar(id)
);

insert into animation (id, name) values
('walk', 'Walk'),
('jump', 'Jump');