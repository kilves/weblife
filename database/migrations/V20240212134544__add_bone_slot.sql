alter table avatar_bone_slot add column name text;
create unique index temp_index on avatar_bone_slot (avatar_id, bone_slot_id);
alter table avatar_bone_slot add constraint avatar_bone_slot_pkey primary key using index temp_index;
