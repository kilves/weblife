alter table cell_object
    alter column pos_x set default 0,
    alter column pos_y set default 0,
    alter column pos_z set default 0,
    alter column rot_x set default 0,
    alter column rot_y set default 0,
    alter column rot_z set default 0,
    alter column rot_w set default 1;