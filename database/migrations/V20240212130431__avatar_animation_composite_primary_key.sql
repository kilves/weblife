alter table avatar_animation drop constraint avatar_animation_pkey;
alter table avatar_animation
    drop column id,
    alter column avatar_id set not null,
    alter column animation_id set not null;
create unique index temp_index on avatar_animation (avatar_id, animation_id);
alter table avatar_animation add constraint avatar_animation_pkey primary key using index temp_index;