alter table resource drop column cell;
insert into resource(uuid, extension, type) values('cube', 'glb', 'OBJECT');
create table cell_resource(
    id serial8 primary key,
    cell int8 references cell(id),
    resource int8 references resource(id)
);