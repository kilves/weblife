create table avatar(
    id serial8 primary key,
    login_user int8 references login_user(id),
    name text
);