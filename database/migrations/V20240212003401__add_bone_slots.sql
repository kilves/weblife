create table bone_slot(
    id serial8 primary key,
    name text
);

create table avatar_bone_slot(
    avatar_id int8 references avatar(id) not null,
    bone_slot_id int8 references bone_slot(id) not null
);