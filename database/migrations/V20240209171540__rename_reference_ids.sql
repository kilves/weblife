alter table avatar rename column login_user to login_user_id;
alter table avatar_animation rename column avatar to avatar_id;
alter table avatar_animation rename column animation to animation_id;
alter table cell_object rename column cell to cell_id;
alter table cell_object_resource rename column cell_object to cell_object_id;
alter table cell_object_resource rename column resource to resource_id;
alter table cell_resource rename column cell to cell_id;
alter table cell_resource rename column resource to resource_id;
alter table login rename column login_user to login_user_id;
alter table profile_picture rename column login_user to login_user_id;
alter table resource rename column login_user to login_user_id;