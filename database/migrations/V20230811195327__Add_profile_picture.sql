create table profile_picture(
    id serial8 primary key,
    login_user int8 references login_user(id),
    uuid text not null,
    active bool not null default false,
    extension text not null
);