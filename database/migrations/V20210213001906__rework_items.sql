DROP TABLE cell_item;
DROP TABLE item_resource;
DROP TABLE item;
DROP TABLE inventory;

CREATE TABLE cell_object(
    id serial8 PRIMARY KEY,
    cell int8 REFERENCES cell(id) NOT NULL,
    name text,
    pos_x decimal,
    pos_y decimal,
    pos_z decimal,
    rot_x decimal,
    rot_y decimal,
    rot_z decimal,
    rot_w decimal
);

CREATE TABLE cell_object_resource(
    id serial8 PRIMARY KEY,
    cell_object int8 REFERENCES cell_object(id),
    resource int8 REFERENCES resource(id)
);