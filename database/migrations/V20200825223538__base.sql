create table login_user(
    id serial8 primary key,
    username text,
    password text
);

create table login(
    id serial8 primary key,
    login_user int8 references login_user(id),
    token text,
    expires timestamp
);

create table inventory(
    id serial8 primary key,
    login_user int8 references login_user(id)
);

create table item(
    id serial8 primary key,
    name text,
    inventory int8 references inventory(id),
    type text
);

create table cell(
    id serial8 primary key,
    name text
);

create table cell_item(
    id serial8 primary key,
    cell int8 references cell(id),
    item int8 references item(id),
    pos_x decimal,
    pos_y decimal,
    pos_z decimal,
    rot_x decimal,
    rot_y decimal,
    rot_z decimal,
    rot_w decimal
);

create table resource(
    id serial8 primary key,
    uuid text,
    cell int8 references cell(id),
    extension text,
    type text
);

create table item_resource(
    id serial8 primary key,
    item int8 references item(id),
    resource int8 references resource(id)
);
