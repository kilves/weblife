# Weblife (WIP name)
It's a virtual world or something

* All cmd stuff in this documentation start relative to the root folder
* `[foo]` = optional
* `(foo|bar|baz)` = choose either foo, bar, or baz

## Stuff you need
* node >= 12.x (for backend)
* Angular CLI (for building frontend)
* flyway (for migrations)
* postgresql

## Setting up configuration
Create file(s) `src/(development|production).env`
(example file provided at `src/environment.env.sample`)

## Setting up database
Enter <PGPASSWORD> (the password in the configuration file) as password when prompted
```shell
sudo -u postgres createuser <PGUSER> -P
sudo -u postgres createdb <PGDATABASE>
sudo -u postgres psql
alter database <PGDATABASE> owner to <PGUSER>
```

## Creating resource directory
Create <RESOURCE_DIRECTORY> if it does not already exist.
Move everything from `src/resource` to the resource directory 

## Installing
```
cd frontend
npm install
cd ../src
npm install
```

## Building
### Frontend
```
cd frontned
ng build [--prod]
```
### Backend
```
cd src
tsc
```

## Running
Run either development or production depending on what you configured.
This will also run all migrations with flyway
```
cd src
npm run (development|production)
```

## Developing
While developing you should run the provided watch scripts
at `(src|frontend)/package.json -> watch`.
Restart the server (see above) whenever you change the backend.
No restart is required if you only change the frontend.