import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { DefaultComponent } from "./component/default/default.component";
import { SocketService } from "./service/socket.service";
import { LoginComponent } from "./component/login/login.component";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { UserService } from "./service/user.service";
import { HttpClientModule } from "@angular/common/http";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { CellService } from "./service/cell.service";
import { LogoutComponent } from "./component/logout/logout.component";
import { ChatComponent } from "./component/chat/chat.component";
import {DragDropModule} from "@angular/cdk/drag-drop";
import {AngularDraggableModule} from "angular2-draggable";
import { WindowComponent } from "./component/window/window.component";
import { InventoryComponent } from "./component/inventory/inventory.component";
import {WindowService} from "./service/window.service";
import { CreateItemComponent } from './component/create-item/create-item.component';
import { WindowContainerDirective } from './directive/window-container.directive';
import { WindowContainerComponent } from './component/window-container/window-container.component';
import {PromiseClient} from "./service/promise-client";
import {CustomContextMenuDirective} from "./directive/custom-context-menu.directive";
import { RegisterComponent } from './component/register/register.component';
import { EnterRoomComponent } from './component/enter-room/enter-room.component';
import { ItemComponent } from './component/item/item.component';
import { AreaComponent } from './component/area/area.component';
import {SceneDirective} from "./directive/scene.directive";
import {SceneService} from "./service/scene.service";
import {UiDirective} from "./directive/ui.directive";
import {UIService} from "./service/ui.service";
import {CameraService} from "./service/camera.service";
import {InputService} from "./service/input.service";
import {InputDirective} from "./directive/input.directive";
import {ContextMenuTargetService} from "./service/context-menu-target.service";
import {EffectService} from "./service/effect.service";
import {GrabService} from "./service/grab.service";
import { LabelComponent } from './component/label/label.component';
import {NameLabelService} from "./service/name-label.service";
import { ContextMenuComponent } from './component/context-menu/context-menu.component';
import {RoomService} from "./service/room.service";
import { SafePipe } from './pipe/safe.pipe';
import { EditObjectComponent } from './component/edit-object/edit-object.component';
import {GizmoService} from "./service/gizmo.service";
import {MatRadioModule} from "@angular/material/radio";
import {MatButtonModule} from "@angular/material/button";
import {MatInputModule} from "@angular/material/input";
import { SelectAvatarComponent } from './component/select-avatar/select-avatar.component';
import {MatSelectModule} from "@angular/material/select";
import { CreateAvatarComponent } from './component/create-avatar/create-avatar.component';
import {MatProgressBarModule} from "@angular/material/progress-bar";
import {MatFormFieldModule} from "@angular/material/form-field";
import { EditMapComponent } from './component/edit-map/edit-map.component';
import {MatIconModule} from "@angular/material/icon";
import {MatTooltipModule} from "@angular/material/tooltip";
import {MatSliderModule} from "@angular/material/slider";
import {ResourceService} from "./service/resource.service";
import { ModelListComponent } from './component/model-list/model-list.component';
import { SelectObjectResourceComponent } from './component/select-object-resource/select-object-resource.component';
import {MatSnackBarModule} from "@angular/material/snack-bar";
import { ProfileWindowComponent } from './component/profile-window/profile-window.component';
import { EditAvatarComponent } from './component/edit-avatar/edit-avatar.component';

@NgModule({
    declarations: [
        AppComponent,
        DefaultComponent,
        LoginComponent,
        LogoutComponent,
        ChatComponent,
        WindowComponent,
        InventoryComponent,
        CreateItemComponent,
        CustomContextMenuDirective,
        SceneDirective,
        InputDirective,
        UiDirective,
        WindowContainerDirective,
        WindowContainerComponent,
        RegisterComponent,
        EnterRoomComponent,
        ItemComponent,
        AreaComponent,
        LabelComponent,
        ContextMenuComponent,
        SafePipe,
        EditObjectComponent,
        SelectAvatarComponent,
        CreateAvatarComponent,
        EditMapComponent,
        ModelListComponent,
        SelectObjectResourceComponent,
        ProfileWindowComponent,
        EditAvatarComponent,
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        AppRoutingModule,
        HttpClientModule,
        MatFormFieldModule,
        MatInputModule,
        MatSnackBarModule,
        MatButtonModule,
        FormsModule,
        DragDropModule,
        AngularDraggableModule,
        ReactiveFormsModule,
        MatRadioModule,
        MatSelectModule,
        MatProgressBarModule,
        MatIconModule,
        MatTooltipModule,
        MatSliderModule,
    ],
    providers: [
        SceneService,
        CameraService,
        UIService,
        InputService,
        SocketService,
        RoomService,
        UserService,
        CellService,
        WindowService,
        ContextMenuTargetService,
        GrabService,
        GizmoService,
        EffectService,
        NameLabelService,
        PromiseClient,
        ResourceService,
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
