import {AfterViewInit, Component, ElementRef, ViewChild} from "@angular/core";
import {WindowClient} from "../../window-client";
import {
    AmbientLight, AnimationClip,
    Box3,
    DirectionalLight,
    Group,
    Object3D,
    PerspectiveCamera,
    Scene,
    Vector3,
    WebGLRenderer,
} from "three";
import {GLTFLoader} from "three/examples/jsm/loaders/GLTFLoader";
import {AvatarService} from "../../service/avatar.service";
import {WindowService} from "../../service/window.service";
import {Subject} from "rxjs";
import {Animation} from "../../model/animation";
import {FormControl, FormGroup} from "@angular/forms";

interface GameAction {
    name: string;
    id: string;
    animation?: AnimationClip;
}

@Component({
    selector: "app-create-avatar",
    templateUrl: "./create-avatar.component.html",
    styleUrls: ["./create-avatar.component.css"],
})
export class CreateAvatarComponent extends WindowClient {

    errors: any = {};
    onComplete: Subject<number> = new Subject<number>();

    constructor(private avatarService: AvatarService) {
        super();
    }

    submit(formData: FormData) {
        this.avatarService.createAvatar(formData).subscribe(res => {
            this.onComplete.next(res.id);
        }, error => {
            this.errors = error.error;
        });
    }
}
