import {ChangeDetectorRef, Component, OnInit} from "@angular/core";
import {Avatar} from "../../model/avatar";
import {AvatarService} from "../../service/avatar.service";
import {WindowService} from "../../service/window.service";
import {CreateAvatarComponent} from "../create-avatar/create-avatar.component";
import {RoomService} from "../../service/room.service";
import {EditAvatarComponent} from "../edit-avatar/edit-avatar.component";
import {ToastService} from "../../service/toast.service";
import {tap} from "rxjs/operators";
import {GLTFLoader} from "three/examples/jsm/loaders/GLTFLoader";

@Component({
    selector: "app-select-avatar",
    templateUrl: "./select-avatar.component.html",
    styleUrls: ["./select-avatar.component.css"],
})
export class SelectAvatarComponent implements OnInit {
    _avatar: Avatar;

    constructor(public avatarService: AvatarService,
                private windowService: WindowService,
                private toastService: ToastService,
                private changeDetectorRef: ChangeDetectorRef,
                private roomService: RoomService) {
    }

    async ngOnInit() {
        await this.avatarService.updateAvatars();
    }

    private async openEditMenu(): Promise<EditAvatarComponent> {
        await this.windowService.create(EditAvatarComponent, {
            destroyOnClose: true,
            centerOnCreate: true,
            widthEm: 50,
            heightEm: 28,
            resizable: false,
        });
        return this.windowService.open(EditAvatarComponent);
    }

    async createAvatar() {
        const component = await this.openEditMenu();
        component.onSubmit.subscribe(async formData => {
            this.avatarService.createAvatar(formData).subscribe(async res => {
                this.windowService.close(EditAvatarComponent);
                await this.avatarService.updateAvatars();
                this.avatarService.avatar.next(this.avatarService.avatars.find(a => a.id === res.id));
            }, err => {
                this.toastService.error(err);
            });
        });
    }

    async editAvatar() {
        const component = await this.openEditMenu();
        const avatarId = this.avatarService.avatar.getValue().id;
        const loader = new GLTFLoader();
        const gltf = await loader.loadAsync(`/avatar/${avatarId}`);
        component.setAvatar(gltf.scene, gltf.animations);
        const avatar = await this.avatarService.get(avatarId);
        component.setAnimations(avatar.animations);
        component.setBones(avatar.boneSlots);
        component.avatarName = avatar.name;
        component.onSubmit.subscribe(async formData => {
            this.avatarService.editAvatar(avatar.id, formData).subscribe(async res => {
                this.windowService.close(EditAvatarComponent);
                await this.avatarService.updateAvatars();
                this.avatarService.avatar.next(this.avatarService.avatars.find(a => a.id === res.id));
            }, err => {
                this.toastService.error(err);
            });
        });
    }
}
