import {
    ApplicationRef, ChangeDetectorRef,
    Component,
    ComponentRef,
    ElementRef,
    EventEmitter,
    OnInit,
    Output,
    ViewChild
} from "@angular/core";
import {ChannelData, ChatMessage, ChatMessageSend} from "../../model/chat-message";
import {SocketService} from "../../service/socket.service";
import {WindowClient} from "../../window-client";
import {BehaviorSubject} from "rxjs";
import {UserService} from "../../service/user.service";
import {NameLabelService} from "../../service/name-label.service";

@Component({
    selector: "app-chat",
    templateUrl: "./chat.component.html",
    styleUrls: ["./chat.component.css"]
})
export class ChatComponent extends WindowClient implements OnInit {

    message: ChatMessageSend = {
        text: "",
        channel: {
            name: "local",
            private: false,
        }
    };

    channels: Channel[] = [];

    channel: Channel;

    @Output()
    closeWindow: EventEmitter<void> = new EventEmitter<void>();

    @ViewChild("scrollView")
    private scrollContainer: ElementRef;
    
    constructor(public socketService: SocketService,
                private changeDetectorRef: ChangeDetectorRef,
                private userService: UserService,
                private nameLabelService: NameLabelService) {
        super();
    }

    ngOnInit(): void {
        this.socketService.onChatMessage.subscribe(message => {
            this.nameLabelService.showMessage(message.sender, message.text);
            this.addMessage(message);
            this.changeDetectorRef.detectChanges();
            this.scrollContainer.nativeElement.scrollTop = this.scrollContainer.nativeElement.scrollHeight;
        });
    }

    addMessage(message: ChatMessage) {
        if (!this.hasChannel(message.channel)) {
            this.addChannel(message.channel);
        }
        const channel = this.getChannel(message.channel);
        channel.addMessage(message);
        const thisChannelData = this.channel.getChannelData();
        const channelData = channel.getChannelData();
        if (thisChannelData.name === channelData.name &&
            message.channel.private &&
            message.sender !== this.userService.user.getValue().username) {

            channel.checked = false;
        }
        this.updateChecked();
    }

    addChannel(channel: ChannelData) {
        const newChannel = new Channel({...channel});
        this.channels.push(newChannel);
        this.setChannel(newChannel);
        return newChannel;
    }

    hasChannel(channel: ChannelData) {
        return !!this.channels.find(c => c.getChannelData().name === channel.name);
    }

    getChannel(channel: ChannelData) {
        return this.channels.find(c => c.getChannelData().name === channel.name);
    }

    setChannel(channel: Channel) {
        this.message.channel = channel.getChannelData();
        channel.checked = true;
        this.channel = channel;
        this.updateChecked();
    }

    say() {
        this.socketService.chatMessage(this.message);
        this.message.text = "";
    }

    updateChecked() {
        const uncheckedChannels = this.channels.filter(c => !c.checked);
        this.notification.next(uncheckedChannels.length > 0 ? "!" : "");
    }

    onWindowOpened() {
        this.channel.checked = true;
        this.updateChecked();
    }

}

class Channel {
    messages: ChatMessage[] = [];
    checked: boolean = true;

    constructor(private info: ChannelData) {}

    addMessage(message: ChatMessage) {
        this.messages.push(message);
    }

    getChannelData(): ChannelData {
        return this.info;
    }
}
