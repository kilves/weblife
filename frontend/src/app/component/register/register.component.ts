import {Component, OnInit} from '@angular/core';
import {NgForm} from "@angular/forms";
import {UserService} from "../../service/user.service";

@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.css'],
})
export class RegisterComponent implements OnInit {

    errors: any = {};
    success: boolean = false;
    verificationRequired: boolean;

    constructor(private userService: UserService) {
    }

    ngOnInit(): void {
    }

    async register(f: NgForm) {
        this.userService.register(f.value).then(res => {
            this.success = true;
            this.verificationRequired = res.verificationRequired;
        }).catch(res => {
            this.errors = res.error;
            for (const field of Object.getOwnPropertyNames(this.errors)) {
                f.form.controls[field].setErrors({"incorrect": true});
            }
        });
    }
}
