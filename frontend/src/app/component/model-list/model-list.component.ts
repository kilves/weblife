import {Component, EventEmitter, Input, OnInit, Output} from "@angular/core";
import {Resource, ResourceType} from "../../model/resource";
import {Vector2} from "three";
import {CellService} from "../../service/cell.service";
import {MathService} from "../../service/math.service";
import {ResourceService} from "../../service/resource.service";

@Component({
    selector: "app-model-list",
    templateUrl: "./model-list.component.html",
    styleUrls: ["./model-list.component.css"],
})
export class ModelListComponent {

    @Output()
    modelClicked: EventEmitter<Resource> = new EventEmitter<Resource>();
    dropPosition: Vector2;

    @Input()
    highlighted: Resource;

    constructor(private cellService: CellService,
                private math: MathService,
                public resourceService: ResourceService) {
    }

    itemClicked(item: Resource) {
        this.modelClicked.emit(item);
    }

    dragStarted(event, item: Resource) {
        // TODO drag drop this.cellService.loadSpawnPreview(item);
    }

    async dragEnded(event, item: Resource) {
    }

    dragMoved(event, item: Resource) {
        this.dropPosition = new Vector2(event.pointerPosition.x, event.pointerPosition.y);
        this.cellService.showSpawnPreview(this.math.getNormalizedScreenPosition(this.dropPosition), item);
    }
}
