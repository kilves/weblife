import {Component, OnInit} from "@angular/core";
import {ContextMenuService} from "../../service/context-menu.service";
import {MenuOption} from "../../model/menu-option";

@Component({
    selector: "app-context-menu",
    templateUrl: "./context-menu.component.html",
    styleUrls: ["./context-menu.component.css"],
})
export class ContextMenuComponent implements OnInit {

    constructor(public menuService: ContextMenuService) {
    }

    onClick(option: MenuOption) {
        option.action(this.menuService.customData);
        if (option.closeOnClick) {
            this.menuService.close();
        }
    }

    ngOnInit(): void {
    }

}
