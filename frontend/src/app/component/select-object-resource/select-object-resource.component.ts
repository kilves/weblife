import {Component} from "@angular/core";
import {WindowClient} from "../../window-client";
import {Resource} from "../../model/resource";
import {WindowService} from "../../service/window.service";
import {GameObjectService} from "../../service/game-object.service";

@Component({
    selector: "app-select-object-resource",
    templateUrl: "./select-object-resource.component.html",
    styleUrls: ["./select-object-resource.component.css"],
})
export class SelectObjectResourceComponent extends WindowClient {

    constructor(private windowService: WindowService,
                private gameObjectService: GameObjectService) {
        super();
    }

    async select(object: Resource) {
        await this.gameObjectService.setModel(this.customData.uuid, object.uuid);
        this.windowService.close(SelectObjectResourceComponent);
    }
}
