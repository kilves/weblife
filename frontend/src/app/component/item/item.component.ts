import {Component, Input, OnInit} from "@angular/core";
import {GameObjectCreate, ItemType} from "../../model/gameObjectCreate";
import {Resource, ResourceType} from "../../model/resource";

@Component({
    selector: "app-item",
    templateUrl: "./item.component.html",
    styleUrls: ["./item.component.css"],
})
export class ItemComponent implements OnInit {

    @Input() item: Resource;
    ItemType = ItemType;
    ResourceType = ResourceType;

    constructor() {
    }

    ngOnInit(): void {
    }

}
