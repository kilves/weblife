import {Component, OnInit} from "@angular/core";
import {WindowClient} from "../../window-client";
import {CellService} from "../../service/cell.service";
import {ItemObject} from "../../../3d/world/item-object";

interface ObjectData {
    uuid: string;
}

enum EditMode {
    MOVE,
    ROTATE,
    SCALE
}

@Component({
    selector: "app-edit-object",
    templateUrl: "./edit-object.component.html",
    styleUrls: ["./edit-object.component.css"],
})
export class EditObjectComponent extends WindowClient implements OnInit {

    customData: ObjectData;
    object: ItemObject;
    EditMode = EditMode;

    private _editMode: EditMode = EditMode.MOVE;

    get editMode(): EditMode {
        return this._editMode;
    }

    set editMode(mode: EditMode) {
        this._editMode = mode;
        this.updateGizmos();
    }

    constructor(private cellService: CellService) {
        super();
    }

    ngOnInit(): void {
        this.object = this.cellService.findObject(this.customData.uuid);
        this.updateGizmos();
    }

    private updateGizmos() {
        this.object.moveGizmo.visible = this._editMode === EditMode.MOVE;
        this.object.rotateGizmo.visible = this._editMode === EditMode.ROTATE;
        this.object.scaleGizmo.visible = this._editMode === EditMode.SCALE;
    }

    resetRotation() {
        this.object.rotateGizmo.reset();
    }

    onClose() {
        this.object.moveGizmo.visible = false;
        this.object.rotateGizmo.visible = false;
        this.object.scaleGizmo.visible = false;
    }
}
