import {Component, Input} from "@angular/core";
import {Vector2} from "three";
import {NameLabel} from "../../service/name-label.service";

@Component({
    selector: "app-label",
    templateUrl: "./label.component.html",
    styleUrls: ["./label.component.css"],
})
export class LabelComponent {

    style = {
        position: "relative",
        left: "0px",
        bottom: "0px"
    };

    private _position: Vector2;
    @Input()
    get position(): Vector2 {
        return this._position;
    }
    set position(value: Vector2) {
        this._position = value;
        this.update();
    }

    @Input() name: string;
    @Input() chatMessage: string;

    update() {
        this.style = {
            ...this.style,
            position: "absolute",
            left: `${(this._position.x + 1) * 50}%`,
            bottom: `${((this._position.y + 1) * 50)}%`
        }
    }
}
