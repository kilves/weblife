import {Component, OnInit} from "@angular/core";
import {WindowClient} from "../../window-client";
import {UserService} from "../../service/user.service";
import {HttpClient} from "@angular/common/http";
import {PromiseClient} from "../../service/promise-client";

enum PictureState {
    CHECKING,
    AVAILABLE,
    NOT_AVAILABLE
}

@Component({
    selector: "app-profile-window",
    templateUrl: "./profile-window.component.html",
    styleUrls: ["./profile-window.component.css"],
})
export class ProfileWindowComponent extends WindowClient implements OnInit {
    profilePictureUrl: string;
    me: boolean;
    private _pictureAvailable = PictureState.CHECKING;
    PictureState = PictureState;

    get showPicture() {
        return this._pictureAvailable === PictureState.AVAILABLE;
    }

    get showPlaceholder() {
        return this._pictureAvailable === PictureState.NOT_AVAILABLE;
    }

    constructor(private userService: UserService,
                private http: PromiseClient) {
        super();
    }

    ngOnInit(): void {
        this.profilePictureUrl = `/profiles/${this.customData.username}/profilePicture.png`;
        this.me = this.userService.user.value && this.userService.user.value.username === this.customData.username;
        this.http.get(this.profilePictureUrl).then(() => {
            this._pictureAvailable = PictureState.AVAILABLE;
        }).catch(() => {
            this._pictureAvailable = PictureState.NOT_AVAILABLE
        })
    }

    uploadProfilePicture(event) {
        const inputElement: HTMLInputElement = event.target;
        if (inputElement.files.length == 0) {
            return;
        }
    }
}

