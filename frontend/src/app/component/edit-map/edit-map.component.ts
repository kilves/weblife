import {Component} from "@angular/core";
import {WindowClient} from "../../window-client";
import {ThemePalette} from "@angular/material/core";
import {EditTerrainService} from "../../service/edit-terrain.service";
import {Brush, BrushConfig, BrushState} from "../../model/brush";

@Component({
    selector: "app-edit-map",
    templateUrl: "./edit-map.component.html",
    styleUrls: ["./edit-map.component.css"],
})
export class EditMapComponent extends WindowClient {
    brushes: Brush[] = [
        {
            name: "Raise",
            color: undefined,
            brush: BrushState.RAISE,
            icon: "vertical_align_top"
        },
        {
            name: "Lower",
            color: undefined,
            brush: BrushState.LOWER,
            icon: "vertical_align_bottom"
        },
        {
            name: "Smooth",
            color: undefined,
            brush: BrushState.SMOOTH,
            icon: "waves"
        }
    ];
    activeBrush: Brush;
    config: BrushConfig = {
        size: 20,
        force: 50
    };

    constructor(private editTerrainService: EditTerrainService) {
        super();
    }

    setBrush(brush: Brush) {
        if (this.activeBrush) {
            this.activeBrush.color = undefined;
        }
        this.activeBrush = brush;
        this.activeBrush.color = "accent";
        this.editTerrainService.setBrush(brush.brush, this.config);
    }

    clearBrush() {
        if (this.activeBrush) {
            this.activeBrush.color = undefined;
            this.activeBrush = undefined;
        }
        this.editTerrainService.setBrush(BrushState.NONE, this.config);
    }

    setSize(event) {
        this.config.size = +(event.target as HTMLInputElement).value;
        if (this.activeBrush) {
            this.editTerrainService.setBrush(this.activeBrush.brush, this.config);
        }
    }

    setForce(event) {
        this.config.force = +(event.target as HTMLInputElement).value;
        if (this.activeBrush) {
            this.editTerrainService.setBrush(this.activeBrush.brush, this.config)
        }
    }

    onClose() {
        this.editTerrainService.setBrush(BrushState.NONE, {
            size: 20,
            force: 50
        });
        super.onClose();
    }
}
