import {
    Component, OnInit,
    Type,
    ViewChild,
    ViewContainerRef,
} from "@angular/core";
import { CellService } from "../../service/cell.service";
import {User} from "../../model/user";
import {WindowService} from "../../service/window.service";
import {ChatComponent} from "../chat/chat.component";
import {InventoryComponent} from "../inventory/inventory.component";
import {MathService} from "../../service/math.service";
import {AreaComponent} from "../area/area.component";
import {SceneService} from "../../service/scene.service";
import {Vector2} from "three";
import {UserService} from "../../service/user.service";
import {ActivatedRoute} from "@angular/router";
import {NameLabelService} from "../../service/name-label.service";
import {RoomService} from "../../service/room.service";
import {SocketService} from "../../service/socket.service";
import {RaycastService} from "../../service/raycast.service";
import {WindowOptions} from "../window/window.component";
import {BehaviorSubject} from "rxjs";
import {AvatarService} from "../../service/avatar.service";
import {XRService} from "../../service/xr.service";

export interface AppWindow {
    component: Type<any>;
    options: WindowOptions;
}

const WINDOWS: AppWindow[] = [
    {
        component: ChatComponent,
        options: {
            name: "Chat",
            heightEm: 20,
            widthEm: 35,
            notification: new BehaviorSubject<string>("")
        },

    },
    {
        component: InventoryComponent,
        options: {
            name: "Meshes"
        }
    },
    {
        component: AreaComponent,
        options: {
            name: "Area"
        }
    }
];

export enum Page {
    LOGIN,
    REGISTER
}

@Component({
    templateUrl: "./default.component.html",
    styleUrls: ["./default.component.css"]
})
export class DefaultComponent implements OnInit {
    title = "frontend";
    windows = WINDOWS;
    page: Page = Page.LOGIN;
    Page = Page;
    navigatorXR = navigator.xr;
    vrLoading = false;
    inVr = false;

    get user(): BehaviorSubject<User> {
        return this.userService.user;
    }
    
    @ViewChild("ui", {static: true, read: ViewContainerRef})
    private inputElement: ViewContainerRef;

    constructor(public cellService: CellService,
                private raycastService: RaycastService,
                public windowService: WindowService,
                public sceneService: SceneService,
                private xrService: XRService,
                private userService: UserService,
                private math: MathService,
                private route: ActivatedRoute,
                public nameLabelService: NameLabelService,
                public roomService: RoomService,
                private socketService: SocketService,
                public avatarService: AvatarService) {
    }

    async ngOnInit() {
        if (this.userService.user.getValue()) {
            await this.avatarService.updateAvatars();
        }
        this.sceneService.initScene();
        this.route.url.subscribe(url => {
            if (url.length !== 2 || url[0].path !== "room") {
                return;
            }
            const name = url[1].path;
            this.socketService.connect(name).then(() => {
                this.roomService.loadRoom(name);
            });
        });
    }

    contextMenu(event: MouseEvent) {
        event.preventDefault();
        this.raycastService.raycastContextMenu(this.math.getNormalizedScreenPosition(new Vector2(event.clientX, event.clientY)));
    }

    createWindows() {
        for (let window of WINDOWS) {
            this.windowService.create(window.component, window.options).then();
        }
    }

    async toggleVR() {
        this.vrLoading = true;
        if (!this.inVr) {
            this.xrService.enterVR().then(success => {
                if (success) {
                    this.inVr = true;
                }
            }).finally(() => {
                this.vrLoading = false;
            });
        } else if (this.inVr) {
            this.xrService.exitVR().then(success => {
                if (success) {
                    this.inVr = false;
                }
            }).finally(() => {
                this.vrLoading = false;
            })
        }
    }

    async recenter() {
        await this.xrService.recenter();
    }

    protected readonly JSON = JSON;
}
