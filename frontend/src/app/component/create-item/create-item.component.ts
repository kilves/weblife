import {Component, OnInit} from "@angular/core";
import {GameObjectService} from "../../service/game-object.service";
import {GameObjectCreate, ItemType} from "../../model/gameObjectCreate";
import {WindowService} from "../../service/window.service";
import {WindowClient} from "../../window-client";
import {BehaviorSubject} from "rxjs";

@Component({
    selector: "app-create-item",
    templateUrl: "./create-item.component.html",
    styleUrls: ["./create-item.component.css"]
})
export class CreateItemComponent extends WindowClient implements OnInit {

    item: GameObjectCreate = {
    };
    
    error: string;
    
    itemTypes = Object.keys(ItemType);

    constructor(private itemService: GameObjectService,
                private windowService: WindowService) {
        super();
    }

    ngOnInit(): void {
    }
  
    create() {
        this.itemService.create(this.item).subscribe(() => {
            this.windowService.close(CreateItemComponent);
        }, error => {
            this.error = "Error";
        });
    }
}
