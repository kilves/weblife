import {Component, ElementRef, OnInit, ViewChild} from "@angular/core";
import {WindowClient} from "../../window-client";
import {MeshService} from "../../service/mesh.service";
import {ResourceService} from "../../service/resource.service";
import {Resource} from "../../model/resource";

@Component({
    selector: "app-inventory",
    templateUrl: "./inventory.component.html",
    styleUrls: ["./inventory.component.scss"]
})
export class InventoryComponent extends WindowClient implements OnInit {

    @ViewChild("uploadForm")
    private uploadForm: ElementRef;

    @ViewChild("uploadFile")
    private uploadFile: ElementRef;

    selected: Resource;

    constructor(private meshService: MeshService,
                private resourceService: ResourceService) {
        super();
    }

    modelClicked(model: Resource) {
        this.selected = model;
    }

    async deleteSelected() {
        await this.resourceService.delete(this.selected);
    }

    ngOnInit(): void {
    }

    async uploadMesh(e) {
        const formData = new FormData(this.uploadForm.nativeElement);
        await this.meshService.createModel(formData);
        this.uploadFile.nativeElement.value = this.uploadFile.nativeElement.defaultValue;
        await this.resourceService.updateResources();
    }
}
