import {Component, OnInit} from "@angular/core";
import { User } from "../../model/user";
import { UserService } from "../../service/user.service";
import {FormBuilder, FormControl, FormGroup, NgForm} from "@angular/forms";
import {AvatarService} from "../../service/avatar.service";

@Component({
    templateUrl: "./login.component.html",
    styleUrls: ["./login.component.css"],
    selector: "app-login"
})
export class LoginComponent implements OnInit {

    error: string;

    constructor(private userService: UserService,
                private avatarService: AvatarService) {
    }

    ngOnInit() {
    }
    
    login(f: NgForm) {
        this.userService.login(f.value).then(async () => {
            await this.avatarService.updateAvatars();
        }).catch(e => {
            this.error = e.error;
        });
    }
}
