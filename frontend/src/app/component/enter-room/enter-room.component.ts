import {Component, Input, OnInit} from "@angular/core";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
    selector: "app-enter-room",
    templateUrl: "./enter-room.component.html",
    styleUrls: ["./enter-room.component.scss"],
})
export class EnterRoomComponent implements OnInit {

    room: string;
    @Input() roomNotFound: boolean;
    @Input() defaultRoom: string;

    constructor(private router: Router) {
    }

    ngOnInit(): void {
        this.room = this.defaultRoom;
    }

    loadRoom() {
        const url = new URL(window.location.href);
        this.router.navigate([`/room/${this.room}`])
    }
}
