import {Component, ElementRef, EventEmitter, Input, Output, ViewChild} from "@angular/core";
import {
    AmbientLight,
    AnimationClip, Bone,
    Box3, Box3Helper, BoxHelper, BufferGeometry,
    DirectionalLight,
    Group, Mesh, MeshBasicMaterial,
    Object3D,
    PerspectiveCamera,
    Scene, SkinnedMesh, SphereGeometry,
    Vector3,
    WebGLRenderer,
} from "three";
import {Animation} from "../../model/animation";
import {GLTFLoader} from "three/examples/jsm/loaders/GLTFLoader";
import {AvatarService} from "../../service/avatar.service";
import {WindowClient} from "../../window-client";
import {findBones, fixBoundingShapes} from "../../../3d/utility/util";
import {AvatarAnimation, AvatarBoneSlot} from "../../model/avatar";
import {BoneSlot} from "../../model/boneSlot";
import {BoneSlotService} from "../../service/bone-slot.service";

@Component({
    selector: "app-edit-avatar",
    templateUrl: "./edit-avatar.component.html",
    styleUrls: ["./edit-avatar.component.css"],
})
export class EditAvatarComponent extends WindowClient {
    avatar: Group;
    animations: Animation[];
    public animationClips: AnimationClip[];
    errors: any = {};

    @ViewChild("form") form: ElementRef;

    @Output()
    onSubmit = new EventEmitter<FormData>();

    private scene: Scene;
    private renderer: WebGLRenderer;
    private camera: PerspectiveCamera;
    private light: DirectionalLight;
    private cameraPosition: Vector3 = new Vector3();
    private cameraLookAt: Vector3 = new Vector3();
    private debugSphere = new Mesh(
        new SphereGeometry(0.1),
        new MeshBasicMaterial({
            depthTest: false,
            color: 0xff00ff
        })
    )
    @ViewChild("createAvatarCanvas") canvas!: ElementRef;
    avatarName: string;
    boneSlots: BoneSlot[] = [];
    bones: Bone[] = [];
    vrReady: boolean = false;
    vrError: string = "";

    constructor(private avatarService: AvatarService,
                private boneSlotService: BoneSlotService) {
        super();
    }

    async ngAfterViewInit() {
        this.renderer = new WebGLRenderer({
            canvas: this.canvas.nativeElement,
            antialias: true,
            logarithmicDepthBuffer: true,
        });
        this.boneSlots = await this.boneSlotService.listAll();
        this.avatarService.getAllAnimations().subscribe(animations => {
            this.animations = animations;
        });
        const canvas = this.canvas.nativeElement;
        this.renderer.setClearColor(0xadd8e6);
        this.scene = new Scene();
        this.camera = new PerspectiveCamera(60, canvas.width / canvas.height);
        this.camera.position.set(2, 2, 2);
        this.camera.lookAt(new Vector3());
        this.scene.add(this.camera);
        this.scene.add(new AmbientLight(0xffffff, 0.15));
        this.light = new DirectionalLight(0xffffff, 10);
        this.light.position.set(0.3, 1.1, -1);
        this.scene.add(this.light);
        this.debugSphere.renderOrder = 10;
        this.debugSphere.visible = false;
        this.scene.add(this.debugSphere);
        requestAnimationFrame(this.render.bind(this));
    }

    updateSize() {
        const w = this.canvas.nativeElement.clientWidth;
        const h = this.canvas.nativeElement.clientHeight;
        this.renderer.setSize(w, h);
        this.camera.aspect = w / h;
        this.camera.updateProjectionMatrix();
    }

    private updateCamera() {
        fixBoundingShapes(this.avatar);
        const boundingBox = new Box3().expandByObject(this.avatar);
        this.camera.position.set(
            boundingBox.max.x + boundingBox.max.x / 1.5,
            (boundingBox.min.y + boundingBox.max.y) / 2,
            boundingBox.max.z + boundingBox.max.z / 1.5
        );
        boundingBox.getCenter(this.cameraLookAt);
        this.cameraPosition.set(
            0, // Math.max(this.camera.position.x, this.camera.position.z),
            this.camera.position.y,
            Math.max(this.camera.position.x, this.camera.position.z)
        );
        this.light.position.set(
            boundingBox.max.x * 0.3,
            boundingBox.max.y * 1.1,
            boundingBox.max.z * -1
        );
    }

    render(time: number) {
        requestAnimationFrame(this.render.bind(this));
        this.updateSize();
        this.camera.position.set(
            Math.cos(time / 1000) * this.cameraPosition.x,
            this.cameraPosition.y,
            Math.sin(time / 1000) * this.cameraPosition.z
        );
        this.camera.position.copy(this.cameraPosition);
        this.camera.lookAt(this.cameraLookAt);
        this.renderer.render(this.scene, this.camera);
    }

    async upload(event) {
        const inputElement: HTMLInputElement = event.target;
        if (inputElement.files.length == 0) {
            return;
        }
        const file = inputElement.files[0];
        const loader = new GLTFLoader();
        loader.parse(await file.arrayBuffer(), "", gltf => {
            if (this.avatar) {
                this.scene.remove(this.avatar);
            }
            this.setAvatar(gltf.scene, gltf.animations);
        });
    }

    setAvatar(avatar: Group, animations: AnimationClip[]) {
        this.avatar = avatar;
        this.animationClips = animations;
        this.scene.add(this.avatar);
        // This setTimeout is necessary because only next frame updates SkinnedMesh
        setTimeout(this.updateCamera.bind(this));
        this.bones = findBones(this.avatar);
    }

    setAnimations(animations: AvatarAnimation[]) {
        for (const animation of animations) {
            const animationSlot = this.animations.find(
                a => a.id === animation.animationId
            );
            if (animationSlot) {
                animationSlot.animation = animation.name;
            }
        }
    }

    setBones(bones: AvatarBoneSlot[]) {
        for (const bone of bones) {
            const slot = this.boneSlots.find(b => b.id === bone.id);
            if (slot) {
                slot.bone = bone.name;
            }
        }
        this.updateVRReady();
    }
    submit() {
        const formData = new FormData(this.form.nativeElement);
        for (const action of this.animations) {
            if (!action.animation) {
                continue;
            }
            formData.append(`${action.id}-animation`, action.animation);
        }
        for (const slot of this.boneSlots) {
            if (!slot.bone) {
                continue;
            }
            formData.append(`vr_bone_slot-${slot.id}`, slot.bone);
        }
        if (this.vrReady) {
            formData.append("isVrReady", "");
        }
        this.onSubmit.emit(formData);
    }

    private getObjects(obj: Object3D, ary: Mesh[] = []): Mesh<BufferGeometry>[] {
        const objects = (obj.children as Mesh[]).filter(
            c => c["geometry"]
        );
        ary.push(...objects);
        obj.children.forEach(o => {
            this.getObjects(o, ary)

        });
        return ary;
    }

    boneSlotChanged(boneName: string) {
        const bone = this.bones.find(b => b.name === boneName);
        if (!bone) {
            this.debugSphere.visible = false;
            return;
        }
        bone.getWorldPosition(this.debugSphere.position);
        this.debugSphere.visible = true;
        this.updateVRReady();
    }

    private updateVRReady() {
        try {
            this.ensureVRReady();
            this.vrReady = true;
            this.vrError = "Avatar is ready for VR!";
        } catch (e) {
            this.vrError = (e as Error).message;
            this.vrReady = false;
        }
    }

    private ensureVRReady() {
        this.ensureBoneHierarchy(
            "left_shoulder",
            "left_upper_arm",
            "left_lower_arm",
            "left_hand"
        );

        this.ensureBoneHierarchy(
            "right_shoulder",
            "right_upper_arm",
            "right_lower_arm",
            "right_hand"
        );

        this.ensureBoneHierarchy(
            "hips",
            "left_upper_leg",
            "left_lower_leg",
            "left_foot",
            "left_toes"
        );

        this.ensureBoneHierarchy(
            "hips",
            "right_upper_leg",
            "right_lower_leg",
            "right_foot",
            "right_toes"
        );

        this.ensureBoneHierarchy(
            "hips",
            "spine",
            "chest"
        );

        this.ensureBoneHierarchy(
            "neck",
            "head"
        );
    }

    private ensureBoneHierarchy(...slots: string[]) {
        for (const [i, slotName] of slots.entries()) {
            const boneSlot = this.boneSlot(slotName);
            const bone = this.bone(boneSlot);
            if (i === slots.length - 1) {
                return;
            }
            const nextSlot = this.boneSlot(slots[i + 1]);
            const nextBone = this.bone(nextSlot);
            if (nextBone.parent !== bone) {
                const actualParentSlot = this.boneSlots.find(s => s.bone === nextBone.parent.name);
                const errors = [`${nextSlot.name} is not a direct child of ${boneSlot.name}`];
                if (actualParentSlot) {
                    errors.push(`${nextSlot.name} is a child of ${actualParentSlot.name}`);
                } else if (nextBone.parent) {
                    errors.push(`${nextSlot.name} is a child of unassigned bone ${nextBone.parent.name}`);
                } else {
                    errors.push(`${nextSlot.name} is the root bone, and has no parent`);
                }
                throw new Error(errors.join("\n"));
            }
        }
    }

    private bone(boneSlot: BoneSlot) {
        const bone = this.bones.find(b => b.name === boneSlot.bone);
        if (!bone) {
            throw new Error(`No bone assigned to ${boneSlot.name}`);
        }
        return bone;
    }

    private boneSlot(name: string) {
        const boneSlot = this.boneSlots.find(s => s.name === name);
        if (!boneSlot) {
            throw new Error(`No such bone slot: ${boneSlot.name}`);
        }
        return boneSlot;
    }
}
