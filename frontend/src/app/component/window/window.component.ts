import {
    Component,
    ComponentFactoryResolver, ComponentRef,
    ElementRef,
    EventEmitter,
    Host, Injector,
    Input, OnDestroy,
    OnInit,
    Output,
    Type,
    ViewChild, ViewContainerRef,
} from "@angular/core";
import {BehaviorSubject} from "rxjs";
import {WindowClient} from "../../window-client";

export interface WindowOptions {
    name?: string;
    destroyOnClose?: boolean;
    centerOnCreate?: boolean;
    widthEm?: number;
    heightEm?: number;
    resizable?: boolean;
    autoScale?: boolean;
    customData?: any;
    notification?: BehaviorSubject<string>;
}

@Component({
    templateUrl: "./window.component.html",
    styleUrls: ["./window.component.scss"]
})
export class WindowComponent<T extends WindowClient> implements OnInit, OnDestroy {

    active = false;
    component: Type<T>;
    componentRef: ComponentRef<T>;

    resizable = true;

    @ViewChild("content", {static: true, read: ViewContainerRef})
    private content: ViewContainerRef;
    @ViewChild("window", {static: true})
    private window: ElementRef;
    
    @Input() options: WindowOptions;
    @Output() created: EventEmitter<void> = new EventEmitter<void>();
    closed: EventEmitter<void> = new EventEmitter<void>();
    
    constructor(private componentFactoryResolver: ComponentFactoryResolver,
                private injector: Injector) {
    }

    ngOnInit(): void {
        if (!this.component) {
            console.error("WindowComponent cannot be used directly!");
            return;
        }
        const componentFactory = this.componentFactoryResolver.resolveComponentFactory(this.component);
        let componentRef = componentFactory.create(this.injector);
        componentRef.instance.notification = this.options.notification;
        this.componentRef = componentRef;
        this.content.insert(componentRef.hostView);
        componentRef.instance.customData = this.options.customData;

        let nativeElement = this.window.nativeElement as HTMLElement;
        const widthEm = this.options.widthEm || 30;
        const heightEm = this.options.heightEm || 15;
        nativeElement.style.width = `${widthEm}em`;
        nativeElement.style.height = `${heightEm}em`;
        if (this.options.autoScale) {
            nativeElement.style.width = "auto";
            nativeElement.style.height = "auto";
        }
        if (this.options.centerOnCreate) {
            nativeElement.style.top = `calc(50% - ${nativeElement.clientHeight / 2}px)`;
            nativeElement.style.left = `calc(50% - ${nativeElement.clientWidth / 2}px)`;
        }
        if (this.options.resizable !== this.resizable) {
            this.resizable = this.options.resizable;
        }
        this.created.emit();
    }

    open() {
        this.active = true;
    }

    close() {
        this.active = false;
    }
    
    ngOnDestroy() {
    }
    
    closeWindow() {
        this.closed.emit();
    }
}
