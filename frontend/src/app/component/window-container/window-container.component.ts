import {Component, EventEmitter, OnInit, Output, ViewChild, ViewContainerRef} from "@angular/core";
import {WindowService} from "../../service/window.service";

@Component({
    selector: "window-container",
    templateUrl: "./window-container.component.html",
    styleUrls: ["./window-container.component.css"]
})
export class WindowContainerComponent implements OnInit {

    @ViewChild("container", {static: true, read: ViewContainerRef})
    private windowContainer: ViewContainerRef;
    
    @Output()
    public created: EventEmitter<void> = new EventEmitter<void>();
    
    constructor(private windowService: WindowService) {
    }

    ngOnInit(): void {
        this.windowService.windowContainer = this.windowContainer;
        this.created.emit();
    }
    
}
