import {Component, OnInit} from "@angular/core";
import {CellService} from "../../service/cell.service";
import {WindowClient} from "../../window-client";

@Component({
    selector: "app-area",
    templateUrl: "./area.component.html",
    styleUrls: ["./area.component.css"],
})
export class AreaComponent extends WindowClient implements OnInit {

    constructor(public cellService: CellService) {
        super();
    }

    ngOnInit(): void {
    }
}
