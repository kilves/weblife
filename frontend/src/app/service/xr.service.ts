import {Injectable} from "@angular/core";
import {
    ArrowHelper,
    DoubleSide,
    FrontSide,
    Group,
    Matrix4,
    Object3D,
    Quaternion,
    Raycaster,
    Vector3,
    WebGLRenderer,
    WebXRManager, XRHandSpace,
} from "three";
import {CameraMode, CameraService} from "./camera.service";
import {XrControllerObject} from "../../3d/world/xr/xr-controller-object";
import {CharacterService} from "./character.service";
import {ToastService} from "./toast.service";
import {fetchProfile, MotionController} from "@webxr-input-profiles/motion-controllers";
import {CellService} from "./cell.service";
import {SocketService} from "./socket.service";
import {XrWindowObject} from "../../3d/world/xr/xr-window-object";
import {GameObjectService} from "./game-object.service";
import {ItemObject} from "../../3d/world/item-object";

const MOVEMENT_SPEED = 10;

const RAYCAST_DIRECTIONS = [
    new Vector3(0.2, 0, 0),
    new Vector3(-0.2, 0, 0),
    new Vector3(0, 0.2, 0),
    new Vector3(0, -0.2, 0),
    new Vector3(0, 0, 0.2),
    new Vector3(0, 0, -0.2),
]

@Injectable({
    providedIn: "root",
})
export class XRService {

    private xr: WebXRManager;
    private controllerObjects: XrControllerObject[] = [];
    private xrWindows: XrWindowObject[] = [];
    private baseReferenceSpace: XRReferenceSpace;
    private grabbed: ItemObject = null;
    private grabbedFormerParent: Object3D = null;
    public xrScene = new Group();
    private movement = new Vector3();
    private movementVec = new Vector3();
    private intersectRaycaster = new Raycaster();

    constructor(private cameraService: CameraService,
                private characterService: CharacterService,
                private toastService: ToastService,
                private cellService: CellService,
                private socketService: SocketService,
                private gameObjectService: GameObjectService) {
    }

    selectionEvent(event: XRInputSourceEvent) {
        const targetController = this.controllerObjects.find(
            o => o.motionController.xrInputSource === event.inputSource
        );
        if (!targetController) {
            return;
        }
        if (event.type === "select") {
            targetController.onSelect();
        }
        if (event.type === "selectend") {
            targetController.onSelectEnd();
        }
        if (event.type === "selectstart") {
            targetController.onSelectStart();
        }
    }
    selectFunc = this.selectionEvent.bind(this);

    inputSourceChange(event: XRInputSourceChangeEvent) {
        this.updateControllers().then();
    }
    inputSourceChangeFunc = this.inputSourceChange.bind(this);

    referenceSpaceChange(event: XRReferenceSpaceEvent) {
    }
    referenceSpaceChangeFunc = this.referenceSpaceChange.bind(this);

    squeezeEvent(event: XRInputSourceEvent) {
        const targetController = this.controllerObjects.find(
            o => o.motionController.xrInputSource === event.inputSource
        );
        if (!targetController) {
            return;
        }
        if (event.type === "squeezestart") {
            targetController.onSqueezeStart();
        }
        if (event.type === "squeezeend") {
            targetController.onSqueezeEnd();
        }
    }
    squeezeFunc = this.squeezeEvent.bind(this);

    touchEvent(event) {
    }
    touchFunc = this.touchEvent.bind(this);

    init(renderer: WebGLRenderer) {
        this.xr = renderer.xr;
        this.xr.cameraAutoUpdate = true;
        this.xr.setReferenceSpaceType("local");
    }

    handleButton(controller: XrControllerObject, button: any) {
        if (button.id === "xr-standard-touchpad") {
            if (button.state === "touched") {
                controller.setAnalogPosition(
                    button.xAxis,
                    button.yAxis
                );
            }
        }
    }

    processXrGamepads() {
        const inputSources = this.xr.getSession().inputSources;
        for (const [i, inputSource] of inputSources.entries()) {
            const controller = this.controllerObjects[i];
            if (!controller) {
                continue;
            }
            controller.motionController.updateFromGamepad();
            if (!Array.isArray(controller.motionController.data)) {
                return;
            }
            for (const button of controller.motionController.data) {
                this.handleButton(controller, button);
            }
        }
    }

    teleportTo(raycaster: Raycaster) {
        const intersects = raycaster.intersectObject(this.cellService.cellObject);
        if (intersects.length > 0) {
            this.socketService.teleport(intersects[0].point);
        }
    }

    createObject(raycaster: Raycaster) {
        const intersects = raycaster.intersectObject(this.cellService.cellObject);
        if (intersects.length > 0) {
            this.gameObjectService.create({
                x: intersects[0].point.x,
                y: intersects[0].point.y,
                z: intersects[0].point.z,
                name: "New item",
            }).subscribe();
        }
    }

    grabItem(origin: Vector3, grabbedBy: XrControllerObject) {
        const grabTargets = this.cellService.cellObject.items;
        // DoubleSide, Otherwise rays from inside object won't hit
        for (const mesh of grabTargets) {
            mesh.setMaterialSide(DoubleSide);
        }
        /*
        This intersect is a bit unreliable...
        Doesn't work if entire ray is inside object.
        Let's just do multiple intersects until we find one that works :D
         */
        let intersects = [];
        for (const direction of RAYCAST_DIRECTIONS) {
            this.intersectRaycaster.set(origin, direction);
            intersects = this.intersectRaycaster.intersectObjects(
                grabTargets.map(i => i.object)
            );
            if (intersects.length > 0) {
                break;
            }
        }
        for (const mesh of grabTargets) {
            mesh.restoreMaterialSide();
        }

        if (intersects.length === 0) {
            return;
        }

        // We will hit any of the arbitrary child objects...
        // so find closest ItemObject
        const toGrab = this.findParentItemObject(intersects[0].object);
        if (!toGrab) {
            return;
        }

        // Finally grab it
        this.grabbed = toGrab;
        this.grabbedFormerParent = this.grabbed.parent;
        this.grabbed.parent.remove(this.grabbed);
        grabbedBy.attach(this.grabbed);
    }

    releaseItem() {
        if (!this.grabbed) {
            return;
        }
        this.grabbedFormerParent.attach(this.grabbed);
        this.grabbed.updateMatrixWorld(true);
        const position = this.grabbed.object.getWorldPosition(new Vector3());
        const rotation = this.grabbed.object.getWorldQuaternion(new Quaternion());
        this.socketService.setItemPosition(this.grabbed.item, position);
        this.socketService.setItemRotation(this.grabbed.item, rotation);
        this.grabbed = null;
        this.grabbedFormerParent = null;
    }

    openWindow(position: Vector3, rotation: Quaternion) {
        const window = new XrWindowObject(1.2, 0.8);
        window.quaternion.copy(rotation);
        window.position.copy(position);
        this.xrWindows.push(window);
        this.xrScene.add(window);
    }

    setMovement(direction: Vector3) {
        this.movement.copy(direction);
    }

    async updateControllers() {
        const sources = this.xr.getSession().inputSources;
        for (const controller of this.controllerObjects) {
            if (controller) {
                controller.dispose();
            }
        }
        this.controllerObjects = [];
        const me = this.characterService.characters.find(c => c.isMe);
        for (const [i, source] of sources.entries()) {
            if (!source.gamepad) {
                continue;
            }
            const parent = this.xr.getController(i);
            const hand = this.xr.getHand(i);
            // This has wrong typings, actually returns promise!
            const {profile, assetPath} = await fetchProfile(
                source,
                "/xrAssets"
            );
            const motionController = new MotionController(
                source, profile, assetPath
            );
            const controllerObject = new XrControllerObject(
                motionController, hand, this
            );
            this.controllerObjects[i] = controllerObject;
            parent.add(controllerObject);
            if (parent.parent) {
                parent.parent.remove(parent);
            }
            motionController.updateFromGamepad();
            this.xrScene.add(parent);
            // me.setHand(source.handedness, parent);
        }
    }

    async enterVR() {
        const xr = navigator.xr;
        if (!xr) {
            return;
        }
        if (!(await xr.isSessionSupported("immersive-vr"))) {
            this.toastService.error("immersive-vr not supported.");
            return;
        }

        try {
            const session = await xr.requestSession("immersive-vr", {
                requiredFeatures: []
            });
            session.addEventListener("select", this.selectFunc);
            session.addEventListener("selectstart", this.selectFunc);
            session.addEventListener("selectend", this.selectFunc);
            session.addEventListener("squeeze", this.squeezeFunc);
            session.addEventListener("squeezestart", this.squeezeFunc);
            session.addEventListener("squeezeend", this.squeezeFunc);
            session.addEventListener("inputsourceschange", this.inputSourceChangeFunc);
            this.xr.enabled = true;
            await this.xr.setSession(session);
            const referenceSpace = this.xr.getReferenceSpace();
            this.baseReferenceSpace = referenceSpace;
            referenceSpace.addEventListener("reset", this.referenceSpaceChangeFunc);
            this.cameraService.setCameraMode(CameraMode.VR_CAMERA);
            this.characterService.showMe(false);
            const me = this.characterService.characters.find(c => c.isMe);
            me.setHead(this.cameraService.camera);
            return true;
        } catch (e) {
            if (e.name === "InvalidStateError") {
                this.toastService.error("VR session already active.");
            } else if (e.name === "NotSupportedError") {
                console.error(e);
                this.toastService.error("No VR compatible device found.");
            } else if (e.name === "SecurityError") {
                this.toastService.error("Security error: VR blocked");
            } else {
                this.toastService.error(`${e}`);
            }
            return false;
        }
    }

    async exitVR() {
        const session = this.xr.getSession();
        session.removeEventListener("select", this.selectFunc);
        session.removeEventListener("selectstart", this.selectFunc);
        session.removeEventListener("selectend", this.selectFunc);
        session.removeEventListener("squeeze", this.squeezeFunc);
        session.removeEventListener("squeezestart", this.squeezeFunc);
        session.removeEventListener("squeezeend", this.squeezeFunc);
        session.removeEventListener("inputsourceschange", this.inputSourceChangeFunc);
        this.xr.getReferenceSpace()
            .removeEventListener("reset", this.referenceSpaceChangeFunc);
        this.disposeXRControllers();
        await this.xr.getSession().end();
        this.xr.enabled = false;
        this.cameraService.setCameraMode(CameraMode.WALK_LOCK_CAMERA);
        this.characterService.showMe(true);
        return true;
    }

    disposeXRControllers() {
        for (const controller of this.controllerObjects) {
            if (controller) {
                controller.dispose();
            }
        }
    }

    async recenter() {
        await this.exitVR();
        await this.enterVR();
    }

    update(delta: number) {
        if (!this.xr.enabled) {
            return;
        }
        const transform = new XRRigidTransform(this.cameraService.camera.position);
        this.xr.setReferenceSpace(
            this.baseReferenceSpace.getOffsetReferenceSpace(transform.inverse)
        );
        this.processXrGamepads();
        for (const [i, controller] of this.controllerObjects.entries()) {
            if (controller) {
                controller.update(this.xr.getController(i));
            }
        }
        if (this.movement.length() > 0) {
            this.movementVec.copy(this.movement);
            this.movementVec.multiplyScalar(delta * MOVEMENT_SPEED);
            this.movementVec.add(this.cameraService.camera.position);
            this.socketService.teleport(this.movementVec);
        }
    }

    private findParentItemObject(child: Object3D): ItemObject | undefined {
        if (!child) {
            return;
        }
        if ((child as ItemObject).isItemObject) {
            return child as ItemObject;
        }
        return this.findParentItemObject(child.parent);
    }

    showAvatarOptions() {
        const me = this.characterService.myCharacterMesh;
        this.cameraService.camera.getWorldPosition(me.position);
        const direction = this.xr.getCamera().getWorldDirection(new Vector3());
        me.position.set(
            me.position.x + direction.x,
            me.position.y,
            me.position.z + direction.z
        );
        if (!me.parent) {
            this.xrScene.add(me);
        }
    }

    hideAvatarOptions() {
        this.xrScene.remove(this.characterService.myCharacterMesh);
    }
}
