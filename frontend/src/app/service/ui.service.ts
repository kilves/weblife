import {Injectable} from "@angular/core";
import {GrabService} from "./grab.service";
import {MathService} from "./math.service";
import {Raycaster, Vector2} from "three";
import {CameraService} from "./camera.service";

@Injectable()
export class UIService {
    private element: Element;
    private raycaster = new Raycaster();

    constructor(private grabService: GrabService,
                private math: MathService,
                private cameraService: CameraService) {}

    init(element: HTMLElement) {
        this.element = element;
        this.element.addEventListener("mousemove", this.mousemove.bind(this));
    }

    mousemove(event: MouseEvent) {
        this.grabService.update(this.math.getNormalizedScreenPosition(new Vector2(event.clientX, event.clientY)), this.cameraService.camera);
    }
}