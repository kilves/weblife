import {Injectable} from "@angular/core";
import {Controls} from "../../3d/world/input/controls";
import {
    AmbientLight,
    DirectionalLight,
    Group, Object3D,
    PCFSoftShadowMap, PerspectiveCamera,
    ReinhardToneMapping,
    Scene,
    Vector2, Vector3,
    WebGLRenderer,
    WebXRManager,
} from "three";
import {CameraMode, CameraService} from "./camera.service";
import {CharacterService} from "./character.service";
import {CellService} from "./cell.service";
import {MoveReceiver} from "../../3d/world/input/move-receiver";
import {EffectService} from "./effect.service";
import {GLTFLoader} from "three/examples/jsm/loaders/GLTFLoader";
import {Avatar} from "../model/avatar";
import {DebugService} from "./debug.service";
import {ToastService} from "./toast.service";
import {XrControllerObject} from "../../3d/world/xr/xr-controller-object";
import {XRControllerModel} from "three/examples/jsm/webxr/XRControllerModelFactory";
import {XRService} from "./xr.service";

@Injectable()
export class SceneService {

    canvas: HTMLCanvasElement;
    controls: Controls;
    renderer: WebGLRenderer;
    scene: Scene;
    last = 0;
    light: DirectionalLight;
    moveReceiver = new MoveReceiver();
    private preview: Group;

    // FPS
    fpsLastUpdated = 0;
    fps: number;
    frames = 0;
    framesTime = 0;

    constructor(private cameraService: CameraService,
                private characterService: CharacterService,
                private cellService: CellService,
                private effectService: EffectService,
                private debugService: DebugService,
                private toastService: ToastService,
                private xrService: XRService) {
    }

    init(canvas: HTMLCanvasElement) {
        window.addEventListener("resize", this.resize.bind(this));
        this.canvas = canvas;
        this.controls = new Controls();
        this.controls.initialize(document.body);
        this.renderer = new WebGLRenderer({
            canvas: this.canvas,
            antialias: true,
            logarithmicDepthBuffer: true,
        });
        this.xrService.init(this.renderer);
        this.renderer.toneMapping = ReinhardToneMapping;
        this.renderer.shadowMap.enabled = true;
        this.renderer.shadowMap.type = PCFSoftShadowMap;
        this.scene = new Scene();
        this.scene.add(this.xrService.xrScene);
        this.debugService.scene = this.scene;
        const size = this.renderer.getSize(new Vector2());
        this.cameraService.init(size.x, size.y);
        this.effectService.init(this.renderer, this.scene);
    }

    initScene() {
        this.light = new DirectionalLight(0xfff9fd, 7.5);
        this.light.castShadow = true;
        this.light.shadow.bias = -0.0003;
        this.light.shadow.radius = 4;
        this.light.shadow.camera.near = 10;
        this.light.shadow.camera.far = 250;
        this.light.shadow.camera.left = -100;
        this.light.shadow.camera.right = 100;
        this.light.shadow.camera.top = 100;
        this.light.shadow.camera.bottom = -100;
        this.light.shadow.mapSize.width = 32768;
        this.light.shadow.mapSize.height = 32768;
        this.light.rotateX(Math.PI / 4);
        this.light.position.set(50, 50, 50);
        const ambientLight = new AmbientLight(0xcae6e9, 1.0);
        this.scene.add(ambientLight);
        this.scene.add(this.light);
        this.scene.add(this.cameraService.camera);
        this.controls.addReceiver(this.moveReceiver);
        this.resize();
        this.renderer.setAnimationLoop(this.update.bind(this));
    }

    update(time: number) {
        let now = time / 1000;
        let delta = now - this.last;
        this.last = now;
        this.characterService.update(delta);
        this.cameraService.update(delta);
        this.cellService.update(delta);
        this.xrService.update(delta);
        // this.effectService.update(delta);
        this.renderer.render(this.scene, this.cameraService.camera);

        this.frames += 1;
        this.framesTime += delta;
        if (this.fpsLastUpdated < now - 0.3) {
            this.fps = this.frames / this.framesTime;
            this.frames = 0;
            this.framesTime = 0;
            this.fpsLastUpdated = now;
        }
    }

    resize() {
        let w = this.canvas.parentElement.clientWidth;
        let h = this.canvas.parentElement.clientHeight;
        this.renderer.setSize(w, h);
        this.effectService.setSize(w, h);
        this.cameraService.camera.setSize(w, h);
    }

    showPreview(avatar: Avatar) {
        if (!avatar) {
            return;
        }
        const gltfLoader = new GLTFLoader();
        gltfLoader.load(`/avatar/${avatar.id}`, avatarModel => {
            this.preview = avatarModel.scene;
            this.scene.add(this.preview);
        });
    }

    hidePreview() {
        if (this.preview) {
            this.scene.remove(this.preview);
        }
    }
}
