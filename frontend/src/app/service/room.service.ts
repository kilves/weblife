import {Injectable} from "@angular/core";
import {Router} from "@angular/router";
import {SocketService} from "./socket.service";
import {CellService} from "./cell.service";
import {SceneService} from "./scene.service";
import {AvatarService} from "./avatar.service";

@Injectable()
export class RoomService {
    room: string;
    noRoomProvided: boolean = true;
    roomNotFound: boolean;
    roomShouldLoad: boolean = false;

    constructor(private route: Router,
                private socketService: SocketService,
                private cellService: CellService,
                private sceneService: SceneService,
                private avatarService: AvatarService) {
    }

    loadRoom(roomName: string) {
        if (!this.avatarService.avatar.getValue()) {
            // Defer loading room until avatar is selected
            this.avatarService.avatar.subscribe(avatar => {
                if (avatar) {
                    return this.loadRoom(roomName);
                }
            });
            return;
        }
        if (!roomName) {
            this.noRoomProvided = true;
            return;
        }
        this.room = roomName;
        this.noRoomProvided = false;
        this.roomNotFound = false;
        this.cellService.loadCell(roomName).then(() => {
            this.socketService.join(this.room, this.avatarService.avatar.getValue());
            this.sceneService.scene.add(this.cellService.cellObject);
        }).catch(error => {
            this.noRoomProvided = true;
            if (error.status === 404) {
                this.noRoomProvided = true;
                this.roomNotFound = true;
            }
        });
    }
}
