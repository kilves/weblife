import {EventEmitter, Injectable} from "@angular/core";
import {Observable, of} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {Inventory} from "../model/inventory";
import {GameObjectCreate} from "../model/gameObjectCreate";
import {CellService} from "./cell.service";
import {PromiseClient} from "./promise-client";

@Injectable({
    providedIn: "root"
})
export class GameObjectService {

    get apiUrl(): string {
        return `${this.cellService.apiUrl}/${this.cellService.cell.name}`;
    }

    static instance: GameObjectService;
    itemCreated: EventEmitter<void> = new EventEmitter<void>();

    constructor(private http: HttpClient,
                private promiseClient: PromiseClient,
                private cellService: CellService) {
        GameObjectService.instance = this;
    }

    getInventory(): Observable<Inventory> {
        return of({
            items: []
        });
    }

    setModel(objectId: number, resourceUuid: string) {
        return this.promiseClient.put(`${this.apiUrl}/objects/${objectId}/setModel/${resourceUuid}`, {});
    }

    create(object: GameObjectCreate): Observable<void> {
        return new Observable<void>(subscriber => {
            return this.http.post<void>(`${this.apiUrl}/objects`, object).subscribe(() => {
                this.itemCreated.emit();
                subscriber.next();
            });
        });
    }
    destroy(uuid: string): Observable<void> {
        return this.http.delete<void>(`${this.apiUrl}/objects/${uuid}`);
    }
}
