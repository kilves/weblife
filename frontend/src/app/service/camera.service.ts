import {Injectable} from "@angular/core";
import {WalkLockCamera} from "../../3d/walk-lock-camera";
import {GameCamera} from "../../3d/game-camera";
import {Camera, Object3D} from "three";
import {NameLabelService} from "./name-label.service";
import {VRCamera} from "../../3d/vr-camera";

export enum CameraMode {
    WALK_LOCK_CAMERA,
    VR_CAMERA
}

@Injectable()
export class CameraService {

    walkLockCamera: WalkLockCamera;
    vrCamera: VRCamera;
    static instance: CameraService;

    private _camera: GameCamera;
    get camera(): GameCamera {
        return this._camera;
    }

    setCameraMode(mode: CameraMode) {
        if (mode === CameraMode.WALK_LOCK_CAMERA) {
            this._camera = this.walkLockCamera;
        } else if (mode === CameraMode.VR_CAMERA) {
            this._camera = this.vrCamera;
        }
    }

    constructor(private nameLabelService: NameLabelService) {
        CameraService.instance = this;
    }

    init(width, height) {
        this.walkLockCamera = new WalkLockCamera(
            70,
            width,
            height,
            0.01, 1000,
        );
        this.vrCamera = new VRCamera(
            70,
            width,
            height,
            0.01, 1000,
        );
        this._camera = this.walkLockCamera;
    }

    update(delta: number) {
        this.camera.update(delta);
        this.nameLabelService.update(this.camera);
    }

    zoomCamera(delta: number) {
        if (this.walkLockCamera instanceof WalkLockCamera) {

        }
        this.walkLockCamera.zoomCamera(delta);
    }

    startRotating(x: number, y: number) {
        this.walkLockCamera.startRotating(x, y);
    }

    stopRotating() {
        this.walkLockCamera.stopRotating();
    }

    rotate(x: number, y: number) {
        this.walkLockCamera.rotate(x, y);
    }

    setTarget(target: Object3D) {
        this.vrCamera.target = target;
        this.walkLockCamera.target = target;
    }
}
