import {Injectable} from "@angular/core";
import {PromiseClient} from "./promise-client";

@Injectable({
    providedIn: "root",
})
export class MeshService {

    private apiUrl = "/api/resources/meshes";

    constructor(private http: PromiseClient) {
    }

    async createModel(formData: FormData) {
        await this.http.post(`${this.apiUrl}`, formData);

    }
}
