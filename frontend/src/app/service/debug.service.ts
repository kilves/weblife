import {Injectable} from "@angular/core";
import {SceneService} from "./scene.service";
import {ArrowHelper, Box3, Box3Helper, Mesh, MeshBasicMaterial, Object3D, Ray, Scene, Sphere, SphereGeometry, Vector3} from "three";
import {LineGeometry} from "three/examples/jsm/lines/LineGeometry";

@Injectable({
    providedIn: "root",
})
export class DebugService {

    objects: Map<string, Object3D> = new Map<string, Object3D>();
    scene: Scene;

    remove(name: string) {
        if (this.objects.has(name)) {
            this.scene.remove(this.objects.get(name));
        }
        this.objects.delete(name);
    }

    box3(box: Box3, name: string) {
        const helper = new Box3Helper(box);
        if (this.objects.has(name)) {
            this.scene.remove(this.objects.get(name));
        }
        this.scene.add(helper);
        this.objects.set(name, helper);
    }

    ray(ray: Ray, name: string) {
        const helper = new ArrowHelper(ray.direction, ray.origin, 10000);
        if (this.objects.has(name)) {
            this.scene.remove(this.objects.get(name));
        }
        this.scene.add(helper);
        this.objects.set(name, helper);
    }

    point(pos: Vector3, name: string) {
        const helper = new Mesh(
            new SphereGeometry(0.1),
            new MeshBasicMaterial({
                color: 0xff00ff,
                depthTest: false
            })
        )
        helper.position.copy(pos);
        if (this.objects.has(name)) {
            this.scene.remove(this.objects.get(name));
        }
        this.scene.add(helper);
        this.objects.set(name, helper);
    }

    sphere(sphere: Sphere, name: string) {
        const helper = new Mesh(
            new SphereGeometry(sphere.radius),
            new MeshBasicMaterial({
                color: 0xff00ff,
                depthTest: false,
                wireframe: true,
            })
        );
        helper.position.copy(sphere.center);
        if (this.objects.has(name)) {
            this.scene.remove(this.objects.get(name));
        }
        this.scene.add(helper);
        this.objects.set(name, helper);
    }
}
