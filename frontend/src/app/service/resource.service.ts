import {Injectable} from "@angular/core";
import {PromiseClient} from "./promise-client";
import {Resource, ResourceType} from "../model/resource";
import {BehaviorSubject} from "rxjs";
import {ToastService} from "./toast.service";

@Injectable()
export class ResourceService {

    private apiUrl = "/api/resources";
    resources: BehaviorSubject<Resource[]> = new BehaviorSubject<Resource[]>([]);
    models: BehaviorSubject<Resource[]> = new BehaviorSubject<Resource[]>([]);

    constructor(private http: PromiseClient,
                private toastService: ToastService) {}

    listMy() {
        return this.http.get<Resource[]>(`${this.apiUrl}/my`);
    }

    async delete(resource: Resource) {
        try {
            await this.http.delete(`${this.apiUrl}/${resource.uuid}`);
        } catch (e) {
            if (e.error.errorId === "RESOURCE_BEING_USED") {
                this.toastService.error("Mesh is being used by an object");
                return;
            }
            throw e;
        }
        await this.updateResources();
    }

    async updateResources() {
        const resources = await this.listMy();
        this.resources.next(resources);
        this.models.next(resources.filter(r => r.type === ResourceType.OBJECT3D));
    }
}
