import {EventEmitter, Injectable} from "@angular/core";
import {Object3D, PerspectiveCamera, Vector2, Vector3} from "three";
import {CharacterObject} from "../../3d/world/character-object";
import {DebugService} from "./debug.service";
import {CameraService} from "./camera.service";

export class NameLabelObject extends Object3D {
    label: string;
    chatMessage: string;

    constructor(label: string) {
        super();
        this.label = label;
    }
}

export interface NameLabel {
    position: Vector2;
    name: string;
    chatMessage: string;
}

@Injectable()
export class NameLabelService {
    private labelObjects: NameLabelObject[] = [];
    labels: NameLabel[] = [];

    constructor(private debugService: DebugService) {
    }


    add(character: CharacterObject) {
        const label = new NameLabelObject(character.name);
        character.add(label);
        label.position.set(0, 2.4, 0);
        this.labelObjects.push(label);
    }

    showMessage(sender: string, message: string) {
        const nameLabel = this.labelObjects.find(l => l.label === sender);
        if (nameLabel) {
            nameLabel.chatMessage = message;
            setTimeout(() => {
                nameLabel.chatMessage = null;
            }, 1500 + message.length * 50);
        }
    }

    remove(character: CharacterObject) {
        const object = this.labelObjects.find(l => l.parent === character);
        this.labelObjects = this.labelObjects.filter(l => l.parent !== character);
        character.remove(object);
    }

    update(camera: PerspectiveCamera) {
        this.labels = [];
        for (let labelObject of this.labelObjects) {
            const pos = labelObject.getWorldPosition(new Vector3());
            pos.project(camera);
            if (pos.z > 1) {
                continue;
            }
            this.labels.push({
                position: new Vector2(pos.x, pos.y),
                name: labelObject.label,
                chatMessage: labelObject.chatMessage
            });
        }
    }
}
