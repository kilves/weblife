import {Injectable, Type} from "@angular/core";
import {MenuData, MenuOption} from "../model/menu-option";
import {Vector2} from "three";
import {CharacterObject} from "../../3d/world/character-object";
import {CellObject} from "../../3d/world/cell-object";
import {ItemObject} from "../../3d/world/item-object";
import {GameObjectService} from "./game-object.service";
import {SocketService} from "./socket.service";
import {WindowService} from "./window.service";
import {CellService} from "./cell.service";
import {EditObjectComponent} from "../component/edit-object/edit-object.component";
import {EffectService} from "./effect.service";
import {EditMapComponent} from "../component/edit-map/edit-map.component";
import {CreateItemComponent} from "../component/create-item/create-item.component";
import {EditTerrainService} from "./edit-terrain.service";
import {SelectObjectResourceComponent} from "../component/select-object-resource/select-object-resource.component";
import {ProfileWindowComponent} from "../component/profile-window/profile-window.component";

@Injectable({
    providedIn: "root"
})
export class ContextMenuService {

    posX: number = 0;
    posY: number = 0;
    private menuOptions = new Map<Type<any>, MenuData>([
        [CharacterObject, {
            header: () => "Character",
            options: [
                {
                    id: "profile",
                    name: "Profile",
                    closeOnClick: true,
                    action: async (customData?: {username: string, me: boolean}) => {
                        await this.windowService.create(ProfileWindowComponent, {
                            name: customData.me ? "Your profile" : "Profile",
                            destroyOnClose: true,
                            centerOnCreate: true,
                            autoScale: true,
                            customData: {
                                username: customData.username,
                                me: customData.me
                            }
                        });
                        this.windowService.open(ProfileWindowComponent);
                    },
                },
                {
                    id: "edit",
                    name: "Edit character",
                    action: () => {
                    },
                },
                {
                    id: "whisper",
                    name: "Whisper",
                    closeOnClick: true,
                    action: (customData?: {username: string}) => {
                        this.windowService.openWhisper(customData.username);
                    },
                },
            ]
        }],
        [CellObject, {
            header: () => "Area",
            options: [
                {
                    id: "spawn",
                    name: "Create object here",
                    closeOnClick: true,
                    action: (customData?: any) => {
                        this.gameObjectService.create({
                            x: customData.position.x,
                            y: customData.position.y,
                            z: customData.position.z,
                            name: "New item",
                        }).subscribe();
                    },
                },
                {
                    id: "editmap",
                    name: "Edit terrain",
                    closeOnClick: true,
                    action: async () => {
                        await this.windowService.create(EditMapComponent, {
                            destroyOnClose: true,
                            name: "Edit terrain",
                            heightEm: 20,
                        });
                        this.windowService.open(EditMapComponent);
                    }
                },
                {
                    id: "teleport",
                    name: "Teleport",
                    closeOnClick: true,
                    action: ((customData?: any) => {
                        this.socketService.teleport(customData.position);
                    }),
                },
            ]
        }],
        [ItemObject, {
            header: (customData) => customData.name || "Item",
            options: [
                {
                    id: "delete",
                    name: "Delete",
                    closeOnClick: true,
                    action: (customData?: any) => {
                        this.gameObjectService.destroy(customData.uuid).subscribe();
                    },
                },
                {
                    id: "edit-object",
                    name: "Edit",
                    closeOnClick: true,
                    action: async (customData?: any) => {
                        this.effectService.hideOutlines();
                        await this.windowService.create(EditObjectComponent, {
                            name: `Editing ${customData.name || "item"}`,
                            customData: customData,
                            destroyOnClose: true,
                            centerOnCreate: true,
                        });
                        this.windowService.open(EditObjectComponent);
                    }
                },
                {
                    id: "edit-model",
                    name: "Change model",
                    closeOnClick: true,
                    action: async (customData?: any) => {
                        await this.windowService.create(SelectObjectResourceComponent, {
                            name: `Select model for ${customData.name}`,
                            customData: customData,
                            destroyOnClose: true,
                            centerOnCreate: true
                        });
                        this.windowService.open(SelectObjectResourceComponent);                    }
                }
            ]
        }],
    ]);


    private _open: boolean;
    menuData: MenuData;
    options: MenuOption[];
    customData: any;
    get isOpen(): boolean {
        return this._open;
    }

    constructor(private gameObjectService: GameObjectService,
                private cellService: CellService,
                private socketService: SocketService,
                private windowService: WindowService,
                private effectService: EffectService) {
    }

    update(mousePos: Vector2) {
        if (this._open) {
            return;
        }
        this.posX = mousePos.x;
        this.posY = mousePos.y;
    }
    
    open(object: any, customData?: any) {
        this.menuData = this.menuOptions.get(object.constructor);
        this.customData = customData;
        this._open = true;
    }

    close() {
        this._open = false;
    }
}
