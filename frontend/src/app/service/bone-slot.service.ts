import {Injectable} from "@angular/core";
import {PromiseClient} from "./promise-client";
import {BoneSlot} from "../model/boneSlot";

@Injectable({
    providedIn: "root",
})
export class BoneSlotService {

    apiUrl = "/api/boneSlots";

    constructor(private http: PromiseClient) {
    }

    listAll() {
        return this.http.get<BoneSlot[]>(this.apiUrl);
    }
}
