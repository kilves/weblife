import {Injectable, Type} from "@angular/core";
import {Object3D, Vector2} from "three";
import * as uuid from "uuid";

@Injectable({
    providedIn: "root"
})
export class MathService {

    constructor() {
    }

    getNormalizedScreenPosition(windowPosition: Vector2): Vector2 {
        return new Vector2(
            (windowPosition.x / window.innerWidth) * 2 - 1,
            1 - (windowPosition.y / window.innerHeight) * 2
        );
    }
    
    findClosestParentByKey<T>(object: Object3D, key: string): T {
        if (object[key]) {
            return object as any as T;
        }
        if (object.parent === null) {
            return;
        }
        return this.findClosestParentByKey<T>(object.parent, key);
    }
}
