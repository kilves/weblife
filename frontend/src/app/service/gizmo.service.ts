import {Injectable} from "@angular/core";
import {Grabbable} from "../../3d/world/grabbable";
import {GizmoObject} from "../../3d/world/gizmo-object";

@Injectable()
export class GizmoService {

    grabbables: Grabbable[] = [];

    addGizmo(gizmo: GizmoObject) {
        this.grabbables.push(...gizmo.handles);
    }

    removeGizmo(gizmo: GizmoObject) {
        this.grabbables = this.grabbables.filter(g => {
            for (let arrow of gizmo.handles) {
                if (arrow === g) {
                    return false;
                }
            }
            return true;
        });
    }
}