import {Injectable} from "@angular/core";
import {Matrix4, Raycaster, SkinnedMesh, Vector2, Vector3} from "three";
import {WorldObject} from "../../3d/world/worldObject";
import {ItemObject} from "../../3d/world/item-object";
import {CellObject} from "../../3d/world/cell-object";
import {ContextMenuTargetService} from "./context-menu-target.service";
import {CameraService} from "./camera.service";
import {MathService} from "./math.service";
import {ContextMenuService} from "./context-menu.service";
import {CellService} from "./cell.service";
import {EffectService} from "./effect.service";
import {CharacterObject} from "../../3d/world/character-object";
import {CharacterService} from "./character.service";
import {DebugService} from "./debug.service";
import {BrushConfig} from "../model/brush";
import {UserService} from "./user.service";
import {fixBoundingShapes} from "../../3d/utility/util";

@Injectable({
    providedIn: "root",
})
export class RaycastService {

    raycaster: Raycaster;

    constructor(private contextMenuTargetService: ContextMenuTargetService,
                private cameraService: CameraService,
                private math: MathService,
                private menuService: ContextMenuService,
                private effectService: EffectService,
                private cellService: CellService,
                private userService: UserService,
                private debugService: DebugService) {
        this.raycaster = new Raycaster(
            new Vector3(0, 0, 0),
            new Vector3(1, 0, 0),
            0.1,
            1000
        );
    }

    raycastBrush(position: Vector2, config: BrushConfig) {
        this.raycaster.setFromCamera(position, this.cameraService.camera);
        const intersects = this.raycaster.intersectObject(this.cellService.cellObject);
        if (intersects.length > 0) {
            this.cellService.cellObject.setBrush(intersects[0].point, config);
        } else {
            this.cellService.cellObject.clearBrush();
        }
    }

    raycastContextMenu(position: Vector2) {
        this.raycaster.setFromCamera(position, this.cameraService.camera);

        const playerTargets = this.contextMenuTargetService.targets
            .filter(target => target instanceof CharacterObject)
            .values();

        for (let playerTarget of playerTargets) {
            fixBoundingShapes(playerTarget);
        }

        // Right-click on context menu target
        (this.raycaster as any).contextCaster = true;
        let intersects = this.raycaster.intersectObjects(this.contextMenuTargetService.targets, true);
        for (let intersect of intersects) {
            let object = intersect.object as WorldObject;
            let parent = this.math.findClosestParentByKey<WorldObject>(object, "supportsContextMenu");
            if (parent) {
                let customData = {};
                if (parent instanceof ItemObject) {
                    customData = {
                        uuid: parent.item.uuid,
                        id: parent.item.id,
                        name: parent.item.name,
                    };
                }
                if (parent instanceof CharacterObject) {
                    const user = this.userService.user.getValue();
                    if (!user) {
                        return;
                    }
                    customData = {
                        username: parent.name,
                        me: parent.name === user.username
                    };
                }
                this.menuService.open(parent, customData);
                this.effectService.showOutline(parent);
                return;
            }
        }
        (this.raycaster as any).contextCaster = false;

        // Right-click on terrain
        let terrainIntersects = this.raycaster.intersectObject(this.cellService.cellObject.plane, true);
        if (terrainIntersects.length > 0) {
            const intersect = terrainIntersects[0];
            const position = intersect.point;
            const object = this.math.findClosestParentByKey<CellObject>(intersect.object, "isCellObject");

            this.menuService.open(object, {
                position: position.clone()
            })
        }
    }
}
