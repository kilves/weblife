import {Injectable} from "@angular/core";
import { Cell } from "../model/cell";
import {Intersection, Raycaster, Vector2} from "three";
import {GameObjectCreate} from "../model/gameObjectCreate";
import {PromiseClient} from "./promise-client";
import {CellObject} from "../../3d/world/cell-object";
import {ItemObject} from "../../3d/world/item-object";
import {MathService} from "./math.service";
import {CameraService} from "./camera.service";
import {GizmoService} from "./gizmo.service";
import {Resource} from "../model/resource";

@Injectable()
export class CellService {
    apiUrl = "/api/cells";

    private _cell: Cell;
    get cell(): Cell {
        return this._cell;
    }
    cellObject: CellObject = new CellObject();
    raycaster: Raycaster = new Raycaster();

    constructor(private http: PromiseClient,
                private math: MathService,
                private cameraService: CameraService,
                private gizmoService: GizmoService) {
    }
    
    async find(name: string) {
        return this.http.get<Cell>(`${this.apiUrl}/${name}`);
    }

    async updateCellToServer() {
        const blob = await this.cellObject.getHeightmap();
        const request = new FormData();
        request.append("heightmap", blob);
        return this.http.put(`${this.apiUrl}/${this._cell.name}`, request)
    }

    async loadCell(name: string) {
        this._cell = await this.find(name);
        await this.cellObject.load(this._cell);
    }
    
    async spawnAtScreen(screenCoordinates: Vector2, item: GameObjectCreate) {
        this.cellObject.hidePreview();
    }
    
    showSpawnPreview(screenCoordinates: Vector2, item: Resource) {
        let intersect = this.raycastGround(screenCoordinates);
        if (intersect) {
            this.cellObject.setPreviewPosition(intersect.point);
        }
    }

    loadSpawnPreview(item: GameObjectCreate) {
        this.cellObject.loadPreview(item);
    }

    spawnObject(item: ItemObject) {
        this.gizmoService.addGizmo(item.moveGizmo);
        this.gizmoService.addGizmo(item.rotateGizmo);
        this.gizmoService.addGizmo(item.scaleGizmo);
        this.cellObject.spawnItem(item);
    }

    removeObject(item: ItemObject) {
        this.gizmoService.removeGizmo(item.moveGizmo);
        this.gizmoService.removeGizmo(item.rotateGizmo);
        this.gizmoService.removeGizmo(item.scaleGizmo);
        this.cellObject.removeItem(item);
    }

    findObject(uuid: string): ItemObject {
        return this.cellObject.items.find(i => i.item.uuid === uuid);
    }

    update(delta: number) {
        this.cellObject.update(delta);
    }

    private raycastGround(position: Vector2): Intersection {
        this.raycaster.setFromCamera(position, this.cameraService.camera);
        let intersects = this.raycaster.intersectObject(this.cellObject.plane);
        if (intersects.length > 0) {
            return intersects[0];
        }
    }
}
