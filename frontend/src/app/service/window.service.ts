import {
    ApplicationRef,
    ComponentFactoryResolver, ComponentRef,
    Injectable,
    Injector,
    Type,
    ViewContainerRef
} from "@angular/core";
import {Observable, Subject} from "rxjs";
import {WindowComponent, WindowOptions} from "../component/window/window.component";
import {ChatComponent} from "../component/chat/chat.component";
import {SocketService} from "./socket.service";
import {WindowClient} from "../window-client";

@Injectable()
export class WindowService {

    private windows: Map<Type<any>, ComponentRef<WindowComponent<any>>> = new Map<Type<any>, ComponentRef<WindowComponent<any>>>();
    windowContainer: ViewContainerRef;
    
    constructor(private componentFactoryResolver: ComponentFactoryResolver,
                private applicationRef: ApplicationRef,
                private socketService: SocketService,
                private injector: Injector) {
    }

    openWhisper(username: string) {
        const chatComponent = this.open(ChatComponent);
        const channelData = {
            name: username,
            private: true
        };
        const channel = chatComponent.getChannel(channelData);
        if (!channel) {
            chatComponent.addChannel(channelData);
        }
        chatComponent.message.channel = channelData;
    }

    open<T extends WindowClient>(windowType: Type<T>, customData?: any): T {
        let windowRef = this.getWindow(windowType);
        if (!windowRef.instance.active) {
            windowRef.instance.open();
        }
        return windowRef.instance.componentRef.instance;
    }
    
    close<T extends WindowClient>(windowType: Type<T>) {
        let windowRef = this.getWindow(windowType);
        let window = windowRef.instance;
        window.componentRef.instance.onClose();
        if (window.options.destroyOnClose) {
            this.windows.delete(windowType);
            windowRef.destroy();
        }
        window.close();
    }
    
    toggle<T extends WindowClient>(windowType: Type<T>) {
        let windowRef = this.getWindow(windowType);
        if (windowRef.instance.active) {
            this.close(windowType);
        } else {
            windowRef.instance.componentRef.instance.onWindowOpened();
            this.open(windowType);
        }
    }

    async create<T extends WindowClient>(component: Type<T>, options?: WindowOptions) {
        if (this.windows.has(component)) {
            return;
        }
        return new Promise<void>(resolve => {
            const componentFactory = this.componentFactoryResolver.resolveComponentFactory(WindowComponent);
            let componentRef = componentFactory.create(this.injector);
            componentRef.instance.component = component;
            componentRef.instance.options = options;
            componentRef.instance.created.subscribe(() => {
                resolve();
            });
            componentRef.instance.closed.subscribe(() => {
                this.close(componentRef.instance.component);
            });
            this.windows.set(component, componentRef);
            this.windowContainer.insert(componentRef.hostView);  
        });
    }
    
    private getWindow<T extends WindowClient>(windowType: Type<T>): ComponentRef<WindowComponent<T>> {
        return this.windows.get(windowType);
    }
}
