import {HttpClient, HttpErrorResponse, HttpHeaders, HttpParams, HttpResponse} from "@angular/common/http";
import {Injectable} from "@angular/core";

export interface RequestOptions {
    headers?: HttpHeaders | {
        [header: string]: string | string[];
    };
    observe?: "body";
    params?: HttpParams | {
        [param: string]: string | string[];
    };
    reportProgress?: boolean;
    responseType?: "json";
    withCredentials?: boolean;
}

@Injectable()
export class PromiseClient {
    constructor(private http: HttpClient) {}
    
    async get<T>(url: string, options?: RequestOptions): Promise<T> {
        return this.http.get<T>(url, options).toPromise();
    }
    
    async post<T>(url: string, body: any, options?: RequestOptions): Promise<T> {
        return this.http.post<T>(url, body, options).toPromise();
    }
    
    async delete<T>(url: string, options?: RequestOptions): Promise<T> {
        return this.http.delete<T>(url, options).toPromise();
    }

    async put<T>(url: string, body: any, options?: RequestOptions): Promise<T> {
        return this.http.put<T>(url, body, options).toPromise();
    }
}
