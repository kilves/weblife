import {Injectable} from "@angular/core";
import {Avatar, FullAvatar} from "../model/avatar";
import {PromiseClient} from "./promise-client";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {UserService} from "./user.service";
import {BehaviorSubject, Observable} from "rxjs";
import {map} from "rxjs/operators";
import {Animation} from "../model/animation";

@Injectable({
    providedIn: "root",
})
export class AvatarService {

    apiUrl = "/api/avatars";
    avatar: BehaviorSubject<Avatar> = new BehaviorSubject<Avatar>(null);
    avatars: Avatar[] = [];

    constructor(private promiseClient: PromiseClient,
                private http: HttpClient) {
    }

    getAllAnimations() {
        return this.http.get<Animation[]>(`${this.apiUrl}/animations`);
    }

    get(id: number) {
        return this.promiseClient.get<FullAvatar>(`${this.apiUrl}/${id}`);
    }

    async updateAvatars() {
        await this.http.get<Avatar[]>(this.apiUrl).pipe(
            map(avatars => {
                this.avatars = avatars
            })
        ).toPromise();
    }

    createAvatar(formData: FormData): Observable<any> {
        return this.http.post(`${this.apiUrl}`, formData);
    }

    editAvatar(id: number, formData: FormData): Observable<any> {
        return this.http.put(`${this.apiUrl}/${id}`, formData)
    }
}
