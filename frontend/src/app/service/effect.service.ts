import {Injectable} from "@angular/core";
import {EffectComposer} from "three/examples/jsm/postprocessing/EffectComposer";
import {RenderPass} from "three/examples/jsm/postprocessing/RenderPass";
import {OutlinePass} from "three/examples/jsm/postprocessing/OutlinePass";
import {DoubleSide, NoBlending, NormalBlending, Object3D, Scene, Vector2, WebGLRenderer} from "three";
import {ShaderPass} from "three/examples/jsm/postprocessing/ShaderPass";
import {FXAAShader} from "three/examples/jsm/shaders/FXAAShader";
import {CameraService} from "./camera.service";
import {WorldObject} from "../../3d/world/worldObject";
import {CharacterObject} from "../../3d/world/character-object";

@Injectable()
export class EffectService {

    private composer: EffectComposer;
    private outlinePass: OutlinePass;
    private fxaaPass: ShaderPass;
    private fxaa = true;
    private scene: Scene;
    private size: Vector2;

    constructor(private cameraService: CameraService) {
    }

    refreshOutlinePass() {
        if (this.outlinePass) {
            this.composer.removePass(this.outlinePass);
        }
        this.outlinePass = new OutlinePass(this.size, this.scene, this.cameraService.camera);
        this.outlinePass.clear = true;
        // (this.outlinePass.depthMaterial as any).morphTargets = true;
        this.outlinePass.hiddenEdgeColor.set(0xffffff);
        this.outlinePass.visibleEdgeColor.set(0xffffff);
        this.composer.addPass(this.outlinePass);
    }

    init(renderer: WebGLRenderer, scene: Scene) {
        this.composer = new EffectComposer(renderer);
        this.composer.addPass(new RenderPass(scene, this.cameraService.camera));
        const size = renderer.getSize(new Vector2());
        this.size = size;
        this.scene = scene;
        this.refreshOutlinePass();
        if (this.fxaa) {
            this.fxaaPass = new ShaderPass(FXAAShader);
            let pixelRatio = renderer.getPixelRatio();
            this.fxaaPass.uniforms["resolution"].value.x = 1 / (size.x * pixelRatio);
            this.fxaaPass.uniforms["resolution"].value.y = 1 / (size.y * pixelRatio);
            this.composer.addPass(this.fxaaPass);
        }
    }

    update(delta: number) {
        this.composer.render();
    }

    setSize(w: number, h: number) {
        const pixelRatio = h / w;
        this.fxaaPass.uniforms["resolution"].value.x = 1 / (w * pixelRatio) / 2;
        this.fxaaPass.uniforms["resolution"].value.y = 1 / (h * pixelRatio) / 2;
        this.fxaaPass.setSize(h, w);
        this.outlinePass.setSize(w, h);
        this.composer.setSize(w, h);
        this.composer.removePass(this.fxaaPass);
        this.composer.addPass(this.fxaaPass);
        this.size = new Vector2(w, h);
    }

    showOutline(object: WorldObject) {
        this.hideOutlines();
        // Need to entirely reconstruct outline pass because it glitches otherwise
        this.refreshOutlinePass();
        this.outlinePass.selectedObjects.push(object);
    }

    hideOutlines() {
        this.outlinePass.selectedObjects.length = 0;
    }

    setFXAA(fxaa: boolean) {
        if (!fxaa) {
            this.composer.removePass(this.fxaaPass);
            this.composer.removePass(this.outlinePass);
        } else  {
            this.composer.addPass(this.fxaaPass);
            this.composer.addPass(this.outlinePass);
        }
    }
}
