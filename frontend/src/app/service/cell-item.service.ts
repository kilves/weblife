import {Injectable, Type} from "@angular/core";
import {CellItem} from "../model/gameObjectCreate";
import {ItemObject} from "../../3d/world/item-object";
import {SpawnerService} from "./spawner.service";
import {Quaternion} from "three";
import {CellService} from "./cell.service";
import {ContextMenuTargetService} from "./context-menu-target.service";

@Injectable({
    providedIn: "root"
})
export class CellItemService extends SpawnerService<CellItem, ItemObject> {

    constructor(private cellService: CellService,
                private contextMenuTargetService: ContextMenuTargetService) {
        super();
    }

    async updateEntity(item: CellItem, SpawnObject3D: Type<ItemObject>): Promise<ItemObject> {
        let object3D = await super.updateEntity(item, SpawnObject3D);
        object3D.object.rotation.setFromQuaternion(new Quaternion(
            item.rotation.x, item.rotation.y, item.rotation.z, item.rotation.w
        ));
        object3D.position.set(item.position.x, item.position.y, item.position.z);
        await object3D.updateResources(item.resources);
        return object3D;
    }

    async createEntity(item: CellItem, type: Type<ItemObject>): Promise<ItemObject> {
        const obj = await super.createEntity(item, type);
        await obj.load();
        this.contextMenuTargetService.add(obj);
        this.cellService.spawnObject(obj);
        return obj;
    }


    async destroyEntity(item: CellItem): Promise<ItemObject> {
        const obj = await super.destroyEntity(item);
        this.contextMenuTargetService.remove(obj);
        this.cellService.removeObject(obj);
        return obj;
    }
}
