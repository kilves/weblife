import {EventEmitter, Injectable} from "@angular/core";
import {Bone, Quaternion, Vector2, Vector3} from "three";
import {CharacterService} from "./character.service";
import {ChatMessage, ChatMessageSend} from "../model/chat-message";
import {CellItemService} from "./cell-item.service";
import {CharacterObject} from "../../3d/world/character-object";
import {ItemObject} from "../../3d/world/item-object";
import {CellItem} from "../model/gameObjectCreate";
import {UserService} from "./user.service";
import {User} from "../model/user";
import {Avatar} from "../model/avatar";
import {BoneSlot} from "../model/boneSlot";
import {MathService} from "./math.service";
import {SyncProtocolReader} from "../protocol/syncProtocolReader";

@Injectable()
export class SocketService {
    
    static instance: SocketService;
    ws: WebSocket;
    wsUrl: URL;
    user: User;
    channels: string[] = [];
    private boneBuffer = new Float32Array(7);
    private broadcastBuffer = new Uint8Array(10);
    private boneIdBuffer = new Uint32Array(1);
    private protocol: SyncProtocolReader = new SyncProtocolReader();

    onChatMessage: EventEmitter<ChatMessage> = new EventEmitter<ChatMessage>();
    
    constructor(private characterService: CharacterService,
                private cellItemService: CellItemService,
                private userService: UserService,
                private mathService: MathService) {
        SocketService.instance = this;
        this.wsUrl = new URL(window.location.href);
        this.wsUrl.protocol = this.wsUrl.protocol === "https:" ? "wss" : "ws";
        this.wsUrl.search = "";
        this.wsUrl.pathname = "/ws";
        this.userService.user.subscribe(user => {
            this.user = user;
        });
    }

    async connect(room: string) {
        return new Promise<void>((resolve, reject) => {
            this.wsUrl.pathname += `/${room}`
            this.ws = new WebSocket(this.wsUrl);
            this.ws.binaryType = "arraybuffer";
            this.ws.onopen = e => {};
            this.ws.onerror = err => reject(err);
            this.ws.onmessage = message => {
                if (message.data instanceof ArrayBuffer) {
                    const view = new DataView(message.data);
                    const messageType = view.getUint8(0);
                    if (messageType === 0x02) {
                        this.onCellUpdate(view);
                    }
                    return;
                }
                const data = JSON.parse(message.data);
                if (data.c === "ready") {
                    return resolve();
                }
                this.onMessage(data);
            }
        });
    }
    
    move(vec: Vector2) {
        this.ws.send(JSON.stringify({
            c: "move",
            x: vec.x,
            y: vec.y
        }));
    }

    teleport(pos: Vector3) {
        this.ws.send(JSON.stringify({
            c: "teleport",
            x: pos.x,
            y: pos.y,
            z: pos.z
        }));
    }

    
    jump() {
        this.ws.send(JSON.stringify({c: "jump"}));
    }

    addChatMessage(message: ChatMessage) {
        this.onChatMessage.emit(message);
    }
    
    chatMessage(message: ChatMessageSend) {
        this.ws.send(JSON.stringify({
            c: "say",
            message
        }));
    }
    
    join(cellName: string, avatar: Avatar) {
        this.ws.send(JSON.stringify({
            c: "join",
            cell: cellName,
            avatar: avatar.id,
            token: this.user.websocketToken
        }));
    }

    setItemPosition(item: CellItem, position: Vector3) {
        this.ws.send(JSON.stringify({
            c: "ipos",
            uuid: item.uuid,
            x: position.x,
            y: position.y,
            z: position.z
        }));
    }

    setItemRotation(item: CellItem, rotation: Quaternion) {
        this.ws.send(JSON.stringify({
            c: "irot",
            uuid: item.uuid,
            x: rotation.x,
            y: rotation.y,
            z: rotation.z,
            w: rotation.w
        }));
    }

    broadcastBones(bones: Bone[], slot: BoneSlot) {
        const headerLength = 2; // header byte + length
        const messageBytes = headerLength + bones.length * 8 * 4;
        if (this.broadcastBuffer.length !== messageBytes) {
            this.broadcastBuffer = new Uint8Array(messageBytes);
        }
        this.broadcastBuffer[0] = 0x01; // bones id
        this.broadcastBuffer[1] = bones.length;
        for (const [i, bone] of bones.entries()) {
            const offset = headerLength + i * 8 * 4;
            this.boneIdBuffer[0] = slot.id;
            this.boneBuffer[0] = bone.position.x;
            this.boneBuffer[1] = bone.position.y;
            this.boneBuffer[2] = bone.position.z;
            this.boneBuffer[3] = bone.quaternion.x;
            this.boneBuffer[4] = bone.quaternion.y;
            this.boneBuffer[5] = bone.quaternion.z;
            this.boneBuffer[6] = bone.quaternion.w;

            this.broadcastBuffer.set(this.boneIdBuffer, offset);
            this.broadcastBuffer.set(this.boneBuffer, offset + 1);
        }
        this.ws.send(this.broadcastBuffer);
    }
    
    onError(err: string) {
        console.error(err);
    }
    
    async onMessage(data: any) {
        if (data.c === "message") {
            this.addChatMessage({
                sender: data.sender,
                channel: data.channel,
                text: data.text,
            });
        } else if (data.c === "destroy") {
            await this.characterService.destroyEntity(data.character);
        } else if (data.c === "kick") {
            window.location.reload();
        } else if (data.c === "cell") {
            if (data.characters) {
                for (let character of data.characters) {
                    await this.characterService.updateEntity(character, CharacterObject);
                }
            }
            if (data.objects && data.objects.length > 0) {
                for (let cellObject of data.objects) {
                    await this.cellItemService.updateEntity(cellObject, ItemObject);
                }
            }
        } else if (data.c === "destroyObject") {
            if (data.object) {
                await this.cellItemService.destroyEntity(data.object);
            }
        } else if (data.c === "joinChannel") {
            this.channels.push(data.channel);
        } else if (data.c === "anim") {
            this.characterService.setAnimation(data.uuid, data.animation, data.action);
        }
    }

    async onCellUpdate(data: DataView) {
        let i = 1;
        const changes = this.protocol.readChanges(data);
        for (const c of changes.characters) {
            await this.characterService.updateEntity(c, CharacterObject);
        }
        for (const c of changes.items) {
            await this.cellItemService.updateEntity(c, ItemObject);
        }
    }
}
