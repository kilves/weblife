import {Injectable} from "@angular/core";
import {BrushConfig, BrushState} from "../model/brush";
import {RaycastService} from "./raycast.service";
import {Vector2} from "three";
import {CellService} from "./cell.service";

@Injectable({
    providedIn: "root",
})
export class EditTerrainService {
    private brush: BrushState = BrushState.NONE;
    private painting = false;
    private config: BrushConfig;

    constructor(private raycastService: RaycastService,
                private cellService: CellService) {
    }

    updateBrush(pos: Vector2) {
        if (this.brush !== BrushState.NONE) {
            this.raycastService.raycastBrush(pos, this.config);
        } else {
            this.cellService.cellObject.clearBrush();
        }
    }

    setBrush(brush: BrushState, config: BrushConfig) {
        this.brush = brush;
        this.config = config;
    }

    startPainting() {
        this.painting = true;
    }

    stopPainting() {
        this.painting = false;
        this.cellService.updateCellToServer().then(async () => {
            await this.cellService.loadCell(this.cellService.cell.name);
        });
    }

    updatePainting() {
        if (this.painting) {
            this.cellService.cellObject.paint(this.brush);
        }
    }

    isEnabled() {
        return this.brush !== BrushState.NONE;
    }
}
