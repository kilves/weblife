import {Injectable} from "@angular/core";
import {Box3, Camera, Mesh, MeshStandardMaterial, Ray, Raycaster, Sphere, Vector2, Vector3} from "three";
import {Grabbable} from "../../3d/world/grabbable";
import {MoveGizmoObject} from "../../3d/world/move-gizmo-object";
import {SceneService} from "./scene.service";
import {GizmoService} from "./gizmo.service";
import {rayPointClosestToRay} from "../../3d/utility/util";

@Injectable()
export class GrabService {
    private _hovered: Grabbable;
    private grabbed: Grabbable;
    private grabbedPosition: Vector3 = new Vector3();
    private startMousePosition: Vector2 = new Vector2();
    private currentMousePosition: Vector2 = new Vector2();
    private raycaster: Raycaster = new Raycaster();

    constructor(private sceneService: SceneService,
                private gizmoService: GizmoService) {
    }

    get active(): boolean {
        return !!this._hovered || !!this.grabbed;
    }

    get hovered(): Grabbable {
        return this._hovered;
    }

    set hovered(hovered: Grabbable) {
        if (this._hovered && (hovered === null || hovered !== this._hovered)) {
            this._hovered.setHovered(false);
        }
        this._hovered = hovered;
        if (this._hovered) {
            this._hovered.setHovered(true);
        }
    }

    update(mousePosition: Vector2, camera: Camera) {
        this.currentMousePosition.copy(mousePosition);
        this.raycaster.setFromCamera(mousePosition, camera);
        if (this.grabbed) {
            this.grabbed.moveToRay(this.raycaster.ray);
            return;
        }
        const intersections = this.raycaster.intersectObjects(this.gizmoService.grabbables);
        if (!intersections || intersections.length === 0) {
            this.hovered = null;
            document.body.style.cursor = "auto";
        } else {
            for (let intersection of intersections) {
                const obj = (intersection.object as Grabbable);
                if (obj.isReallyVisible()) {
                    this.hovered = (intersection.object as Grabbable);
                    document.body.style.cursor = "grab";
                    break;
                }
                this.hovered = null;
            }
        }
    }

    grab() {
        if (!this._hovered) {
            return;
        }
        this._hovered.grabbed = true;
        this.grabbed = this._hovered;
        this.grabbed.getWorldPosition(this.grabbedPosition);
        this.startMousePosition.copy(this.currentMousePosition);
        this.hovered = null;
        document.body.style.cursor = "grabbed";
    }

    release() {
        if (!this.grabbed) {
            return;
        }
        this.startMousePosition.set(0, 0);
        this.currentMousePosition.set(0, 0);
        this.grabbed.grabbed = false;
        this.grabbed = null;
        document.body.style.cursor = "auto";
    }
}
