import {Injectable, Type} from "@angular/core";
import {Character} from "../model/character";
import {Group, LoopOnce, Vector2, Vector3} from "three";
import {SpawnerService} from "./spawner.service";
import {CharacterObject} from "../../3d/world/character-object";
import {CameraService} from "./camera.service";
import {CellService} from "./cell.service";
import {ContextMenuTargetService} from "./context-menu-target.service";
import {NameLabelService} from "./name-label.service";
import {GameObjectService} from "./game-object.service";
import {BoneSlotService} from "./bone-slot.service";
import {BoneSlot} from "../model/boneSlot";

@Injectable({
    providedIn: "root"
})
export class CharacterService extends SpawnerService<Character, CharacterObject> {

    characters: CharacterObject[] = [];
    myCharacterMesh: Group;
    stoppingAnimations: Map<string, any> = new Map<string, any>();
    startingAnimations: Map<string, any> = new Map<string, any>();
    private boneSlots: BoneSlot[] = [];

    constructor(private cameraService: CameraService,
                private cellService: CellService,
                private contextMenuTargetService: ContextMenuTargetService,
                private boneSlotService: BoneSlotService,
                private nameLabelService: NameLabelService) {
        super();
        (window as any).characterService = this;
    }

    async updateEntity(character: Character, SpawnObject3D: Type<CharacterObject>): Promise<CharacterObject> {
        let characterObject = await super.updateEntity(character, CharacterObject);
        characterObject.setTarget(
            new Vector3(character.position.x, character.position.y, character.position.z),
            new Vector2(character.rx, character.ry)
        );
        return characterObject;
    }

    async createEntity(character: Character, type: Type<CharacterObject>): Promise<CharacterObject> {
        const characterObject = await super.createEntity(character, type);
        if (this.boneSlots.length === 0) {
            this.boneSlots = await this.boneSlotService.listAll();
        }
        characterObject.boneSlots = this.boneSlots;
        characterObject.name = character.name;
        this.nameLabelService.add(characterObject);
        this.characters.push(characterObject);
        this.contextMenuTargetService.add(characterObject);
        this.cellService.cellObject.add(characterObject);
        await characterObject.load();
        if (character.me) {
            characterObject.isMe = true;
            characterObject.loadBones();
            if (characterObject.debugSkeleton) {
                this.cellService.cellObject.add(characterObject.debugSkeleton);
            }
            this.myCharacterMesh = (await characterObject.loadClone()).scene;
            console.log("set target");
            this.cameraService.setTarget(characterObject);
        }
        return characterObject;
    }

    async destroyEntity(character: Character): Promise<CharacterObject> {
        const characterObject = await super.destroyEntity(character);
        this.nameLabelService.remove(characterObject);
        this.contextMenuTargetService.remove(characterObject);
        this.cellService.cellObject.remove(characterObject);
        let ci = this.characters.findIndex(c => c.uuid === character.uuid);
        if (ci !== -1) {
            this.characters.splice(ci, 1);
        }
        return characterObject;
    }

    setAnimation(uuid: string, animation: string, state: "start"|"stop"|"oneshot") {
        const character = this.characters.find(c => c.gameUUID === uuid);
        if (!character) {
            return;
        }
        const clip = character.clips.get(animation);
        if (!clip) {
            console.warn(`Clip ${animation} not found`);
            return;
        }
        const animationAction = character.mixer.clipAction(clip);
        if (state === "start") {
            if (!animationAction.isRunning() || this.stoppingAnimations.has(animation)) {
                animationAction.fadeIn(0.2);
                this.startingAnimations.set(animation, setTimeout(() => {
                    animationAction.setEffectiveWeight(1);
                    this.startingAnimations.delete(animation);
                }));
            }
            clearTimeout(this.stoppingAnimations.get(animation));
            this.stoppingAnimations.delete(animation);
            animationAction.play();
        } else if (state === "stop") {
            animationAction.fadeOut(0.2);
            if (this.stoppingAnimations.has(animation)) {
                return;
            }
            this.stoppingAnimations.set(animation, setTimeout(() => {
                animationAction.setEffectiveWeight(1);
                animationAction.stop();
                this.stoppingAnimations.delete(animation);
            }, 200));
        } else if(state === "oneshot") {
            animationAction.setLoop(LoopOnce, 1);
            animationAction.play();
        }
    }

    showMe(show: boolean) {
        const me = this.characters.find(c => c.isMe);
        if (me) {
            me.visible = show;
        }
    }

    update(delta: number) {
        for (let character of this.characters) {
            character.update(delta);
        }
    }
}
