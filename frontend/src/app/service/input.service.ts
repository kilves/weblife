import {Injectable} from "@angular/core";
import {CameraService} from "./camera.service";
import {GrabService} from "./grab.service";
import {ContextMenuService} from "./context-menu.service";
import {Vector2} from "three";
import {EffectService} from "./effect.service";
import {EditTerrainService} from "./edit-terrain.service";
import {MathService} from "./math.service";

type EventKey = keyof HTMLElementEventMap;

@Injectable()
export class InputService {

    private inputElement: HTMLElement;
    private _mousePosition: Vector2 = new Vector2();
    private updateFunc: FrameRequestCallback;

    constructor(private cameraService: CameraService,
                private grabService: GrabService,
                private menuService: ContextMenuService,
                private effectService: EffectService,
                private editTerrainService: EditTerrainService,
                private math: MathService) {
        this.updateFunc = this.update.bind(this);
    }

    init(inputElement: HTMLElement) {
        this.inputElement = inputElement;
        this.inputElement.addEventListener("mousemove", this.mousemoveRaw.bind(this));
        this.handle("mouseup", this.mouseup);
        this.handle("mousedown", this.mousedown);
        this.handle("wheel", this.wheel, {passive: false});
        requestAnimationFrame(this.updateFunc);
    }

    private handle(event: EventKey,
                   handler: (e: Event) => void,
                   options?: AddEventListenerOptions): void {
        (window as any).gameInputElement = this.inputElement;
        this.inputElement.addEventListener(event, this.doHandle.bind(this, handler.bind(this)), options);
    }

    private doHandle(handler: (event: Event) => void, event: Event) {
        if (event.target !== this.inputElement) {
            return;
        }
        event.preventDefault();
        handler(event);
    }

    private update() {
        requestAnimationFrame(this.updateFunc);
        const normalizedScreenPosition = this.math.getNormalizedScreenPosition(this._mousePosition);
        this.editTerrainService.updateBrush(normalizedScreenPosition);
        if (this.editTerrainService.isEnabled()) {
            this.editTerrainService.updatePainting();
        }
        if (this.grabService.active) {
            return;
        }
        this.menuService.update(this._mousePosition.clone());
        this.cameraService.rotate(this._mousePosition.x, this._mousePosition.y);
    }

    private mousemoveRaw(event: MouseEvent) {
        if (event.target !== this.inputElement) {
            this.cameraService.stopRotating();
            return;
        }
        this._mousePosition.set(event.offsetX, event.offsetY);
    }

    private mouseup(event: MouseEvent) {
        this.grabService.release();
        if (this.editTerrainService.isEnabled()) {
            if (event.button === 0) {
                this.editTerrainService.stopPainting();
            }
        }
        if (this.grabService.active) {
            return;
        }
        if (event.button === 0) {
            this.cameraService.stopRotating();
        }
    }

    private mousedown(event: MouseEvent) {
        if (event.target === event.currentTarget) {
            if (document.activeElement) {
                (document.activeElement as HTMLElement).blur();
            }
        }
        const target = event.target as HTMLElement;
        if (!target.hasAttribute("data-menu")) {
            this.menuService.close();
            this.effectService.hideOutlines();
        }
        this.menuService.close();

        if (this.editTerrainService.isEnabled()) {
            if (event.button === 0) {
                this.editTerrainService.startPainting();
            }
            return;
        }
        this.grabService.grab();
        if (this.grabService.active) {
            return;
        }
        if (event.button === 0) {
            this.cameraService.startRotating(event.offsetX, event.offsetY);
        }
    }

    private wheel(event: WheelEvent) {
        this.cameraService.zoomCamera(event.deltaY);
    }
}
