import {EventEmitter, Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {User, UserLogin, UserRegister} from "../model/user";
import {tap} from "rxjs/operators";
import {PromiseClient} from "./promise-client";
import {Register} from "ts-node";
import {SocketService} from "./socket.service";
import {SceneService} from "./scene.service";
import {BehaviorSubject} from "rxjs";
import {ResourceService} from "./resource.service";

export interface RegisterResult {
    verificationRequired: boolean;
}

@Injectable()
export class UserService {

    apiUrl = "/api/users";

    user: BehaviorSubject<User> = new BehaviorSubject<User>(null);

    constructor(private http: PromiseClient,
                private resourceService: ResourceService) {
        this.get().then(user => {
            this.user.next(user);
            if (user) {
                this.resourceService.updateResources().then();
            }
        }).catch(error => {
            if (error.status !== 404) {
                throw error;
            }
        });
    }

    async login(user: UserLogin) {
        let u = await this.http.post<User>(`${this.apiUrl}/login`, user);
        if (u) {
            this.user.next(u);
            await this.resourceService.updateResources();
        }
        return u;
    }

    async register(user: UserRegister): Promise<RegisterResult> {
        return this.http.post<RegisterResult>(`${this.apiUrl}/register`, user);
    }
    
    async logout() {
        let u = await this.http.delete<void>(`${this.apiUrl}/login`);
        this.user.next(null);
        return u;
    }

    async get() {
        return this.http.get<User>(`${this.apiUrl}/me`);
    }
}
