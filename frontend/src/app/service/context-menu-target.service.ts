import {Injectable} from "@angular/core";
import {Object3D} from "three";

@Injectable()
export class ContextMenuTargetService {

    targets: Object3D[] = [];

    add(target: Object3D) {
        this.targets.push(target);
    }

    remove(target: Object3D) {
        this.targets = this.targets.filter(p => p !== target);
    }
}
