import {EventEmitter, Type} from "@angular/core";
import {Group} from "three";
import {IdentifiableObject} from "../model/identifiableObject";

export abstract class SpawnerService<T extends IdentifiableObject, U extends Group> {
    objects: Map<string, U> = new Map<string, U>();

    async createEntity(identifiable: T, SpawnObject3D: Type<U>): Promise<U> {
        let object3D = new SpawnObject3D(identifiable);
        this.objects.set(identifiable.uuid, object3D);
        object3D.position.set(identifiable.position.x, identifiable.position.y, identifiable.position.z);
        return object3D;
    }

    async destroyEntity(identifiable: T): Promise<U> {
        let object3d = this.objects.get(identifiable.uuid);
        this.objects.delete(identifiable.uuid);
        return object3d;
    }

    async updateEntity(identifiable: T, SpawnObject3D: Type<U>): Promise<U> {
        let object3D = this.objects.get(identifiable.uuid);
        if (!object3D) {
            object3D = await this.createEntity(identifiable, SpawnObject3D);
        }
        return object3D;
    }
}
