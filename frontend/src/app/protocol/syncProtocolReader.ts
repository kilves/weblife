import {SQuaternion, SVector3} from "../model/identifiableObject";
import * as uuid from "uuid";
import {Character, CharacterAnimation, CharacterBone} from "../model/character";
import {CellItem} from "../model/gameObjectCreate";
import {Animation} from "../model/animation";

const FLOAT_SIZE = 4;
const INT_SIZE = 4;
const UUID_SIZE = 16;

const BONE_VECTOR3_PRESENCE_BIT = 0x01;
const BONE_QUATERNION_PRESENCE_BIT = 0x02;
const BONE_NAME_PRESENCE_BIT = 0x04;

const CHARACTER_NAME_PRESENCE_BIT = 0x01;

export class SyncProtocolReader {

    private textDecoder = new TextDecoder();

    readChanges(data: DataView): {characters: Character[], items: CellItem[]} {
        let i = 1;
        const characters = [];
        const characterCount = data.getUint32(i); i += INT_SIZE;
        for (let ci = 0; ci < characterCount; ci++) {
            const character: Character = {
                position: { x: 0, y: 0, z: 0 },
                id: 0, rx: 0, ry: 0, s: 0, me: false, uuid: "",
                name: "",
                animations: [],
                bones: []
            };
            const metadata = data.getUint8(i); i += 1;
            if (metadata & CHARACTER_NAME_PRESENCE_BIT) {
                [i, character.name] = this.readString(data, i);
            }

            i = this.readVector3(data, i, character.position);
            character.me = !!data.getUint8(i); i+= 1;
            character.rx = data.getFloat32(i); i += FLOAT_SIZE;
            character.ry = data.getFloat32(i); i += FLOAT_SIZE;
            character.s = data.getFloat32(i); i += FLOAT_SIZE;
            [i, character.uuid] = this.readUUID(data, i);
            character.id = data.getUint32(i); i += INT_SIZE;
            i = this.readAnimations(data, i, character.animations);
            i = this.readBones(data, i, character.bones);
            characters.push(character);
        }

        const items = [];
        const itemCount = data.getUint32(i); i += INT_SIZE;
        for (let oi = 0; oi < itemCount; oi++) {
            const cellObject: CellItem = {
                name: "",
                position: {x: 0, y: 0, z: 0},
                rotation: {x: 0, y: 0, z: 0, w: 0}
            };
            i = this.readVector3(data, i, cellObject.position);
            i = this.readQuaternion(data, i, cellObject.rotation);
            [i, cellObject.uuid] = this.readUUID(data, i);
            items.push(cellObject);
        }
        return {
            characters, items
        };
    }

    private readAnimations(data: DataView, i: number, ary: CharacterAnimation[]) {
        const animationCount = data.getUint32(i); i += INT_SIZE;
        for (let ai = 0; ai < animationCount; ai++) {
            const animation: CharacterAnimation = {
                animationId: "", name: ""
            };
            [i, animation.animationId] = this.readString(data, i);
            [i, animation.name] = this.readString(data, i);
            ary.push(animation);
        }
        return i;
    }

    private readBones(data: DataView, i: number, ary: CharacterBone[]): number {
        const boneCount = data.getUint32(i); i += INT_SIZE;
        for (let bi = 0; bi < boneCount; bi++) {
            const bone: CharacterBone = {
                boneSlotId: 0,
            };
            const infoBits = data.getUint8(i); i+= 1;
            bone.boneSlotId = data.getUint32(i); i+= INT_SIZE;
            if (infoBits & BONE_VECTOR3_PRESENCE_BIT) {
                bone.position = {x: 0, y: 0, z: 0};
                i = this.readVector3(data, i, bone.position);
            }
            if (infoBits & BONE_QUATERNION_PRESENCE_BIT) {
                bone.quaternion = {x: 0, y: 0, z: 0, w: 0};
                i = this.readQuaternion(data, i, bone.quaternion);
            }
            if (infoBits & BONE_NAME_PRESENCE_BIT) {
                [i, bone.name] = this.readString(data, i);
            }
            ary.push(bone);
        }
        return i;
    }

    private readVector3(data: DataView, i: number, vector: SVector3): number {
        vector.x = data.getFloat32(i); i+= FLOAT_SIZE;
        vector.y = data.getFloat32(i); i+= FLOAT_SIZE;
        vector.z = data.getFloat32(i); i+= FLOAT_SIZE;
        return i;
    }

    private readQuaternion(data: DataView, i: number, quaternion: SQuaternion): number {
        quaternion.x = data.getFloat32(i); i+= FLOAT_SIZE;
        quaternion.y = data.getFloat32(i); i+= FLOAT_SIZE;
        quaternion.z = data.getFloat32(i); i+= FLOAT_SIZE;
        quaternion.w = data.getFloat32(i); i+= FLOAT_SIZE;
        return i;
    }

    private readUUID(view: DataView, index: number): [number, string] {
        const uuidBytes = [];
        for (let i = 0; i < UUID_SIZE; i++) {
            uuidBytes.push(view.getUint8(index)); index++;
        }
        return [index, uuid.stringify(uuidBytes)];
    }

    private readString(view: DataView, i: number): [number, string] {
        const length = view.getUint32(i); i += INT_SIZE;
        return [i + length, this.textDecoder.decode(view.buffer.slice(i, i + length))];
    }
}