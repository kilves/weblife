import {BehaviorSubject} from "rxjs";

export class WindowClient {
    notification: BehaviorSubject<string>;
    customData: any;
    onWindowOpened() {}
    onClose() {}
}
