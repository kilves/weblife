import {Animation} from "./animation";

export interface Avatar {
    id: number;
    name: string;
}

export interface AvatarAnimation {
    id: number;
    animationId: string;
    name: string;
}

export class AvatarBoneSlot {
    id: number;
    name: string;
}

export interface FullAvatar extends Avatar {
    animations: AvatarAnimation[];
    boneSlots: AvatarBoneSlot[];
}