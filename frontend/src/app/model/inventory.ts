import {GameObjectCreate} from "./gameObjectCreate";

export interface Inventory {
    items: GameObjectCreate[];
}
