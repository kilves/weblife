import { Resource } from "./resource";

export interface Cell {
    id: number;
    name: string;
    resources: Resource[];
}
