export enum ResourceType {
    HEIGHTMAP = "HEIGHTMAP",
    TEXTURE = "TEXTURE",
    OBJECT3D = "OBJECT"
}

export interface Resource {
    uuid: string;
    type: ResourceType;
    extension: string;
    name: string;
}
