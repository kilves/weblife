export interface BoneSlot {
    id: number;
    name: string;
    bone?: string;
}