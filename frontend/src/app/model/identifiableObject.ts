export interface IdentifiableObject {
    uuid?: string;
    position: SVector3;
}

export interface SVector3 {
    x: number;
    y: number;
    z: number;
}

export interface SQuaternion {
    x: number;
    y: number;
    z: number;
    w: number;
}