export type Axis = "x" | "y" | "z";

export interface AxisMapped<T> {
    x: T;
    y: T;
    z: T;
}
