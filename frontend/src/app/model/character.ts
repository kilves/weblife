import {IdentifiableObject, SQuaternion, SVector3} from "./identifiableObject";

export interface Character extends IdentifiableObject {
    rx: number;
    ry: number;
    s: number;
    id: number;
    me?: boolean;
    name: string;
    animations: CharacterAnimation[];
    bones?: CharacterBone[];
}

export interface CharacterBone {
    boneSlotId: number;
    name?: string;
    position?: SVector3,
    quaternion?: SQuaternion;
}

export interface CharacterAnimation {
    animationId: string;
    name: string;
}