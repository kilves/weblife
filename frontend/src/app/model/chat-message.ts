export interface ChatMessage {
    sender: string;
    text: string;
    channel: ChannelData;
}

export interface ChatMessageSend {
    text: string;
    channel: ChannelData;
}

export interface ChannelData {
    name: string;
    private: boolean;
}
