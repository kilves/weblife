import {Resource} from "./resource";

export interface ItemResources {
    resources: Resource[];
}
