import {Resource} from "./resource";
import {IdentifiableObject, SQuaternion} from "./identifiableObject";

export enum ItemType {
    OBJECT = "OBJECT",
    TEXTURE = "TEXTURE",
    SOUND = "SOUND"
}

export interface CellItem extends IdentifiableObject {
    id?: number;
    rotation: SQuaternion;
    name: string;
    resources?: Resource[];
}

export interface GameObjectCreate {
    x?: number;
    y?: number;
    z?: number;
    type?: ItemType;
    name?: string;
}
