export interface User {
    username?: string;
    password?: string;
    token?: string;
    websocketToken?: string;
}

export interface UserLogin {
    username: string;
    password: string;
}

export interface UserRegister {
    username: string;
    email: string;
    password: string;
    passwordAgain: string;
}