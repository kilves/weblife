export interface MenuOption {
    id: string;
    name: string;
    closeOnClick?: boolean
    action: Function;
}

export interface MenuData {
    header: (customData?: any) => string;
    options: MenuOption[];
}
