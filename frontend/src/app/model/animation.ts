import {AnimationClip} from "three";

export interface Animation {
    id: string;
    name: string;
    animation?: string;
}