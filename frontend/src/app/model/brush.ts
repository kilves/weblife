import {ThemePalette} from "@angular/material/core";

export enum BrushState {
    NONE = "NONE",
    RAISE = "RAISE",
    LOWER = "LOWER",
    SMOOTH = "SMOOTH"
}

export interface Brush {
    name: string;
    color: ThemePalette;
    brush: BrushState;
    icon: string;
}

export interface BrushConfig {
    size: number;
    force: number;
}