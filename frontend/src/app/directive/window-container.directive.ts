import {Directive} from "@angular/core";
import {WindowService} from "../service/window.service";
import {NgTemplateOutlet} from "@angular/common";

@Directive({
    selector: "[windowContainer]"
})
export class WindowContainerDirective {

    constructor(private windowService: WindowService,
                private ngTemplate: NgTemplateOutlet) {
    }

    render() {
        
    }
}
