import {Directive, ElementRef} from "@angular/core";
import {InputService} from "../service/input.service";

@Directive({
    selector: "[game-input]"
})
export class InputDirective {
    constructor(private elementRef: ElementRef,
                private inputService: InputService) {
        this.inputService.init(elementRef.nativeElement);
    }
}