import {Directive, ElementRef} from "@angular/core";
import {UIService} from "../service/ui.service";

@Directive({
    selector: "[ui]"
})
export class UiDirective {
    constructor(private elementRef: ElementRef,
                private uiService: UIService) {
        this.uiService.init(elementRef.nativeElement);
    }
}