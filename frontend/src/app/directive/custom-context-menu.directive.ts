import {Directive, ElementRef, EventEmitter, HostListener, Output} from "@angular/core";

@Directive({
    selector: "[custom-context-menu]"
})
export class CustomContextMenuDirective {
    @Output() onContextMenu: EventEmitter<MouseEvent> = new EventEmitter<MouseEvent>();

    constructor(private element: ElementRef) {
    }

    @HostListener("contextmenu", ["$event"])
    public onListenerTriggered(event: MouseEvent) {
        if (event.target === this.element.nativeElement) {
            this.onContextMenu.emit(event);
        }
    }
}