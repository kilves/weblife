import {Directive, ElementRef} from "@angular/core";
import {SceneService} from "../service/scene.service";

@Directive({
    selector: "[scene]"
})
export class SceneDirective {

    constructor(private elementRef: ElementRef,
                private sceneService: SceneService) {
        this.sceneService.init(this.elementRef.nativeElement);
    }
}