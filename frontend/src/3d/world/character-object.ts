import {
    Bone,
    Object3D, Quaternion, SkeletonHelper,
    Vector2,
    Vector3, XRHandSpace,
} from "three";
import {Character, CharacterAnimation, CharacterBone} from "../../app/model/character";
import {AnimatedVector} from "./math/animated-vector";
import {WorldObject} from "./worldObject";
import {GLTFLoader} from "three/examples/jsm/loaders/GLTFLoader";
import {IKChain, IKJoint, IK} from "three-ik";
import {BoneSlot} from "../../app/model/boneSlot";
import {findBones, findRootBone} from "../utility/util";
import {AvatarApproximation} from "./xr/avatar-approximation";

export class CharacterObject extends WorldObject {

    x: number;
    y: number;
    speed: number;
    gameUUID: string;
    private gameId: number;
    private gameAnimations: CharacterAnimation[] = [];
    boneSlots: BoneSlot[];
    private bonesBySlotId: {
        [k: number]: Bone
    } = {};
    private bonesBySlotName: {
        [k: string]: Bone
    } = {};

    // Interpolation
    private smoothPosition: AnimatedVector;
    private smoothRotation: AnimatedVector;

    // VR
    private ik: IK;
    private leftHand = new IKChain();
    private leftHandChain = ["left_shoulder", "left_upper_arm", "left_lower_arm", "left_hand"]
    private rightHand = new IKChain();
    private rightHandChain = ["right_shoulder", "right_upper_arm", "right_lower_arm", "right_hand"]
    private leftFoot = new IKChain();
    private leftFootChain = [
        "hips",
        "left_upper_leg",
        "left_lower_leg",
        "left_foot",
        "left_toes"
    ]
    private rightFoot = new IKChain();
    private rightFootChain = [
        "hips",
        "right_upper_leg",
        "right_lower_leg",
        "right_foot",
        "right_toes"
    ]
    private spine = new IKChain();
    private chainsByName = new Map<string, {chain: IKChain, joint: IKJoint}>();
    private joints: IKJoint[] = [];
    private spineChain = ["hips", "spine","chest"];
    private head = new IKChain();
    private headChain = ["neck", "head"];
    private bones: CharacterBone[];
    debugSkeleton: SkeletonHelper;
    private approximation = new AvatarApproximation();

    public isMe = false;
    
    constructor(c: Character) {
        super(c);
        this.add(this.dbgTarget);
        this.gameId = c.id;
        this.gameUUID = c.uuid;
        this.name = c.name;
        this.position.set(c.position.x, c.position.y, c.position.z);
        this.speed = c.s;
        this.gameAnimations = c.animations;
        this.bones = c.bones;
        this.setTarget(
            new Vector3(c.position.x, c.position.x, c.position.z),
            new Vector2(c.rx, c.ry)
        );
    }
    
    async load() {
        await this.loadGltf(`/avatar/${this.gameId}`, this.gameAnimations);
        await super.load();
    }

    async loadClone() {
        const loader = new GLTFLoader();
        return loader.loadAsync(`/avatar/${this.gameId}`);
    }

    async loadAnimations() {

    }

    /*
    raycast(raycaster: Raycaster, intersects: Intersection[]) {
        const skinnedMesh = this.findSkinnedMesh();
        raycastSkinnedMesh(this.object, skinnedMesh, this.mixer, this.clips, raycaster, intersects);
    } */
    
    update(delta: number) {
        this.dbgTarget.position.y += delta;
        super.update(delta);
        if (this.ik) {
            this.ik.solve();
        }
        if (this.smoothPosition) {
            this.smoothPosition.update(delta);
        }
        if (this.smoothRotation) {
            this.smoothRotation.update(delta);
        }
        this.position.copy(this.smoothPosition.vector as Vector3);
        this.rotation.set(0, (this.smoothRotation.vector as Vector2).x, 0);
    }
    
    setTarget(position: Vector3, rotation: Vector2) {
        if (!this.smoothRotation) {
            this.smoothRotation = new AnimatedVector();
        }
        if (!this.smoothPosition) {
            this.smoothPosition = new AnimatedVector();
        }
        
        let rotVector = new Vector2(this.rotation.y, this.rotation.x);
        this.smoothRotation.animate(rotVector, rotation, 0.05);
        this.smoothPosition.animate(this.position, position, 0.05);
    }

    setVRTarget(boneSlotName: string, target: Object3D) {
        const res = this.chainsByName.get(boneSlotName);
        if (!res) {
            return;
        }
        this.setJointTarget(res.chain, res.joint, target);
    }

    loadBones() {
        // Create copy of the skeleton to work with
        let bones = findBones(this.object);
        let rootBone = findRootBone(bones);

        rootBone = rootBone.clone(true);
        bones = findBones(rootBone);
        this.useDebug(rootBone);
        if (this.bones.length === 0) {
            return;
        }
        for (const cBone of this.bones) {
            const bone = bones.find(b => b.name === cBone.name);
            if (bone) {
                this.bonesBySlotId[cBone.boneSlotId] = bone;
                const boneSlot = this.boneSlots.find(s => s.id === cBone.boneSlotId);
                this.bonesBySlotName[boneSlot.name] = bone;
            }
        }
        this.ik = new IK();
        this.makeChain(this.leftHand, this.leftHandChain, true);
        this.makeChain(this.rightHand, this.rightHandChain, true);
        this.makeChain(this.leftFoot, this.leftFootChain);
        this.makeChain(this.rightFoot, this.rightFootChain);
        this.makeChain(this.spine, this.spineChain);
        this.makeChain(this.head, this.headChain, true);
        this.ik.add(this.leftHand);
        this.ik.add(this.rightHand);
        this.ik.add(this.leftFoot);
        this.ik.add(this.rightFoot);
        this.ik.add(this.spine);
        this.spine.connect(this.leftFoot);
        this.spine.connect(this.rightFoot);
        this.fillChainUntilJoint(this.spine, this.rightHand.base);
        this.spine.add(this.makeIKJoint(this.head.base.bone));
        this.spine.connect(this.rightHand);
        this.spine.connect(this.leftHand);
        this.spine.connect(this.head);
        this.ik.recalculate();
    }

    private fillChainUntilJoint(chain: IKChain, joint: IKJoint, i: number = 0) {
        if (i >= chain.joints.length) {
            const last = chain.joints[chain.joints.length - 1].bone;
            if (last.children.length !== 1) {
                // Only 1 child allowed
                console.warn("Couldn't fill chain");
                return;
            }
            const child = last.children[0];
            if (joint.bone === child) {
                chain.add(joint);
                return joint;
            }
            chain.add(this.makeIKJoint(child));
            return this.fillChainUntilJoint(chain, joint, i + 1);
        }

        if (joint === chain.joints[i]) {
            return chain.joints[i];
        }

        return this.fillChainUntilJoint(chain, joint, i + 1);
    }

    private makeChain(chain: IKChain, boneNames: string[], expandParent: boolean = false) {
        const bones = boneNames.map(n => [n, this.bonesBySlotName[n]]);
        const first = bones[0][1] as Bone;
        if (expandParent && (first.parent as any).isBone) {
            bones.unshift(["", first.parent as Bone]);
        }
        for (const boneData of bones) {
            const name = boneData[0] as string;
            const bone = boneData[1] as Bone;
            if (!name) {
                // Expanded. Dont add to chain map
                chain.add(this.makeIKJoint(bone));
                continue;
            }
            const joint = this.makeIKJoint(bone);
            this.chainsByName.set(name, {chain, joint});
            chain.add(joint);
        }
    }

    private makeIKJoint(bone: Bone) {
        const existingJoint = this.joints.find(j => j.bone === bone);
        if (existingJoint) {
            return existingJoint;
        }
        const newJoint = new IKJoint(bone);
        this.joints.push(newJoint);
        return newJoint;
    }

    private findBone() {
    }

    private setJointTarget(chain: IKChain, joint: IKJoint, target: any) {
        chain.effector = joint;
        chain.effectorIndex = joint;
        chain.target = target;
    }

    private setTargetAtEnd(chain: IKChain, target: any) {
        this.setJointTarget(chain, chain.joints[chain.joints.length - 1], target);
    }

    private useDebug(rootBone: Bone) {
        const debugObj = new Object3D();
        this.debugSkeleton = new SkeletonHelper(rootBone);
    }

    private dbgTarget = new Object3D();

    setHand(handedness: XRHandedness, hand: Object3D) {
        if (handedness === "right") {
            this.setTargetAtEnd(this.rightHand, hand);
        }
        if (handedness === "left") {
            this.setTargetAtEnd(this.leftHand, hand);
        }
    }

    setHead(head: Object3D) {
        this.setTargetAtEnd(this.head, head);
    }
}
