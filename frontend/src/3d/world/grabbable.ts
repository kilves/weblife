import {Box3Helper, Color, Mesh, MeshBasicMaterial, Object3D, Ray, Vector2, Vector3} from "three";
import {BufferGeometry} from "three/src/core/BufferGeometry";
import {Material} from "three/src/materials/Material";
import {Geometry} from "three/examples/jsm/deprecated/Geometry";

export interface GrabOptions {
    grabbedColor?: Color;
}

export class Grabbable extends Mesh {

    private _grabbed: boolean;
    private originalColor: Color;
    private options: GrabOptions = {
        grabbedColor: null
    }

    get grabbed(): boolean {
        return this._grabbed;
    }

    setHovered(hovered: boolean) {
    }

    set grabbed(value: boolean) {
        this._grabbed = value;
        const material = (this.material as MeshBasicMaterial);
        if (value && !this._grabbed) {
            // Turn grab on
            this.originalColor = material.color;
            material.color = this.options.grabbedColor ? this.options.grabbedColor : this.originalColor;
            material.needsUpdate = true;
        } else if (!value && this.grabbed) {
            // Turn grab off
            if (this.originalColor) {
                (this.material as MeshBasicMaterial).color = this.originalColor;
            }
            material.needsUpdate = true;
        }
        this._grabbed = value;
        if (!this._grabbed) {
            this.release();
        }
    }

    protected release() {

    }

    moveToRay(ray: Ray) {

    }

    isReallyVisible(object?: Object3D): boolean {
        if (object === null) {
            return true;
        }
        if (object === undefined) {
            if (!this.visible) {
                return false;
            }
            return this.isReallyVisible(this.parent);
        }
        if (!object.visible) {
            return false;
        }
        return this.isReallyVisible(object.parent);
    }

    constructor(geometry?: BufferGeometry, material?: Material | Material[], options?: GrabOptions) {
        super(geometry, material);
        if (options) {
            this.options = options;
        }
    }
}
