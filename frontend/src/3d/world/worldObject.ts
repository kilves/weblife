import {
    AnimationClip,
    AnimationMixer, BoxGeometry, DoubleSide,
    Group,
    Mesh, MeshStandardMaterial,
    Object3D, Side,
} from "three";
import {IdentifiableObject} from "../../app/model/identifiableObject";
import {restoreAttribute, setAttribute} from "../utility/util";
import {Meshes} from "../utility/meshes";
import {CharacterAnimation} from "../../app/model/character";
import {Material} from "three/src/materials/Material";
import {userData} from "three/examples/jsm/nodes/shadernode/ShaderNodeBaseElements";

const MAX_ITERATION_DEPTH = 20;

export class WorldObject extends Group {
    supportsContextMenu = true;
    mixer: AnimationMixer;
    clips: Map<string, AnimationClip> = new Map<string, AnimationClip>();
    object: Group;

    constructor(identifiable: IdentifiableObject) {
        super();
    }
    
    async load() {
    }

    update(delta: number) {
        if (this.mixer) {
            this.mixer.update(delta);
        }
    }


    // TODO make animations for items as well
    async loadGltf(path: string, animations?: CharacterAnimation[]) {
        if (this.object) {
            this.remove(this.object);
        }
        const obj = await Meshes.get(path);

        this.mixer = new AnimationMixer(obj.scene);
        this.add(obj.scene);
        this.object = obj.scene;
        if (animations) {
            for (const animation of animations) {
                const clip = obj.animations.find(c => c.name === animation.name);
                if (clip) {
                    this.clips.set(animation.animationId, clip);
                }
            }
        }
        this.enableShadows();
    }

    async loadDefault() {
        const group = new Group();
        const obj = new Mesh(new BoxGeometry(), new MeshStandardMaterial());
        group.add(obj);
        this.add(group);
        this.object = group;
        this.enableShadows();
    }

    enableShadows(obj: Object3D = this, ss: number = 0) {
        if (!obj.userData.skipCastShadow) {
            obj.castShadow = true;
            obj.receiveShadow = true;
        }
        setAttribute((obj as Mesh).material, "shadowSide", DoubleSide);
        if (ss > 20) {
            return;
        }
        for (let child of obj.children) {
            this.enableShadows(child, ss + 1);
        }
    }

    setMaterialSide(side: Side, obj: Object3D = this.object, depth: number = 0) {
        setAttribute((obj as Mesh).material, "side", side);
        if (depth > 20) {
            return;
        }
        for (const child of obj.children) {
            this.setMaterialSide(side, child, depth + 1);
        }
    }

    restoreMaterialSide(obj: Object3D = this.object, depth: number = 0) {
        restoreAttribute((obj as Mesh).material, "side");
        if (depth > 20) {
            return;
        }
        for (const child of obj.children) {
            this.restoreMaterialSide(child, depth + 1);
        }
    }
}
