import {MeshBasicMaterial, Ray} from "three";
import {MoveGizmoObject} from "./move-gizmo-object";
import {Axis} from "../../app/model/axis";
import {rayPointClosestToRay} from "../utility/util";
import {GizmoHandle} from "./gizmo-handle";

export class MoveGizmoHandle extends GizmoHandle {
    constructor(protected gizmo: MoveGizmoObject, axis: Axis) {
        super(gizmo, axis);
        this.position.copy(this.positions[axis]);
    }

    release() {
        this.gizmo.positionSet.emit(this.gizmo.parent.position);
    }

    moveToRay(ray: Ray) {
        const grabbedRay = new Ray();
        this.parent.getWorldPosition(grabbedRay.origin);
        grabbedRay.direction = this.positions[this.axis];
        rayPointClosestToRay(grabbedRay, ray, this.gizmo.parent.position);
        const scale = -this.parent.scale[this.axis];
        this.gizmo.parent.position.addScaledVector(this.positions[this.axis], scale);
    }

    setHovered(hovered: boolean) {
        super.setHovered(hovered);
        const material = this.material as MeshBasicMaterial;
        material.opacity = hovered ? 1 : 0.5;
        const c = this.colors[this.axis];
        material.color.copy(c).multiplyScalar(0.5);
    }
}
