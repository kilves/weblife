import {Group, Object3D} from "three";

export class AvatarApproximation {
    private head = new Object3D();
    hips = new Object3D();
    leftToes = new Object3D();
    rightToes = new Object3D();
    leftFoot = new Object3D();
    rightFoot = new Object3D();

    constructor() {
    }

    update() {

    }

    setHead(head: Object3D) {
        this.head = head;
        this.reposition();
    }

    private reposition() {
        this.head.add(this.hips, this.leftToes, this.rightToes, this.leftFoot, this.rightFoot);
    }
}