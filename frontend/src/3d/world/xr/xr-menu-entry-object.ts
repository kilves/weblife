import {
    BoxGeometry,
    BufferGeometry, CanvasTexture,
    DoubleSide,
    Group,
    Mesh,
    MeshBasicMaterial,
    PlaneGeometry,
    RGBAFormat,
    SphereGeometry, Texture,
    TextureLoader,
} from "three";
import {Material} from "three/src/materials/Material";

const BUTTON_SCALE = 0.08;
let entryTexture: Texture = null;

export class XrMenuEntryObject extends Group {

    private _highlight = false;
    private basicMaterial = new MeshBasicMaterial({
        transparent: true,
        side: DoubleSide,
    });
    private highlightMaterial = new MeshBasicMaterial({
        transparent: true,
        side: DoubleSide
    });
    set highlight(value: boolean) {
        this._highlight = value;
        if (this._highlight) {
            this.imageMesh.material = this.highlightMaterial;
            this.mesh.scale.set(1.2, 1.2, 1.2);
        } else {
            this.mesh.scale.set(1, 1, 1);
            this.imageMesh.material = this.basicMaterial;
        }
        this.imageMesh.material.needsUpdate = true;
    }

    get highlight(): boolean {
        return this._highlight;
    }

    private mesh: Mesh<BufferGeometry, Material> = new Mesh(
        new PlaneGeometry(),
        new MeshBasicMaterial({
            side: DoubleSide,
            transparent: true
        })
    );
    private imageMesh = new Mesh(
        new PlaneGeometry(),
        new MeshBasicMaterial({
            side: DoubleSide,
            transparent: true
        })
    );

    constructor(public entryName: string, rot: number) {
        super();
        this.initialize(rot).then();
    }

    async initialize(rot: number) {
        this.mesh.renderOrder = 101;

        // Fit menu button object
        this.scale.set(BUTTON_SCALE, BUTTON_SCALE, BUTTON_SCALE);
        // this.mesh.rotateX(Math.PI / 2 * 80 / 90);
        this.mesh.rotateX(- Math.PI / 2);
        this.mesh.rotateZ(-rot - Math.PI / 2);
        const x = Math.cos(rot);
        const y = Math.sin(rot);
        this.mesh.position.set(x, 0.01, y);

        // Setup menu button texture
        const loader = new TextureLoader();
        if (!entryTexture) {
            entryTexture = await loader.loadAsync("/resources/menuOption.png");
            entryTexture.format = RGBAFormat;
        }
        this.mesh.material.dispose();
        this.mesh.material = new MeshBasicMaterial({
            map: entryTexture,
            side: DoubleSide,
            transparent: true
        });

        // Fit menu content (icon/text) object
        this.imageMesh.scale.set(0.5, 0.5, 0.5);
        this.imageMesh.position.set(0, 0, 0.00001);
        this.imageMesh.rotateZ(rot + Math.PI / 2);

        // Draw menu content to texture
        this.drawContentCanvas();
        this.highlight = false;
        this.imageMesh.renderOrder = 105;
        this.add(this.mesh);
        this.mesh.add(this.imageMesh);
    }

    update() {

    }

    dispose() {
        this.highlightMaterial.dispose();
        this.basicMaterial.dispose();
        this.mesh.material.dispose();
        this.mesh.geometry.dispose();
        if (this.parent) {
            this.parent.remove(this);
        }
    }

    drawContentCanvas() {
        const basicCanvas = new OffscreenCanvas(1024, 1024);
        const basicContext = basicCanvas.getContext("2d");
        basicContext.clearRect(0, 0, basicCanvas.width, basicCanvas.height);
        basicContext.font = "bold 412px serif";
        basicContext.textAlign = "center";
        basicContext.textBaseline = "middle";
        basicContext.fillStyle = "white";
        basicContext.fillText(this.entryName, 512, 512);
        this.basicMaterial.map = new CanvasTexture(basicCanvas);
        this.basicMaterial.needsUpdate = true;

        const highlightCanvas = new OffscreenCanvas(1024, 1024);
        const highlightContext = highlightCanvas.getContext("2d");
        highlightContext.clearRect(0, 0, basicCanvas.width, basicCanvas.height);
        highlightContext.font = "bold 412px serif";
        highlightContext.textAlign = "center";
        highlightContext.textBaseline = "middle";
        highlightContext.fillStyle = "red";
        highlightContext.fillText(this.entryName, 512, 512);
        this.highlightMaterial.map = new CanvasTexture(highlightCanvas);
        this.highlightMaterial.needsUpdate = true;
    }
}