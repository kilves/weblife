import {
    AdditiveBlending,
    CanvasTexture,
    DoubleSide,
    Mesh,
    MeshBasicMaterial,
    Object3D,
    PlaneGeometry,
} from "three";

export class ControllerBeamObject extends Object3D {
    private material = new MeshBasicMaterial({
        blending: AdditiveBlending,
        color: 0x4444aa,
        side: DoubleSide,
        depthWrite: false,
        transparent: true
    });
    private geometry = new PlaneGeometry(1, 0.1 * 5);

    set length(value: number) {
        this.scale.setZ(value);
    }

    get length() {
        return this.scale.z;
    }

    constructor() {
        super();
        const canvas = this.generateLaserBodyCanvas();
        const texture = new CanvasTexture(canvas);
        this.geometry.rotateY(0.5 * Math.PI);
        this.material.map = texture;
        this.material.needsUpdate = true;
        const nPlanes = 15;
        for (let i = 0; i < nPlanes; i++) {
            const mesh = new Mesh(this.geometry, this.material);
            mesh.position.z = 1 / 2;
            mesh.rotation.z = i / nPlanes * Math.PI;
            this.add(mesh);
        }
    }

    dispose() {
        this.material.dispose();
        this.geometry.dispose();
    }

    private generateLaserBodyCanvas(): OffscreenCanvas {
        const canvas = new OffscreenCanvas(1, 64);
        const context = canvas.getContext('2d');
        // set gradient
        const gradient = context.createLinearGradient(
            0, 0, canvas.width, canvas.height
        );
        gradient.addColorStop(0, 'rgba(  0,  0,  0,0.1)');
        gradient.addColorStop(0.1, 'rgba(160,160,160,0.3)');
        gradient.addColorStop(0.5, 'rgba(255,255,255,0.5)');
        gradient.addColorStop(0.9, 'rgba(160,160,160,0.3)');
        gradient.addColorStop(1.0, 'rgba(  0,  0,  0,0.1)');
        // fill the rectangle
        context.fillStyle = gradient;
        context.fillRect(0, 0, canvas.width, canvas.height);
        // return the just built canvas
        return canvas;
    }
}