import {DoubleSide, Group, Mesh, MeshBasicMaterial, PlaneGeometry} from "three";

export class XrWindowObject extends Group {

    private plane = new Mesh(
        new PlaneGeometry(),
        new MeshBasicMaterial({
            side: DoubleSide,
            color: 0xff0000
        })
    );

    constructor(width: number, height: number) {
        super();
        this.plane.geometry.dispose();
        this.plane.geometry = new PlaneGeometry(width, height);
        this.add(this.plane);
    }

    dispose() {
        this.plane.geometry.dispose();
        this.plane.material.dispose();
    }
}