import {MeshBasicMaterial} from "three";

export interface MenuOption {
    name: string;
    toolMode?: ToolMode;
    backOnSelect?: boolean;
    selectedTexture?: MeshBasicMaterial;
    children: MenuOption[];
}

export enum ToolMode {
    NONE = "NONE",
    WALK = "WALK",
    SELECT = "SELECT",
    TELEPORT = "TELEPORT",
    WINDOW = "WINDOW",
    OBJECT_NEW = "OBJECT_NEW",
    OBJECT_GRAB = "OBJECT_GRAB",
    AVATAR = "AVATAR",
}

export const ROOT_OPTIONS: MenuOption[] = [
    {
        name: "OBJ",
        toolMode: ToolMode.NONE,
        children: [
            {
                name: "<-",
                children: [],
                backOnSelect: true,
            },
            {
                name: "NEW",
                toolMode: ToolMode.OBJECT_NEW,
                children: []
            },
            {
                name: "GRB",
                toolMode: ToolMode.OBJECT_GRAB,
                children: []
            },
        ]
    },
    {
        name: "WIN",
        toolMode: ToolMode.NONE,
        children: [
            {
                name: "<-",
                children: [],
                backOnSelect: true,
            },
            {
                name: "Chat",
                children: []
            },
            {
                name: "Debug",
                children: []
            }
        ]
    },
    {
        name: "TLP",
        toolMode: ToolMode.TELEPORT,
        children: []
    },
    {
        name: "WLK",
        toolMode: ToolMode.WALK,
        children: []
    },
    {
        name: "AVA",
        toolMode: ToolMode.AVATAR,
        children: []
    }
];