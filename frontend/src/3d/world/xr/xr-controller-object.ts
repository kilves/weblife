import {
    BufferGeometry,
    CanvasTexture,
    DoubleSide,
    Group,
    Mesh,
    MeshBasicMaterial,
    MeshStandardMaterial,
    Object3D,
    PlaneGeometry,
    Raycaster,
    RGBAFormat,
    Texture,
    TextureLoader,
    Vector2,
    Vector3,
    XRHandSpace,
    XRTargetRaySpace,
} from "three";
import {Material} from "three/src/materials/Material";
import {XrMenuEntryObject} from "./xr-menu-entry-object";
import {ControllerBeamObject} from "./controller-beam-object";
import {GLTFLoader} from "three/examples/jsm/loaders/GLTFLoader";
import {MotionController} from "@webxr-input-profiles/motion-controllers";
import {XRService} from "../../../app/service/xr.service";
import {MenuOption, ROOT_OPTIONS, ToolMode} from "./menuOptions";

let surfaceTexture: Texture = null;
let baseController: Group = null;

const MENU_OPTION_ANGLE = Math.PI / 3;

const Z_FIGHT_OFFSET = 0.00001;

export class XrControllerObject extends Group {

    private controllerMesh: Group;
    private entryObjects: XrMenuEntryObject[] = [];
    private analogTextureMesh: Mesh = new Mesh();
    beamRaycaster = new Raycaster();
    private grabRaycaster = new Raycaster();

    private walkingDirection = new Vector3();
    private controllerBeam = new ControllerBeamObject();
    private emptyTextureMaterial = new MeshBasicMaterial({
        transparent: true,
        opacity: 0,
    });
    private selectionTextureMesh: Mesh = new Mesh<BufferGeometry, Material>(
        new PlaneGeometry(),
        new MeshBasicMaterial({
            transparent: true,
            opacity: 0,
        }),
    );
    private previousMenuOptions: MenuOption[] = [];
    private menuOptions: MenuOption[] = ROOT_OPTIONS;
    private analogPosition: Vector2 = new Vector2();
    private analogLastMoved = new Date().getTime();
    private _toolMode: ToolMode = ToolMode.NONE;
    private walking: boolean = false;
    private menuSurface: Mesh;

    constructor(public motionController: MotionController,
                private globalHand: XRHandSpace,
                private xrService: XRService) {
        super();
        this.initialize().then();
        this.loadMenuOptionTextures();
    }

    async initialize() {
        if (!baseController) {
            const loader = new GLTFLoader();
            baseController = (await loader.loadAsync("/resources/controller.glb")).scene;
        }
        this.controllerMesh = baseController.clone(true);
        this.controllerBeam.visible = false;
        this.controllerBeam.rotateX(Math.PI);
        this.controllerBeam.scale.set(0.05, 0.05, 0.05);
        this.add(this.controllerMesh);
        this.add(this.controllerBeam);
        this.add(this.analogTextureMesh);
        this.controllerMesh.visible = true;
        console.time("prepareMaterials");
        this.prepareMaterials(this.controllerMesh).then(() => {
            console.timeEnd("prepareMaterials");
        });
    }

    async prepareMaterials(obj: Object3D) {
        const asMesh = obj as Mesh;
        const asMaterial = asMesh.material as Material
        if (asMaterial && asMaterial.isMaterial) {
            asMaterial.dispose();
            if (asMesh.name === "MenuSurface") {
                console.time("MenuSurface");
                await this.prepareMenuSurface(asMesh);
                console.timeEnd("MenuSurface");
                return;
            }
            asMesh.material = new MeshStandardMaterial({
                side: DoubleSide
            });
        }
        for (const child of obj.children) {
            await this.prepareMaterials(child);
        }
    }

    private async prepareMenuSurface(menuSurface: Mesh) {
        this.menuSurface = menuSurface;
        // The surface to draw menu on
        const loader = new TextureLoader();
        if (!surfaceTexture) {
            surfaceTexture = await loader.loadAsync("/resources/menuSurface.png");
            surfaceTexture.format = RGBAFormat;
        }
        menuSurface.material = new MeshBasicMaterial({
            side: DoubleSide,
            map: surfaceTexture,
            transparent: true,
        });

        this.showOptions(ROOT_OPTIONS);

        // Pointers
        this.analogTextureMesh.rotateX(Math.PI / 2);
        this.analogTextureMesh.material = new MeshBasicMaterial({
            map: surfaceTexture,
            side: DoubleSide,
            transparent: true,
        });
        this.analogTextureMesh.renderOrder = 100;
        this.analogTextureMesh.geometry = new PlaneGeometry();
        this.analogTextureMesh.position.set(0, Z_FIGHT_OFFSET * 2, 0);
        this.analogTextureMesh.scale.set(0.01, 0.01, 0.01);
        this.analogTextureMesh.visible = false;
        menuSurface.add(this.analogTextureMesh);

        // Position texture that shows when hovering over something
        this.selectionTextureMesh.scale.set(0.04, 0.04, 0.04);
        this.selectionTextureMesh.position.set(0, Z_FIGHT_OFFSET, 0);
        this.selectionTextureMesh.rotateX(-Math.PI / 2);
        menuSurface.add(this.selectionTextureMesh);
    }

    setAnalogPosition(x: number, y: number) {
        if (this.analogPosition.x === x && this.analogPosition.y === y) {
            return;
        }
        this.analogLastMoved = new Date().getTime();
        this.analogPosition.set(x, y);
        const normalized = new Vector2(x, y).normalize();
        this.analogTextureMesh.position.set(
             normalized.x * 0.04, Z_FIGHT_OFFSET * 2, -normalized.y * 0.04
        );
        // Need -x -y because menu indexing starts at top (angle 270deg)
        const angle = Math.atan2(-x, -y) + Math.PI;

        // Update menu highlights
        const toHighlight = this.getMenuIndex(angle);
        const highlighted = this.entryObjects.findIndex(i => i.highlight);
        if (highlighted === -1) {
            this.entryObjects[toHighlight].highlight = true;
        } else if (toHighlight !== highlighted) {
            this.entryObjects[highlighted].highlight = false;
            this.entryObjects[toHighlight].highlight = true;
        }
        this.analogTextureMesh.visible = true;
    }

    update(target: XRTargetRaySpace) {
        const now = new Date().getTime();
        if (now - this.analogLastMoved > 50) {
            // Touch has ended.
            if (this.analogTextureMesh.visible) {
                this.selectHighlightedOption();
            }
            this.analogTextureMesh.visible = false;
        }

        this.getWorldPosition(this.beamRaycaster.ray.origin);
        this.getWorldDirection(this.beamRaycaster.ray.direction);
        this.beamRaycaster.ray.direction.multiplyScalar(-1); // its inverted xd
        if (this._toolMode === ToolMode.TELEPORT
            || this._toolMode === ToolMode.SELECT
            || this._toolMode === ToolMode.OBJECT_NEW
            || this._toolMode === ToolMode.OBJECT_GRAB) {
            this.controllerBeam.visible = true;
            if (this._toolMode === ToolMode.OBJECT_GRAB) {
                this.setSelectRange(0.3);
            } else {
                this.setSelectRange(1000);
            }
        } else {
            this.controllerBeam.visible = false;
        }

        if (this.walking) {
            this.walkingDirection.copy(this.beamRaycaster.ray.direction);
            this.walkingDirection.normalize();
            this.walkingDirection.y = 0;
        } else {
            this.walkingDirection.set(0, 0, 0);
        }
        this.xrService.setMovement(this.walkingDirection);
    }

    private selectHighlightedOption() {
        const highlighted = this.entryObjects.findIndex(
            o => o.highlight
        );
        if (highlighted === -1) {
            return;
        }
        const option = this.menuOptions[highlighted];
        this._toolMode = option.toolMode || this._toolMode;
        this.entryObjects[highlighted].highlight = false;
        if (option.backOnSelect) {
            this.selectionTextureMesh.material = this.emptyTextureMaterial;
            this.showOptions(this.previousMenuOptions);
            return;
        } else {
            this.selectionTextureMesh.material = option.selectedTexture;
        }
        this.selectionTextureMesh.material.needsUpdate = true;
        if (option.children && option.children.length > 0) {
            this.showOptions(option.children)
        }
        if (this._toolMode === ToolMode.AVATAR) {
            this.xrService.showAvatarOptions();
        } else {
            this.xrService.hideAvatarOptions();
        }
    }

    showOptions(options: MenuOption[]) {
        this.previousMenuOptions = this.menuOptions;
        this.disposeEntryObjects();
        this.menuOptions = options;
        for (const [i, menuOption] of options.entries()) {
            const angle = MENU_OPTION_ANGLE * i - Math.PI / 2;
            const entryObject = new XrMenuEntryObject(
                menuOption.name, angle
            );
            this.entryObjects.push(entryObject);
            this.menuSurface.add(entryObject);
        }
    }

    disposeEntryObjects() {
        for (const object of this.entryObjects) {
            object.dispose();
        }
        this.entryObjects = [];
    }

    dispose() {
        if (this.parent) {
            this.parent.remove(this);
        }
        this.disposeEntryObjects();
        this.disposeOptionTextures();
        for (const option of this.menuOptions) {
            if (option.selectedTexture) {
                option.selectedTexture.dispose();
            }
        }
    }

    private disposeOptionTextures(options: MenuOption[] = ROOT_OPTIONS) {
        for (const option of options) {
            if (option.selectedTexture) {
                option.selectedTexture.dispose();
            }
            this.disposeOptionTextures(option.children);
        }
    }

    private getMenuIndex(angle: number) {
        let val = angle / MENU_OPTION_ANGLE;
        if (val >= this.menuOptions.length - 0.5) {
            // Clamp near-max to zero
            val = 0;
        }
        return Math.round(val);
    }

    private loadMenuOptionTextures(options: MenuOption[] = ROOT_OPTIONS) {
        for (const option of options) {
            const canvas = new OffscreenCanvas(1024, 1024);
            const ctx = canvas.getContext("2d");
            ctx.font = "bold 412px serif";
            ctx.textAlign = "center";
            ctx.textBaseline = "middle";
            ctx.fillStyle = "red";
            ctx.fillText(option.name, 512, 512);
            const texture = new CanvasTexture(canvas);
            texture.format = RGBAFormat;
            if (option.selectedTexture) {
                option.selectedTexture.dispose();
            }
            option.selectedTexture = new MeshBasicMaterial({
                map: texture,
                side: DoubleSide,
                transparent: true,
            });
            this.loadMenuOptionTextures(option.children);
        }
    }

    onSelect() {
        if (this._toolMode === ToolMode.TELEPORT) {
            this.xrService.teleportTo(this.beamRaycaster);
        }
        if (this._toolMode === ToolMode.WINDOW) {
            const windowPos = new Vector3();
            windowPos.copy(this.beamRaycaster.ray.direction);
            windowPos.normalize().multiplyScalar(0.5);
            this.xrService.openWindow(
                windowPos.addVectors(this.beamRaycaster.ray.origin, windowPos),
                this.parent.quaternion
            );
        }
        if (this._toolMode === ToolMode.OBJECT_NEW) {
            this.xrService.createObject(this.beamRaycaster);
        }
    }

    onSelectStart() {
        if (this._toolMode === ToolMode.WALK) {
            this.walking = true;
            const direction = this.beamRaycaster.ray.direction.clone().normalize();
            direction.y = 0;
            this.xrService.setMovement(direction);
        }
        if (this._toolMode === ToolMode.OBJECT_GRAB) {
            this.xrService.grabItem(this.beamRaycaster.ray.origin, this);
        }
    }

    onSelectEnd() {
        this.walking = false;
        this.xrService.setMovement(new Vector3());
        this.xrService.releaseItem();
    }

    onSqueezeStart() {
        this.controllerMesh.getWorldPosition(this.grabRaycaster.ray.origin);
        this.controllerMesh.getWorldDirection(this.grabRaycaster.ray.direction);
        // Its inverted
        this.grabRaycaster.ray.direction.multiplyScalar(-1);
        this.grabRaycaster.far = 0.3;
        this.xrService.grabItem(this.grabRaycaster.ray.origin, this);
    }

    onSqueezeEnd() {
        this.xrService.releaseItem();
    }

    private setSelectRange(range: number) {
        this.controllerBeam.length = range;
        this.beamRaycaster.far = range;
    }
}