import {Group, Mesh, MeshBasicMaterial, SphereGeometry, Vector3} from "three";
import {CameraService} from "../../app/service/camera.service";
import {GizmoHandle} from "./gizmo-handle";
import {WorldObject} from "./worldObject";

const _thisPosition = new Vector3();
const _cameraPosition = new Vector3();

export class GizmoObject extends Group {
    sphere: Mesh;
    handles: GizmoHandle[] = [];

    protected points: Vector3[][] = [
        [
            new Vector3(0, 0, 0),
            new Vector3(1, 0, 0)
        ],
        [
            new Vector3(0, 0, 0),
            new Vector3(0, 1, 0)
        ],
        [
            new Vector3(0, 0, 0),
            new Vector3(0, 0, 1)
        ]
    ];

    protected colors: number[] = [0xff0000, 0x00ff00, 0x0000ff];

    constructor(public worldObject: WorldObject) {
        super();
        this.sphere = new Mesh(
            new SphereGeometry(0.05, 32, 32),
            new MeshBasicMaterial({color: 0x000000, depthTest: false})
        );
        this.sphere.renderOrder = 1001;
        this.add(this.sphere);
        this.visible = false;
    }

    update(delta: number) {
        const camera = CameraService.instance.camera;
        this.getWorldPosition(_thisPosition);
        camera.getWorldPosition(_cameraPosition);
        const s = _thisPosition.distanceTo(_cameraPosition) * 0.1;
        this.scale.set(s, s, s);
    }
}
