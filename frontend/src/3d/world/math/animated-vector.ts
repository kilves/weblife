import {Vector} from "three";

export class AnimatedVector {
    
    private deltaPerSec: Vector;
    private to: Vector;
    private time: number;
    private currentVector: Vector;
    
    get vector(): Vector {
        return this.currentVector;
    }
    
    get finished(): boolean {
        return !this.time;
    }
    
    animate(from: Vector, to: Vector, time: number) {
        this.deltaPerSec = from.clone().subVectors(to, from).divideScalar(time);
        this.currentVector = from;
        this.to = to;
        this.time = time;
    }
    
    update(delta: number) {
        if (!this.currentVector) {
            return;
        }
        if (this.time === 0) {
            return;
        }
        this.time -= delta;
        if (this.time < 0) {
            this.currentVector.copy(this.to);
            this.time = 0;
            return;
        }
        this.currentVector.addScaledVector(this.deltaPerSec, delta);
    }
}
