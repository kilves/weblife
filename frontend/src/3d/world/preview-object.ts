import {Box3, Group, Material, Object3D, Texture, Vector3} from "three";
import {GameObjectCreate} from "../../app/model/gameObjectCreate";
import {GameObjectService} from "../../app/service/game-object.service";
import {Resource, ResourceType} from "../../app/model/resource";
import {GLTFLoader} from "three/examples/jsm/loaders/GLTFLoader";

export class PreviewObject extends Group {
    preview: Object3D;
    loading: Object3D = new Group();
    box: Box3;
    boxSize: Vector3 = new Vector3();
    
    constructor() {
        super();
        this.loadLoading();
        this.loading.visible = false;
        this.add(this.loading);
    }
    
    private swap(newPreview: Object3D | Object3D[]) {
        if (this.preview) {
            this.dispose(this.preview);
            this.remove(this.preview);
        }
        
        if (Array.isArray(newPreview)) {
            this.preview = new Group();
            for (let object of newPreview) {
                this.preview.add(object);
            }
        } else {
            this.preview = newPreview;
        }
        this.add(this.preview);
        this.box = new Box3().setFromObject(this);
        this.box.getSize(this.boxSize);
    }
    
    private dispose(target: Object3D) {
        let tgt = target as any;
        if (tgt.geometry) {
            tgt.geometry.dispose();
        }
        if (tgt.material) {
            if (Array.isArray(tgt.material)) {
                for (let material of tgt.material) {
                    this.disposeMaterial(material);
                }
            } else {
                this.disposeMaterial(tgt.material);
            }
        }
        for (let child of target.children) {
            this.dispose(child);
        }
    }
    
    load(item: GameObjectCreate) {
        /*
        GameObjectService.instance.listResources(item).subscribe(resources => {
            for (let resource of resources.resources) {
                if (resource.type === ResourceType.OBJECT3D) {
                    this.loadObject(resource);
                }
            }
        }); */
    }
    
    update(delta: number) {
        if (!this.loading) {
            return;
        }
        this.loading.rotateY(Math.PI * delta);
    }
    
    private loadObject(object: Resource) {
        let gltfLoader = new GLTFLoader();
        this.loading.visible = true;
        gltfLoader.load(`/resources/${object.uuid}.${object.extension}`, group => {
            this.swap(group.scene.children);
            this.loading.visible = false;
        });
    }
    
    private loadLoading() {
        let gltfLoader = new GLTFLoader();
        gltfLoader.load(("/assets/loading.glb"), mesh => {
            let l1 = mesh.scene;
            let l2 = mesh.scene.clone();
            l2.rotateY(Math.PI / 2);
            let l3 = mesh.scene.clone();
            l3.rotateY(Math.PI);
            let l4 = mesh.scene.clone();
            l4.rotateY(- Math.PI / 2);
            this.loading.add(l1, l2, l3, l4);
        });
    }
    
    private disposeMaterial(material: Material) {
        let m = material as any;
        let maps = [
            "alphaMap", "aoMap", "bumpMap", "displacementMap", "emissiveMap", "envMap", "lightMap", "map",
            "metalnessMap", "normalMap", "roughnessMap"
        ];
        
        for (let mapName of maps) {
            if (m[mapName] instanceof Texture) {
                m[mapName].dispose();
            }
        }
        material.dispose();
    }
}
