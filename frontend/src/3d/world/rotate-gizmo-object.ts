import {Euler, Mesh, MeshBasicMaterial, Quaternion, Vector3} from "three";
import {GizmoObject} from "./gizmo-object";
import {RotateGizmoHandle} from "./rotate-gizmo-handle";
import {Axis} from "../../app/model/axis";
import {WorldObject} from "./worldObject";
import {EventEmitter} from "@angular/core";

export class RotateGizmoObject extends GizmoObject {
    sphere: Mesh;
    private identity = new Quaternion().identity();
    rotationSet: EventEmitter<Quaternion> = new EventEmitter<Quaternion>();

    constructor(worldObject: WorldObject) {
        super(worldObject);
        const axes: Axis[] = ["x", "y", "z"];

        for (let i = 0; i < 3; i++) {
            const torus = new RotateGizmoHandle(this, axes[i]);
            torus.userData.skipCastShadow = true;
            this.handles.push(torus);
            this.add(torus);
        }
    }

    reset() {
        this.worldObject.object.setRotationFromQuaternion(this.identity)
    }
}
