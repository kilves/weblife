import {GizmoHandle} from "./gizmo-handle";
import {Axis} from "../../app/model/axis";
import {BoxGeometry, Quaternion} from "three";
import {ScaleGizmoObject} from "./scale-gizmo.object";

export class ScaleGizmoHandle extends GizmoHandle {
    constructor(protected gizmo: ScaleGizmoObject, axis: Axis) {
        super(gizmo, axis);
        this.setRotationFromQuaternion(new Quaternion().identity())
        this.geometry = new BoxGeometry(0.2, 0.2, 0.2);
        this.position.copy(this.positions[axis]);
    }
}