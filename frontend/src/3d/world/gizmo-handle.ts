import {Grabbable} from "./grabbable";
import {Axis, AxisMapped} from "../../app/model/axis";
import {Color, ConeGeometry, Material, MeshBasicMaterial, Vector3} from "three";
import {GizmoObject} from "./gizmo-object";

export class GizmoHandle extends Grabbable {
    protected axis: Axis;
    protected get rotations(): AxisMapped<Vector3> {
        return {
            x: new Vector3(0, 0, -Math.PI / 2),
            y: new Vector3(0, 0, 0),
            z: new Vector3(Math.PI / 2, 0, 0)
        }
    };
    protected colors: AxisMapped<Color> = {
        x: new Color(0xff0000),
        y: new Color(0x00ff00),
        z: new Color(0x0000ff)
    };
    protected positions: AxisMapped<Vector3> = {
        x: new Vector3(1, 0, 0),
        y: new Vector3(0, 1, 0),
        z: new Vector3(0, 0, 1)
    };
    protected defaultMaterial = new MeshBasicMaterial({depthTest: false, transparent: true, opacity: 0.5})

    constructor(protected gizmo: GizmoObject, axis: Axis) {
        super(
            new ConeGeometry(0.1, 0.4, 32),
            new MeshBasicMaterial({depthTest: false, transparent: true, opacity: 0.5})
        );
        this.rotateX(this.rotations[axis].x);
        this.rotateY(this.rotations[axis].y);
        this.rotateZ(this.rotations[axis].z);

        const mat = this.material as MeshBasicMaterial;
        mat.color.copy(this.colors[axis]);
        mat.needsUpdate = true;

        this.axis = axis;
    }

    dispose() {
        this.geometry.dispose();
        (this.material as Material).dispose();
    }

    setHovered(hovered: boolean) {
        super.setHovered(hovered);
        const material = this.material as MeshBasicMaterial;
        material.opacity = hovered ? 1 : 0.5;
        const c = this.colors[this.axis];
        material.color.copy(c).multiplyScalar(0.5);
    }
}