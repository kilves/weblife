import {
    BufferGeometry, Color,
    Line,
    MeshBasicMaterial,
    Vector3,
} from "three";
import {MoveGizmoHandle} from "./move-gizmo-handle";
import {EventEmitter} from "@angular/core";
import {GizmoObject} from "./gizmo-object";
import {Axis} from "../../app/model/axis";
import {WorldObject} from "./worldObject";

export class MoveGizmoObject extends GizmoObject {

    lines: Line[] = [];
    positionSet: EventEmitter<Vector3> = new EventEmitter<Vector3>();

    constructor(worldObject: WorldObject) {
        super(worldObject);

        const axes: Axis[] = ["x", "y", "z"];
        for (let i = 0; i < 3; i++) {
            const line = new Line(
                new BufferGeometry().setFromPoints(this.points[i]),
                new MeshBasicMaterial({color: new Color(this.colors[i]), depthTest: false})
            );
            line.userData.skipCastShadow = true;
            line.renderOrder = 1000;
            this.lines.push(line);
            this.add(line);

            const arrow = new MoveGizmoHandle(this, axes[i]);
            arrow.renderOrder = 1000;
            arrow.userData.skipCastShadow = true;
            arrow.userData.axis = axes[i];
            this.handles.push(arrow);
            this.add(arrow);
        }
    }

    setTarget(target: Vector3) {
        this.parent.position.copy(target);
        this.positionSet.emit(target);
    }

    dispose() {
        for (let line of this.lines) {
            (line.geometry as BufferGeometry).dispose();
            (line.material as MeshBasicMaterial).dispose();
        }
        for (let handle of this.handles) {
            handle.dispose();
        }
        (this.sphere.geometry as BufferGeometry).dispose();
        (this.sphere.material as MeshBasicMaterial).dispose();
    }
}
