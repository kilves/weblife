import {GizmoObject} from "./gizmo-object";
import {WorldObject} from "./worldObject";
import {Axis} from "../../app/model/axis";
import {ScaleGizmoHandle} from "./scale-gizmo-handle";
import {BufferGeometry, Color, Line, MeshBasicMaterial} from "three";

export class ScaleGizmoObject extends GizmoObject {
    lines: Line[] = [];

    constructor(worldObject: WorldObject) {
        super(worldObject);
        const axes: Axis[] = ["x", "y", "z"];


        for (let i = 0; i < 3; i++) {
            const line = new Line(
                new BufferGeometry().setFromPoints(this.points[i]),
                new MeshBasicMaterial({color: new Color(this.colors[i]), depthTest: false})
            );
            line.userData.skipCastShadow = true;
            line.renderOrder = 1000;
            this.lines.push(line);
            this.add(line);

            const box = new ScaleGizmoHandle(this, axes[i]);
            box.userData.skipCastShadow = true;
            this.handles.push(box);
            this.add(box);
        }
    }
}