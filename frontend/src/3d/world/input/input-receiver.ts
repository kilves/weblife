export interface InputReceiver {
    onKeyDown(event: KeyboardEvent);
    onKeyUp(event: KeyboardEvent);
}
