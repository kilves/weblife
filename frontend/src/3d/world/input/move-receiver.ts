import {InputReceiver} from "./input-receiver";
import {SocketService} from "../../../app/service/socket.service";
import {Vector2} from "three";

export class MoveReceiver implements InputReceiver {

    movement: Vector2 = new Vector2();

    onKeyUp(event: KeyboardEvent) {
        if (event.key === "w") {
            this.setMovement({y: 0});
        } else if (event.key === "s") {
            this.setMovement({y: 0});
        } else if (event.key === "d") {
            this.setMovement({x: 0});
        } else if (event.key === "a") {
            this.setMovement({x: 0});
        } else if (event.key === " ") {
            this.jump();
        }
    }

    onKeyDown(event: KeyboardEvent) {
        if (event.key === "w") {
            this.setMovement({y: 1});
        } else if (event.key === "s") {
            this.setMovement({y: -1});
        } else if (event.key === "d") {
            this.setMovement({x: -1});
        } else if (event.key === "a") {
            this.setMovement({x: 1});
        }
    }

    private setMovement(m: {x?: number, y?: number}) {
        if ((m.x === this.movement.x || m.x === undefined) && (m.y === this.movement.y || m.y === undefined)) {
            return;
        }
        if (m.x !== undefined) {
            this.movement.x = m.x;
        }
        if (m.y !== undefined) {
            this.movement.y = m.y;
        }
        SocketService.instance.move(this.movement);
    }
    
    private jump() {
        SocketService.instance.jump();
    }
}
