import { InputReceiver } from "./input-receiver";

export class Controls {
    
    private receivers: Set<InputReceiver> = new Set<InputReceiver>();
    private element: HTMLElement;
    
    initialize(element: HTMLElement) {
        this.element = element;
        element.addEventListener("keydown", this.keyDown.bind(this));
        element.addEventListener("keyup", this.keyUp.bind(this));
    }
    
    addReceiver(receiver: InputReceiver) {
        this.receivers.add(receiver);
    }
    
    removeReceiver(receiver: InputReceiver) {
        this.receivers.delete(receiver);
    }
    
    private keyDown(event: KeyboardEvent) {
        if (event.target !== this.element) {
            return;
        }
        for (let receiver of this.receivers) {
            receiver.onKeyDown(event);
        }
    }
    
    private keyUp(event: KeyboardEvent) {
        if (event.target !== this.element) {
            return;
        }
        for (let receiver of this.receivers) {
            receiver.onKeyUp(event);
        }
    }
}
