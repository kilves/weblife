import {GizmoHandle} from "./gizmo-handle";
import {Axis, AxisMapped} from "../../app/model/axis";
import {Matrix4, Plane, Quaternion, Ray, TorusGeometry, Vector3} from "three";
import {RotateGizmoObject} from "./rotate-gizmo-object";

export class RotateGizmoHandle extends GizmoHandle {
    private targetPos = new Vector3();
    private plane = new Plane();
    private initialPosition = null;
    private initialMatrix = new Matrix4();
    private initialRotation = new Quaternion();
    private handleRotation = new Quaternion();
    private handleMatrix = new Matrix4();
    private fullMatrix = new Matrix4();

    protected get rotations(): AxisMapped<Vector3> {
        return {
            x: new Vector3(0, Math.PI / 2, 0),
            y: new Vector3(Math.PI / 2, 0, 0),
            z: new Vector3(0, 0, 0)
        };
    };

    constructor(protected gizmo: RotateGizmoObject, axis: Axis) {
        super(gizmo, axis);
        this.geometry = new TorusGeometry(1, 0.1, 16, 100);
    }

    moveToRay(ray: Ray) {
        const parentPos = this.gizmo.parent.position;
        const parentRot = this.gizmo.parent.quaternion;
        this.plane.setFromNormalAndCoplanarPoint(this.positions[this.axis].clone().applyQuaternion(this.initialRotation), parentPos);
        ray.intersectPlane(this.plane, this.targetPos);
        if (!this.initialPosition) {
            this.initialPosition = this.targetPos.clone();
            this.initialMatrix.makeRotationFromQuaternion(this.gizmo.worldObject.object.quaternion);
            this.initialRotation.copy(parentRot);
        }
        getAngle(parentPos, this.initialPosition, this.targetPos, this.handleRotation);
        this.handleMatrix.makeRotationFromQuaternion(this.handleRotation);
        this.fullMatrix.multiplyMatrices(this.handleMatrix, this.initialMatrix);
        this.gizmo.worldObject.object.setRotationFromMatrix(this.fullMatrix);
    }

    release() {
        super.release();
        this.gizmo.worldObject.object.updateMatrixWorld(true);
        this.gizmo.rotationSet.emit(this.gizmo.worldObject.object.quaternion);
        this.initialPosition = null;
    }
}


const _a = new Vector3();
const _b = new Vector3();
function getAngle(parentPos: Vector3, initialPos: Vector3, currentPos: Vector3, target = new Quaternion()) {
    _a.subVectors(initialPos, parentPos).normalize();
    _b.subVectors(currentPos, parentPos).normalize();
    target.setFromUnitVectors(_a, _b);
    return target;
}