import {CellItem} from "../../app/model/gameObjectCreate";
import {Resource, ResourceType} from "../../app/model/resource";
import {WorldObject} from "./worldObject";
import {MoveGizmoObject} from "./move-gizmo-object";
import {SocketService} from "../../app/service/socket.service";
import {RotateGizmoObject} from "./rotate-gizmo-object";
import {ScaleGizmoObject} from "./scale-gizmo.object";
import {Object3D} from "three";

export class ItemObject extends WorldObject {
    
    item: CellItem;
    moveGizmo: MoveGizmoObject = new MoveGizmoObject(this);
    rotateGizmo: RotateGizmoObject = new RotateGizmoObject(this);
    scaleGizmo: ScaleGizmoObject = new ScaleGizmoObject(this);
    isItemObject = true;

    constructor(item: CellItem) {
        super(item);
        this.item = item;
        this.add(this.moveGizmo);
        this.add(this.rotateGizmo);
        this.add(this.scaleGizmo);
        this.moveGizmo.positionSet.subscribe(pos => {
            SocketService.instance.setItemPosition(this.item, this.position);
        });
        this.rotateGizmo.rotationSet.subscribe(rot => {
            SocketService.instance.setItemRotation(this.item, rot);
        });
    }

    async updateResources(resources: Resource[]) {
        if (this.resourcesEqual(resources)) {
            return;
        }
        this.item.resources = resources;
        await this.load();
    }

    async load() {
        let object = this.item.resources.find(r => r.type === ResourceType.OBJECT3D);
        if (!object) {
            await this.loadDefault();
        } else {
            await this.loadGltf(`/resources/${object.uuid}.${object.extension}`);
        }
        await super.load();
    }

    update(delta: number) {
        if (this.moveGizmo.visible) {
            this.moveGizmo.update(delta);
        }
        if (this.rotateGizmo.visible) {
            this.rotateGizmo.update(delta);
        }
        if (this.scaleGizmo.visible) {
            this.scaleGizmo.update(delta);
        }
    }

    countAllChildren(): number {
        return this._countChildren(this.object);
    }

    private _countChildren(obj: Object3D, prev: number = 0): number {
        let count = 0;
        for (const child of obj.children) {
            count += 1;
            count += this._countChildren(child);
        }
        return count;
    }


    private resourcesEqual(resources: Resource[]) {
        const oldResources = [...this.item.resources];
        for (const resource of resources) {
            const idx = oldResources.findIndex(r => r.uuid === resource.uuid);
            if (idx === -1) {
                return false;
            }
            oldResources.splice(idx);
        }
        return oldResources.length === 0;
    }
}
