import {
    BackSide,
    BoxGeometry,
    BufferAttribute,
    BufferGeometry,
    DoubleSide,
    Group,
    Mesh,
    MeshBasicMaterial,
    MeshStandardMaterial,
    PlaneGeometry,
    RepeatWrapping,
    TextureLoader,
    Vector2,
    Vector3,
} from "three";
import {Cell} from "../../app/model/cell";
import {Resource, ResourceType} from "../../app/model/resource";
import {PreviewObject} from "./preview-object";
import {GameObjectCreate} from "../../app/model/gameObjectCreate";
import {ItemObject} from "./item-object";
import {BrushConfig, BrushState} from "../../app/model/brush";
import {config} from "rxjs";

export class CellObject extends Group {
    
    plane: Mesh;
    private dimensions: Vector2;
    private preview: PreviewObject = new PreviewObject();
    private brush: Mesh;
    private brushWeights = new Map<number, number>();
    items: ItemObject[] = [];
    isCellObject = true;

    constructor() {
        super();
        this.preview.visible = false;
        const geo = new PlaneGeometry(100, 100, 99, 99);
        this.plane = new Mesh(geo, new MeshStandardMaterial({
            metalness: 0,
            roughness: 1,
        }));
        this.brush = new Mesh(new BufferGeometry(), new MeshBasicMaterial({
            color: 0xff0000,
            transparent: true,
            opacity: 0.5,
            side: DoubleSide
        }));
        this.plane.add(this.brush);
        this.plane.rotateX(-Math.PI / 2);
        this.plane.receiveShadow = true;
        this.plane.castShadow = true;
        this.add(this.plane)
        this.add(this.preview);
    }

    clearBrush() {
        this.brush.visible = false;
    }

    setBrush(pos: Vector3, config: BrushConfig) {
        this.brush.visible = true;
        const vec = new Vector3();
        const vec2 = new Vector2();
        const targetVec2 = new Vector2(pos.x, -pos.z);
        targetVec2.floor();
        this.brush.geometry.dispose();
        this.brush.geometry = new BufferGeometry();

        const position = this.plane.geometry.attributes.position;
        const index = this.plane.geometry.index;
        const indicesInBrush = new Map<number, number>();
        const brushPoints: Vector3[] = [];

        this.brushWeights.clear();
        for (let i = 0, il = position.count; i < il; i++) {
            vec.fromBufferAttribute(position, i);
            vec2.set(vec.x, vec.y);
            const distance = targetVec2.distanceTo(vec2);
            const mult = 0.02 * config.force;
            if (distance < config.size) {
                this.brushWeights.set(i, (1 - distance / config.size) * mult);
                indicesInBrush.set(i, brushPoints.length);
                brushPoints.push(vec.clone());
            }
        }
        const newIndices = [];

        for (let i = 0, il = index.count; i < il; i += 3) {
            const i1 = indicesInBrush.get(index.array[i]);
            const i2 = indicesInBrush.get(index.array[i + 1]);
            const i3 = indicesInBrush.get(index.array[i + 2]);
            if (i1 && i2 && i3) {
                newIndices.push(i1, i2, i3);
            }
        }
        const newIndexArray = Uint16Array.from(newIndices);
        this.brush.geometry.setFromPoints(brushPoints);
        this.brush.geometry.setIndex(
            new BufferAttribute(newIndexArray, 1, false)
        );
        this.brush.geometry.computeVertexNormals();
    }

    paint(brushState: BrushState) {
        if (!this.brush.visible) {
            return;
        }
        const position = this.plane.geometry.getAttribute("position") as BufferAttribute;
        const array = Float32Array.from(position.array);
        let brushMultiplier = 0;
        if (brushState === BrushState.RAISE) {
            brushMultiplier = 1;
        } else if (brushState === BrushState.LOWER) {
            brushMultiplier = -1;
        }
        if (brushState === BrushState.LOWER || brushState === BrushState.RAISE) {
            for (let [i, weight] of this.brushWeights.entries()) {
                const index = i * 3 + 2;
                array[index] += weight * brushMultiplier;
                array[index] = Math.max(0, array[index]);
            }
        } else if (brushState === BrushState.SMOOTH) {
            let avg = 0;
            for (let [i, weight] of this.brushWeights.entries()) {
                avg += array[i * 3 + 2]
            }
            avg /= this.brushWeights.size;
            for (let [i, weight] of this.brushWeights.entries()) {
                const index = i * 3 + 2;
                array[index] += (avg - array[index]) * 0.1;
                array[index] = Math.max(0, array[index]);
            }
        }
        this.plane.geometry.setAttribute("position", new BufferAttribute(array, 3));
        this.plane.geometry.computeVertexNormals();
    }
    
    update(delta: number) {
        if (!this.preview) {
            return;
        }
        this.preview.update(delta);
        for (const itemObject of this.items) {
            itemObject.update(delta);
        }
    }
    
    async load(cell: Cell) {
        for (let resource of cell.resources) {
            if (resource.type === ResourceType.HEIGHTMAP) {
                await this.loadTerrain(resource);
            }
        }
        for (let resource of cell.resources) {
            if (resource.type === ResourceType.TEXTURE) {
                this.loadTexture(resource);
            }
        }
        // this.loadSkybox();
    }
    
    loadPreview(item: GameObjectCreate) {
        this.preview.visible = true;
        // this.preview.load(item);
    }
    
    setPreviewPosition(position: Vector3) {
        this.preview.position.copy(position);
        this.preview.position.y += this.preview.boxSize.y / 2;
    }
    
    loadTexture(resource: Resource) {
        const material = new MeshStandardMaterial();
        const textureLoader = new TextureLoader();
        material.map = textureLoader.load(`/resources/${resource.uuid}.${resource.extension}`);
        material.map.wrapT = material.map.wrapS = RepeatWrapping;
        material.shadowSide = BackSide;
        material.map.repeat = new Vector2(this.dimensions.x / 10, this.dimensions.y / 10);
        this.plane.material = material;
        this.plane.material.needsUpdate = true;
    }

    async getHeightmap(): Promise<Blob> {
        return new Promise((resolve, reject) => {
            const vertices = this.plane.geometry.getAttribute("position");
            let canvas = document.getElementById("dataCanvas") as HTMLCanvasElement;
            canvas.width = 100;
            canvas.height = 100;
            const ctx = canvas.getContext("2d");
            const image = ctx.createImageData(100, 100);
            for (let i = 0; i < vertices.array.length; i++) {
                const index = i * 4;
                const vertIndex = i * 3;
                const val = Math.round(vertices.array[vertIndex + 2] * 10);
                image.data[index] = val;
                image.data[index + 1] = val;
                image.data[index + 2] = val;
                image.data[index + 3] = 255;
            }
            ctx.putImageData(image, 0, 0);
            canvas.toBlob(blob => {
                resolve(blob);
            });
        });
    }
    
    private async loadTerrain(resource: Resource): Promise<void> {
        return new Promise<void>(((resolve, reject) => {
            let heightMap = new Image();
            heightMap.src = `/resources/${resource.uuid}.${resource.extension}?${new Date().getTime()}`;
            heightMap.onload = () => {
                let canvas = document.getElementById("dataCanvas") as HTMLCanvasElement;
                canvas.width = heightMap.width;
                canvas.height = heightMap.height;
                let ctx = canvas.getContext("2d");
                ctx.drawImage(heightMap, 0, 0, heightMap.width, heightMap.height);
                let data = ctx.getImageData(0, 0, heightMap.width, heightMap.height).data;

                let geo = new PlaneGeometry(heightMap.width, heightMap.height, heightMap.width - 1, heightMap.height - 1);
                const vertices = geo.getAttribute("position").array;
                const newArray = new Float32Array(vertices.length);
                newArray.set(vertices);
                for (let i = 0; i < data.length; i += 4) {
                    newArray[i / 4 * 3 + 2] = data[i] * 0.1;
                }
                geo.setAttribute("position", new BufferAttribute(newArray, 3));
                geo.computeVertexNormals();
                this.plane.geometry = geo;
                this.dimensions = new Vector2(heightMap.width, heightMap.height);
                this.add(this.plane);
                resolve();
            };
        }))
    }

    hidePreview() {
        this.preview.visible = false;
    }

    spawnItem(item: ItemObject) {
        this.add(item);
        this.items.push(item);
    }

    removeItem(item: ItemObject) {
        this.remove(item);
        this.items = this.items.filter(i => i !== item);
    }

    private loadSkybox() {
        // TODO move this skybox to cell resources
        const textureLoader = new TextureLoader();
        let materialArray = [];
        let texture_ft = textureLoader.load( '/resources/skybox/front.bmp');
        let texture_bk = textureLoader.load( '/resources/skybox/back.bmp');
        let texture_up = textureLoader.load( '/resources/skybox/top.jpg');
        let texture_dn = textureLoader.load( '/resources/skybox/bottom.jpg');
        let texture_rt = textureLoader.load( '/resources/skybox/right.jpg');
        let texture_lf = textureLoader.load( '/resources/skybox/left.jpg');

        materialArray.push(new MeshBasicMaterial( { map: texture_ft }));
        materialArray.push(new MeshBasicMaterial( { map: texture_bk }));
        materialArray.push(new MeshBasicMaterial( { map: texture_up }));
        materialArray.push(new MeshBasicMaterial( { map: texture_dn }));
        materialArray.push(new MeshBasicMaterial( { map: texture_rt }));
        materialArray.push(new MeshBasicMaterial( { map: texture_lf }));

        for (let i = 0; i < 6; i++)
            materialArray[i].side = BackSide;

        let skyboxGeo = new BoxGeometry( 100, 100, 100);
        let skybox = new Mesh(skyboxGeo, materialArray);
        this.add(skybox);
    }
}
