import {GLTF, GLTFLoader} from "three/examples/jsm/loaders/GLTFLoader";
import {floorPowerOfTwo} from "three/src/math/MathUtils";
import {Mesh, Object3D} from "three";
import {Material} from "three/src/materials/Material";

export class Meshes {
    private static gltfLoader = new GLTFLoader();
    private static cache = new Map<string, GLTF>();

    static get(path: string): Promise<GLTF> {
        return new Promise<GLTF>((resolve, reject) => {
            if (this.cache.has(path)) {
                const gltf = this.cache.get(path);
                const scene = gltf.scene.clone(true);
                this.cloneGeometries(scene);
                return resolve({
                    ...this.cache.get(path),
                    scene: scene,
                    animations: gltf.animations.map(a => a.clone()),
                });
            }
            this.gltfLoader.load(path, gltf => {
                this.cache.set(path, gltf);
                resolve(gltf);
            }, () => {

            }, err => {
                reject(err);
            });
        })
    }

    static cloneGeometries(obj: Object3D) {
        if (obj instanceof Mesh) {
            obj.geometry = obj.geometry.clone();
            obj.material = (obj.material as Material).clone();
        }
        for (const child of obj.children) {
            this.cloneGeometries(child);
        }
    }
}
