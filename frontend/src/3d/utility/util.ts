import {Bone, Box3, Object3D, Ray, SkinnedMesh, Sphere, Vector3} from "three";

export function setAttribute<T>(entities: T | T[], key: keyof T, attribute) {
    if (!entities) {
        return;
    }
    if (Array.isArray(entities)) {
        for (const entity of entities) {
            entity[`__old_${key as string}`] = entity[key];
            entity[key] = attribute;
        }
    } else {
        entities[`__old_${key as string}`] = entities[key];
        entities[key] = attribute;
    }
}

/**
 * SkinnedMesh bounding shape calculations skip bone transforms.
 * Recursively computes all bounding shapes anyway.
 * @param sm
 */
export function fixBoundingShapes(sm: Object3D) {
    const COMPUTE_PRECISION = 500;

    for(const c of sm.children) {
        fixBoundingShapes(c);
    }
    if (!(sm instanceof SkinnedMesh)) {
        return;
    }
    const position = sm.geometry.attributes.position;
    const skip = Math.max(1, Math.floor(position.count / COMPUTE_PRECISION));

    sm.geometry.boundingSphere = new Sphere();
    sm.geometry.boundingBox = new Box3();
    const vec = new Vector3();
    for (let i = 0, il = position.count; i < il; i += skip) {
        vec.fromBufferAttribute(position, i);
        sm.applyBoneTransform(i, vec);
        sm.geometry.boundingBox.expandByPoint(vec);
        sm.geometry.boundingSphere.expandByPoint(vec);
    }
}

export function findBones(object: Object3D, bones: any[] = []): Bone[] {
    if (object.type === "Bone") {
        bones.push(object);
    }
    for (const child of object.children) {
        findBones(child, bones);
    }
    return bones;
}

export function findRootBone(bones: Bone[]) {
    for (const bone of bones) {
        if (!bone.parent || !(bone.parent as any).isBone) {
            return bone;
        }
    }
}

export function restoreAttribute<T>(entities: T | T[], key: keyof T) {
    if (!entities) {
        return;
    }
    if (Array.isArray(entities)) {
        for (const entity of entities) {
            entity[key] = entity[`__old_${key as string}`];
            delete entity[`__old_${key as string}`];
        }
    } else {
        entities[key] = entities[`__old_${key as string}`];
        delete entities[`__old_${key as string}`];
    }
}

const _nv = new Vector3();
const _nb = new Vector3();
const _da = new Vector3();
const _oa = new Vector3();
export function rayPointClosestToRay(a: Ray, b: Ray, target: Vector3 = new Vector3()): Vector3 {
    _nv.copy(a.direction).cross(b.direction);
    _nb.copy(b.direction).cross(_nv).normalize();
    _da.copy(a.direction).normalize();
    const da = _oa.copy(b.origin).sub(a.origin).dot(_nb) / _da.dot(_nb);
    target.copy(a.origin).add(_da.multiplyScalar(da));
    return target;
}