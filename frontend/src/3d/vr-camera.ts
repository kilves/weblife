import {GameCamera} from "./game-camera";
import {Vector3} from "three";

export class VRCamera extends GameCamera {
    private _lookAt = new Vector3();
    update(delta: boolean): boolean {
        super.update(delta);
        if (!this.target) {
            this.position.set(0, 2, 0);
            return true;
        }
        this.position.copy(this.target.position);
        return true;
    }
}