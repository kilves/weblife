import {CameraMode, GameCamera} from "./game-camera";
import {Euler, Object3D, Spherical, Vector2, Vector3, MathUtils, Matrix4} from "three";
import {AnimatedVector} from "./world/math/animated-vector";

const X_ORBIT_SPEED = 3;
const Y_ORBIT_SPEED = 3;
const ZOOM_SPEED = 0.25;
const ZOOM_SLOWDOWN_SPEED = 1;
const Y_MAX = Math.PI / 2 + 0.025;
const Y_MIN = 0.1;
const ZOOM_MIN = 0.1;
const ZOOM_MAX = 100;

export class WalkLockCamera extends GameCamera {
    // Camera is zoomed by [nZoom * delta] (if overrideZoom isn't set) or [overrideZoom] every frame
    nZoom = 0;
    overrideZoom = 0;

    // Camera is rotated by [persistentRotation * delta] every frame
    persistentRotation: Vector2 = new Vector2();

    mode: CameraMode = CameraMode.WALK_LOCK;

    private spherical: Spherical;
    private animatedRotation: AnimatedVector = new AnimatedVector();
    private default: Spherical;
    private startX: number;
    private startY: number;
    private startSpherical: Spherical;
    rotating: boolean;
    
    // Targeting
    private checkSmoothLook = true;
    private previousTargetPosition: Vector3;
    private previousTargetRotation: Euler;

    private previousMatrixWorld: Matrix4;

    get targetPosition(): Vector3 {
        if (this.target && this.target.position) {
            return this.target.position;
        } else {
            return new Vector3();
        }
    }

    get targetRotation(): Euler {
        if (this.target && this.target.rotation) {
            return this.target.rotation;
        } else {
            return new Euler();
        }
    }

    /**
     *
     * @param angle  angle of viewport
     * @param width  width of viewport
     * @param height height of viewport
     * @param near   near clip
     * @param far    far clip
     */
    constructor(angle, width, height, near, far) {
        super(angle, width, height, near, far);
        this.default = new Spherical(8, Math.PI / 3, -Math.PI / 4);
        this.reset();
    }

    update(delta): boolean {
        this.position.setFromSpherical(this.spherical);
        this.setSpherical(
            this.spherical.theta + delta * this.persistentRotation.x,
            this.spherical.phi + delta * this.persistentRotation.y
        );
        if (this.overrideZoom) {
            this.spherical.radius += this.overrideZoom * ZOOM_SPEED * this.spherical.radius;
        } else {
            this.spherical.radius += this.nZoom * delta * ZOOM_SPEED * this.spherical.radius;

            // Basic zoom smoothing
            let zoomSign = Math.sign(this.nZoom);
            if (this.nZoom !== 0) {
                this.nZoom -= zoomSign * ZOOM_SLOWDOWN_SPEED;
            }
            if (Math.sign(this.nZoom) !== zoomSign) {
                this.nZoom = 0;
            }
        }

        // Lock to target
        if (!this.rotating && this.targetHasMoved()) {
            this.smoothLookAtTarget();
        }
        
        if (!this.animatedRotation.finished) {
            this.animatedRotation.update(delta);
            let vec = this.animatedRotation.vector as Vector2;
            this.setSpherical(vec.x, vec.y);
        }

        this.position.add(this.targetPosition);

        // Zoom limits
        this.spherical.radius = MathUtils.clamp(this.spherical.radius, ZOOM_MIN, ZOOM_MAX);

        this.lookAt(this.targetPosition);
        this.updateMatrixWorld(true);

        const updated = !this.previousMatrixWorld || !this.previousMatrixWorld.equals(this.matrixWorld);
        this.previousMatrixWorld = this.matrixWorld.clone();

        this.previousTargetPosition = this.targetPosition.clone();
        this.previousTargetRotation = this.targetRotation.clone();
        return updated;
    }

    setDefaultZoom(value: number) {
        this.default = this.spherical.clone();
        this.default.radius = MathUtils.clamp(value, ZOOM_MIN, ZOOM_MAX);
    }
    
    private targetHasMoved() {
        if (!this.previousTargetPosition || !this.previousTargetRotation) {
            return true;
        }
        return !this.targetPosition.equals(this.previousTargetPosition)
            || !this.targetRotation.equals(this.previousTargetRotation);
    }

    startRotating(x: number, y: number) {
        this.rotating = true;
        this.startSpherical = this.spherical.clone();
        let offsetX = MathUtils.clamp(x, 0, this.width);
        let offsetY = MathUtils.clamp(y, 0, this.height);
        this.startX = offsetX;
        this.startY = offsetY;
    }

    stopRotating() {
        this.rotating = false;
        this.checkSmoothLook = true;
    }

    rotate(x: number, y: number) {
        if (!this.rotating) {
            return;
        }
        let offsetX = MathUtils.clamp(x, 0, this.width);
        let offsetY = MathUtils.clamp(y, 0, this.height);

        let deltaX = (offsetX - this.startX) / this.width;
        let deltaY = (offsetY - this.startY) / this.height;
        let theta = this.startSpherical.theta - deltaX * X_ORBIT_SPEED;
        let phi = this.startSpherical.phi - deltaY * Y_ORBIT_SPEED;
        this.setSpherical(theta, phi);
    }

    private setSphericalTarget(theta, phi) {
        this.animatedRotation.animate(
            new Vector2(this.spherical.theta, this.spherical.phi),
            new Vector2(theta, phi),
            0.1
        );
    }

    private setSpherical(theta: number, phi: number) {
        this.spherical.theta = theta;
        this.spherical.phi = Math.max(Math.min(phi, Y_MAX), Y_MIN);
    }

    private smoothLookAtTarget() {
        this.setSphericalTarget(this.targetRotation.y - Math.PI, this.spherical.phi);
    }

    zoomCamera(delta) {
        let deltaY = Math.sign(delta) * 53;
        this.nZoom = deltaY * ZOOM_SPEED;
    }

    reset() {
        this.spherical = this.default.clone();
    }
}
