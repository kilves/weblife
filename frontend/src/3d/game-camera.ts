import {Camera, Euler, Object3D, PerspectiveCamera, Spherical, Vector2, Vector3} from "three";
import * as THREE from "three";

export enum CameraMode {
    WALK_LOCK,
    FREE,
    FIRST_PERSON
}

export class GameCamera extends PerspectiveCamera {
    target: Object3D;
    protected width: number;
    protected height: number;

    constructor(angle, width, height, near, far) {
        super(angle, width / height, near, far);
        this.width = width;
        this.height = height;
    }
    
    update(delta): boolean {
        return false;
    }

    setSize(width: number, height: number) {
        this.width = width;
        this.height = height;
        this.aspect = width / height;
        this.updateProjectionMatrix();
    }
}
