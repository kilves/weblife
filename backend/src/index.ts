import sourceMap from "source-map-support";
sourceMap.install();

import fs from "fs";

import Koa from "koa";
import bodyParser from "koa-body";
import session from "koa-session";
import Router from "koa-router";
import koaStatic from "koa-static";

import * as users from "./repository/user";
import * as resources from "./repository/resource";
import * as avatars from "./repository/avatar";
import {config} from "./config";
import api from "./api";
import mount from "koa-mount";

import {ServerError} from "./error/serverError";
import {sendFile} from "./util";
import {getLogger} from "log4js";
import * as log4js from "log4js";

log4js.configure({
    appenders: {
        app: {
            type: "dateFile",
            filename: "logs/yyyy-MM-dd.log",
            alwaysIncludePattern: true
        },
        console: { type: "console" }
    },
    categories: {
        default: {
            appenders: ["app", "console"],
            level: "info"
        }
    }
});
const logger = getLogger("general");
logger.level = "info";
import server from "./server";
import {NotFoundError} from "./error/notFoundError";
import {readdir, lstat} from "node:fs/promises";
import "./dataSource";

const PORT = 3001;

if (!fs.existsSync(config.resourceDir)) {
    logger.error(`Resource directory ${config.resourceDir} does not exist`);
    process.exit();
}

const app = new Koa();
const router = new Router();
app.keys = [config.secret];
app.use(async (ctx, next) => {
    try {
        await next();
    } catch (err) {
        if (err instanceof ServerError) {
            err.handle(ctx);
        } else {
            ctx.status = 500;
            ctx.body = "Internal server error";
            logger.error(err);
        }
    }
});
app.use(bodyParser({
    multipart: true
}));
app.use(session(app));

app.use(mount("/xrAssets", koaStatic(
    "/home/node/backend/node_modules/@webxr-input-profiles/assets/dist/profiles"
)));

router.get("/resources/:name", async ctx => {
    let nameAndExtension = ctx.params.name.split(".");
    const base = "/home/node/backend/resource";
    const files = await readdir(base);
    for (const file of files) {
        const filepath = `${base}/${file}`;
        if ((await lstat(filepath)).isFile() && file === nameAndExtension.join(".")) {
            await sendFile(ctx, filepath);
            return;
        }
    }
    let name = nameAndExtension[0];
    let extension = nameAndExtension[1];

    let resource = await resources.get(name);
    if (!resource) {
        ctx.response.status = 404;
        return;
    }
    await sendFile(ctx, `${config.resourceDir}/${resource.uuid}.${resource.extension}`);
});

router.get("/avatar/:id", async ctx => {
    const id = +ctx.params.id;
    const avatar = await avatars.get(id);
    if (!avatar) {
        throw new NotFoundError();
    }
    await sendFile(ctx, `${config.resourceDir}/avatars/${avatar.uuid}.${avatar.extension}`)
});

router.get("/character/:uuid/character.glb", async ctx => {
    const uuid = ctx.params.uuid;
    const targetClient = server.findClientByCharacterUuid(uuid);
    await sendFile(ctx, `${config.resourceDir}/characters/${targetClient.user.id}/character.glb`);
});

router.get("/character/textures/:texture", async ctx => {
    let user = await users.getIfAuthorized(ctx.cookies.get("token"));
    let texture = ctx.params.texture;
    await sendFile(ctx, `${config.resourceDir}/characters/${user.id}/textures/${texture}`);
});

router.use("/api", api.routes());

app.use(router.routes());

logger.info(`Server started on port ${PORT}`);

process.on("SIGTERM", () => {
    console.log("term");
    process.exit(0);
});

process.on("exit", () => {
    process.exit(0);
})

process.on("SIGINT", () => {
    console.log("int");
    process.exit(0);
});

app.listen(PORT);
