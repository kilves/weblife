import {ComponentContainer, Type} from "./util";

export class Entities {
    components: ComponentContainer<any> = new ComponentContainer<any>();

    constructor() {

    }

    add() {

    }

    remove() {

    }

    getAllOfType<T>(type: Type<T>): readonly T[] {
        return this.components.get(type) || [];
    }
}