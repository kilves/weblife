import GameItem from "../gameItem";
import {ItemNotFoundError} from "../error/itemNotFoundError";
import {Vector3} from "three";
import * as cellItem from "../../repository/cellItem";
import GameCell from "../gameCell";
import {ItemPositionData, ItemRotationData} from "../../websocket/websocket";

export class GameItemService {

    items: GameItem[] = [];

    findItem(uuid: string): GameItem {
        const item = this.items.find(i => i.uuid === uuid);
        if (!item) {
            throw new ItemNotFoundError(uuid);
        }
        return item;
    }

    update() {
        for (let item of this.items) {
            if (item.updated) {
                // cellChanges.objects.push(item.serialize());
                item.updated = false;
            }
        }
    }
}