import {EventEmitter} from "events";
import GameCharacter from "./gameCharacter";
import GameCell, {CellMessage} from "./gameCell";
import {Observable, Subject} from "rxjs";
import {AvatarAnimation} from "../model/avatarAnimation";
import {LoginUser} from "../model/loginUser";
import {Avatar} from "../model/avatar";
import {AvatarBoneSlot} from "../model/avatarBoneSlot";

export interface SentMessage {
    escapedMessage: string;
    channel: SentMessageChannel;
}

export interface SentMessageChannel {
    name: string;
    private: boolean;
}

export default class GameUser {
    username: string;
    id: number;
    character: GameCharacter;
    cell: GameCell;

    readonly onKicked = new Subject<void>();
    readonly onSay = new Subject<SentMessage>();
    readonly onReceivePrivateMessage = new Subject<CellMessage>();
    readonly onShowOwnMessage = new Subject<CellMessage>();

    constructor(user: LoginUser, cell: GameCell, avatar: Avatar, animations: AvatarAnimation[], bones: AvatarBoneSlot[]) {
        this.username = user.username;
        this.id = user.id;
        this.character = new GameCharacter(this, avatar, animations, bones);
        this.cell = cell;
    }

    kick() {
        this.onKicked.next();
    }

    say(escapedMessage: SentMessage) {
        this.onSay.next(escapedMessage);
    }

    receivePrivateMessage(message: CellMessage) {
        this.onReceivePrivateMessage.next(message);
    }

    showOwnMessage(message: CellMessage) {
        this.onReceivePrivateMessage.next({
            sender: this.username,
            channel: message.channel,
            text: message.text
        });
    }
};
