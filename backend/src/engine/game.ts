import GameRoom from "./gameCell";

import * as cells from "../repository/cell";
import * as cellObjects from "../repository/cellObject";
import {getLogger} from "log4js";

const logger = getLogger("general");
const cellLogger = getLogger("cell");

export default class Game {

    rooms: GameRoom[];

    constructor() {
        this.rooms = [];
    }

    async loadRoom(roomName: string): Promise<GameRoom> {
        let room = await cells.get(roomName);
        if (!room) {
            throw `Room (${roomName}) does not exist`;
        }
        let items = await cellObjects.listByRoom(room.id);
        let gameRoom = new GameRoom(room);
        await gameRoom.loadItems(items);
        await gameRoom.loadCollision();
        this.rooms.push(gameRoom);
        return gameRoom;
    }

    async findRoom(name: string): Promise<GameRoom> {
        for (let room of this.rooms) {
            if (room.name === name) {
                return room;
            }
        }
        return this.loadRoom(name);
    }

    _mainLoop() {
        for (let cell of this.rooms) {
            cell.update(0.05);
        }
    }

    start() {
        setInterval(this._mainLoop.bind(this), 50);
    }
};
