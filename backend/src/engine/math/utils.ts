import {Quaternion, Ray, Vector2, Vector3} from "three";

export interface SVector2 {
    x: number;
    y: number;
}

export interface SVector3 extends SVector2{
    z: number;
}

export interface SQuaternion extends SVector3{
    w: number;
}

export function serializeVector2(vector: Vector2): SVector2 {
    return {
        x: vector.x,
        y: vector.y,
    }
}

export function serializeVector3(vector: Vector3): SVector3 {
    return {
        x: vector.x,
        y: vector.y,
        z: vector.z
    }
}

export function serializeQuaternion(quaternion: Quaternion): SQuaternion {
    return {
        x: quaternion.x,
        y: quaternion.y,
        z: quaternion.z,
        w: quaternion.w
    };
}