import {CellChanges} from "../gameCell";
import {SAnimation, SBone, SCharacter} from "../gameCharacter";
import {SItem} from "../gameItem";
import * as uuid from "uuid";

const textEncoder = new TextEncoder();

const FLOAT_SIZE = 4;
const INT_SIZE = 4;
const UUID_SIZE = 16;

const BONE_VECTOR3_PRESENCE_BIT = 0x01;
const BONE_QUATERNION_PRESENCE_BIT = 0x02;
const BONE_NAME_PRESENCE_BIT = 0x04;

const CHARACTER_NAME_PRESENCE_BIT = 0x01;

export function changesToBuffer(changes: CellChanges): Buffer {
    const buffer = Buffer.alloc(getSize(changes));
    buffer.writeUint8(0x02, 0);
    buffer.writeUInt32BE(changes.characters.length, 1);
    let i = 5;
    for (const character of changes.characters) {
        const nameBit = character.name ? CHARACTER_NAME_PRESENCE_BIT : 0x00;
        buffer.writeUint8(nameBit, i); i += 1;
        if (character.name) {
            i = writeString(buffer, i, character.name);
        }
        buffer.writeFloatBE(character.position.x, i); i += FLOAT_SIZE;
        buffer.writeFloatBE(character.position.y, i); i += FLOAT_SIZE;
        buffer.writeFloatBE(character.position.z, i); i += FLOAT_SIZE;
        buffer.writeUint8(+character.me, i); i += 1;
        buffer.writeFloatBE(character.rx, i); i += FLOAT_SIZE;
        buffer.writeFloatBE(character.ry, i); i += FLOAT_SIZE;
        buffer.writeFloatBE(character.s, i); i += FLOAT_SIZE;
        writeUUID(buffer, i, character.uuid); i += UUID_SIZE;
        buffer.writeUInt32BE(character.id, i); i += INT_SIZE;
        buffer.writeUInt32BE(character.animations?.length || 0, i); i += INT_SIZE;
        for (const animation of character.animations || []) {
            i = writeAnimation(buffer, animation, i);
        }

        buffer.writeUInt32BE(character.bones?.length || 0, i); i += INT_SIZE;
        for (const bone of character.bones || []) {
            i = writeBone(buffer, bone, i);
        }
    }

    buffer.writeUInt32BE(changes.objects.length, i); i+= INT_SIZE;
    for (const item of changes.objects) {
        buffer.writeFloatBE(item.position.x, i); i+= FLOAT_SIZE;
        buffer.writeFloatBE(item.position.y, i); i+= FLOAT_SIZE;
        buffer.writeFloatBE(item.position.z, i); i+= FLOAT_SIZE;
        buffer.writeFloatBE(item.rotation.x, i); i+= FLOAT_SIZE;
        buffer.writeFloatBE(item.rotation.y, i); i+= FLOAT_SIZE;
        buffer.writeFloatBE(item.rotation.z, i); i+= FLOAT_SIZE;
        buffer.writeFloatBE(item.rotation.w, i); i+= FLOAT_SIZE;
        writeUUID(buffer, i, item.uuid); i+= UUID_SIZE;
    }
    return buffer;
}

function writeAnimation(buffer: Buffer, animation: SAnimation, i: number) {
    i = writeString(buffer, i, animation.animationId);
    i = writeString(buffer, i, animation.name);
    return i;
}

function writeBone(buffer: Buffer, bone: SBone, i: number): number {
    const positionBit = bone.position ? BONE_VECTOR3_PRESENCE_BIT : 0x00;
    const quaternionBit = bone.quaternion ? BONE_QUATERNION_PRESENCE_BIT : 0x00;
    const namePresenceBit = bone.name ? BONE_NAME_PRESENCE_BIT : 0x00;
    buffer.writeUint8(positionBit | quaternionBit | namePresenceBit, i); i+= 1;
    buffer.writeUInt32BE(bone.id, i); i += INT_SIZE;
    if (bone.position) {
        buffer.writeFloatBE(bone.position.x, i); i += FLOAT_SIZE;
        buffer.writeFloatBE(bone.position.y, i); i += FLOAT_SIZE;
        buffer.writeFloatBE(bone.position.z, i); i += FLOAT_SIZE;
    }
    if (bone.quaternion) {
        buffer.writeFloatBE(bone.quaternion.x, i); i += FLOAT_SIZE;
        buffer.writeFloatBE(bone.quaternion.y, i); i += FLOAT_SIZE;
        buffer.writeFloatBE(bone.quaternion.z, i); i += FLOAT_SIZE;
        buffer.writeFloatBE(bone.quaternion.w, i); i += FLOAT_SIZE;
    }
    if (bone.name) {
        i = writeString(buffer, i, bone.name);
    }
    return i;
}

function getSize(changes: CellChanges) {
    let size = 5; // header + characters length
    for (const character of changes.characters) {
        size += getCharacterLength(character);
    }
    size += INT_SIZE; // objects length
    for (const object of changes.objects) {
        size += getItemLength(object);
    }
    return size;
}

function getCharacterLength(character: SCharacter) {
    let size = 1 + 6 * FLOAT_SIZE + 1 + UUID_SIZE + 4 * INT_SIZE
    if (character.name) {
        size += textEncoder.encode(character.name).byteLength + INT_SIZE;
    }
    for (const animation of character.animations || []) {
        size += 2 * INT_SIZE
        size += Buffer.byteLength(animation.name, "utf-8");
        size += Buffer.byteLength(animation.animationId, "utf-8");
    }
    for (const bone of character.bones || []) {
        size += INT_SIZE;
        if (bone.position) {
            size += 3 * FLOAT_SIZE;
        }
        if (bone.quaternion) {
            size += 4 * FLOAT_SIZE;
        }
        if (bone.name) {
            size += textEncoder.encode(bone.name).byteLength + INT_SIZE;
        }
    }
    for (const animation of character.animations || []) {
        size += textEncoder.encode(animation.animationId).byteLength + INT_SIZE;
        size += textEncoder.encode(animation.name).byteLength + INT_SIZE;
    }
    return size;
}

function getItemLength(item: SItem) {
    return 7 * FLOAT_SIZE + UUID_SIZE;
}

function writeUUID(buffer: Buffer, i: number, uid: string) {
    const bytes = uuid.parse(uid);
    buffer.set(bytes, i);
}

function writeString(buffer: Buffer, i: number, str: string): number {
    const bytes = textEncoder.encode(str);
    buffer.writeUint32BE(bytes.byteLength, i); i+= INT_SIZE;
    buffer.set(bytes, i);
    return i + bytes.byteLength;
}