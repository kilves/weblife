export interface Type<T> extends Function { new (...args: any[]): T;}
export class ComponentContainer<T> extends Map<Type<T>, T[]> {}