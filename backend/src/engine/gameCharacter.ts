import {v4 as uuid} from "uuid";
import {MathUtils, Quaternion, Vector2, Vector3} from "three";
import {serializeVector3, SQuaternion, SVector3} from "./math/utils";
import GameUser from "./gameUser";
import {Avatar} from "../model/avatar";
import {AvatarAnimation} from "../model/avatarAnimation";
import AvatarAnimations from "../view/fullAvatar";
import {AvatarBoneSlot} from "../model/avatarBoneSlot";

export interface SCharacter {
    position: SVector3;
    me: boolean;
    rx: number;
    ry: number;
    s: number;
    uuid: string;
    id: number;
    name?: string;
    animations?: SAnimation[],
    bones?: SBone[]
}


export interface SAnimation {
    name: string;
    animationId: string;
}

export interface SBone {
    id: number;
    position?: SVector3;
    quaternion?: SQuaternion;
    name?: string;
}

export default class GameCharacter {

    position: Vector3;
    delta: Vector3;
    rotationDelta: Vector2;
    rotation: Vector2;
    uuid: string;
    speed: number;
    rotationSpeed: number;
    jumpStrength: number;
    updated: boolean;
    ground: boolean;
    spawned = false;
    body: any; // TODO what the fuck is this for?
    id: number;
    bones: {
        [k: number]: {
            name: string;
            position: Vector3,
            quaternion: Quaternion
        }
    } = {};

    constructor(private user: GameUser, avatar: Avatar,
                private animations: AvatarAnimation[],
                private avatarBoneSlots: AvatarBoneSlot[]) {
        this.position = new Vector3();
        this.delta = new Vector3();
        this.rotationDelta = new Vector2();
        this.rotation = new Vector2();
        this.uuid = uuid();
        this.speed = 3;
        this.rotationSpeed = 2;
        this.jumpStrength = 4.5;
        this.updated = false;
        this.body = null;
        this.ground = false;
        this.id = avatar.id;
        for (const boneSlot of this.avatarBoneSlots) {
            this.setBone(
                boneSlot.boneSlotId,
                0, 0, 0,
                0, 0, 0, 0,
                boneSlot.name
            )
        }
    }

    setBone(id: number,
            x: number, y: number, z: number,
            rx: number, ry: number, rz: number, rw: number, name?: string) {
        if (!Object.hasOwn(this.bones, id)) {
            this.bones[id] = {
                name: "",
                position: new Vector3(),
                quaternion: new Quaternion()
            }
        }
        if (name) {
            this.bones[id].name = name;
        }
        this.bones[id].position.set(x, y, z);
        this.bones[id].quaternion.set(rx, ry, rz, rw);
    }

    update(delta: number) {
        this.updated = false;
        let originalPosition = this.position.clone();
        this.rotation.add(this.rotationDelta.clone().multiplyScalar(delta));

        let quat = new Quaternion().setFromAxisAngle(new Vector3(0, 1, 0), this.rotation.x);
        let positionDelta = this.delta.clone().multiplyScalar(delta).applyQuaternion(quat);
        this.position.add(positionDelta);

        if (!this.position.equals(originalPosition)) {
            this.updated = true;
        }
    }

    setMovement(x: number, y: number) {
        let delta2d = new Vector2(
            MathUtils.clamp(x, -1, 1),
            MathUtils.clamp(y, -1, 1)
        );
        delta2d.clampLength(this.speed, this.speed);
        this.delta.x = delta2d.x;
        this.delta.z = delta2d.y;
    }

    setRotation(x: number, y: number) {
        this.rotationDelta.x = MathUtils.clamp(x, -1, 1);
        this.rotationDelta.y = MathUtils.clamp(y, -1, 1);
        this.rotationDelta.multiplyScalar(this.rotationSpeed);
    }

    jump() {
        if (this.ground) {
            this.delta.y = this.jumpStrength;
        }
    }

    serialize(user?: GameUser): SCharacter {
        return {
            position: serializeVector3(this.position),
            rx: this.rotation.x,
            ry: this.rotation.y,
            id: this.id,
            s: this.speed,
            uuid: this.uuid,
            me: false
        };
    }

    serializeFull(): SCharacter {
        return {
            ...this.serialize(),
            name: this.user.username,
            animations: this.animations.map(a => ({
                name: a.name,
                animationId: a.animationId
            })),
            bones: Object.getOwnPropertyNames(this.bones).map((k: any) => ({
                id: k,
                name: this.bones[k].name,
                position: this.bones[k].position ? {
                    x: this.bones[k].position.x,
                    y: this.bones[k].position.y,
                    z: this.bones[k].position.z,
                } : undefined,
                quaternion: this.bones[k].quaternion ? {
                    x: this.bones[k].position.x,
                    y: this.bones[k].position.y,
                    z: this.bones[k].position.z,
                    w: this.bones[k].quaternion.w,
                } : undefined
            }))
        }
    }
};
