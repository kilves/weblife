import {Quaternion, Vector3} from "three";
import {v4 as uuid} from "uuid";
import {serializeQuaternion, serializeVector3, SQuaternion, SVector3} from "./math/utils";
import {CellObject} from "../model/cellObject";
import {Resource} from "../model/resource";

export interface SItem {
    position: SVector3;
    rotation: SQuaternion;
    uuid: string;
    name: string;
    resources?: {
        uuid: string;
        extension: string;
        type: string;
        name: string|null;
    }[];
}

export default class GameItem {

    updated: boolean;
    id: number;
    position: Vector3;
    rotation: Quaternion;
    uuid: string;
    name: string;

    constructor(item: CellObject, public resources: Resource[]) {
        this.id = item.id;
        this.updated = false;
        this.position = new Vector3(item.posX, item.posY, item.posZ);
        this.rotation = new Quaternion(item.rotX, item.rotY, item.rotZ, item.rotW);
        this.name = item.name;
        this.uuid = uuid();
    }
    
    serialize(): SItem {
        return {
            position: serializeVector3(this.position),
            rotation: serializeQuaternion(this.rotation),
            uuid: this.uuid,
            name: this.name,
        }
    }

    serializeFull(): SItem {
        return {
            ...this.serialize(),
            resources: this.resources.map(r => ({
                uuid: r.uuid,
                extension: r.extension,
                type: r.type,
                name: r.name
            }))
        };
    }
};
