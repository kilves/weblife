import {MathUtils, Vector3} from "three";
import GameCharacter, {SCharacter} from "./gameCharacter";
import GameItem , {SItem} from "./gameItem";
import GameUser, {SentMessageChannel} from "./gameUser";
import * as resources from "../repository/resource";
import * as cellItem from "../repository/cellItem";
import {config} from "../config";
import {getLogger, Logger} from "log4js";
import {Subject} from "rxjs";
import {ItemPositionData, ItemRotationData, JoinData, JumpData, MessageData, MoveData, TeleportData} from "../websocket/websocket";
import escape from "escape-html";
import * as users from "../repository/user";
import * as avatars from "../repository/avatar";
import {UnauthorizedError} from "../error/unauthorizedError";
import {NotFoundError} from "../error/notFoundError";
import {ItemNotFoundError} from "./error/itemNotFoundError";
import {BufferCommand, Command, GameWebSocket} from "../typings/websocket";
import {CellObject} from "../model/cellObject";
import {Resource} from "../model/resource";
import {Cell} from "../model/cell";
import * as syncProtocol from "./protocol/syncProtocolWriter";
import {stripVTControlCharacters} from "node:util";
import {changesToBuffer} from "./protocol/syncProtocolWriter";

const logger = getLogger("cell");
logger.level = "info";

// TODO find typescript equivalent
const pngparse = require("pngparse");


export interface CellChanges {
    characters: SCharacter[];
    objects: SItem[];
}

export interface SCell {
    characters: SCharacter[];
    objects: SItem[];
}

export interface CellMessage {
    sender: string;
    channel: SentMessageChannel;
    text: string;
}

export default class GameCell {

    name: string;
    id: number;
    clients: GameWebSocket[];
    collision: Float32Array;
    gravity: number;
    items: GameItem[] = [];
    chatLogger: Logger;
    commands: {
        join: Command<JoinData>
        move: Command<MoveData>,
        teleport: Command<TeleportData>
        say: Command<MessageData>,
        jump: Command<JumpData>,
        ipos: Command<ItemPositionData>,
        irot: Command<ItemRotationData>,
    }
    bufferMessages: {
        bones: BufferCommand
    }
    readonly onUpdate = new Subject<CellChanges>();

    constructor(cell: Cell) {
        this.name = cell.name;
        this.id = cell.id;
        this.clients = [];
        this.collision = new Float32Array(10000);
        this.gravity = -9.81;
        this.chatLogger = getLogger(`chat[${this.name}]`)
        this.commands = {
            join: this.join.bind(this),
            move: this.move.bind(this),
            teleport: this.teleport.bind(this),
            say: this.message.bind(this),
            jump: this.jump.bind(this),
            ipos: this.ipos.bind(this),
            irot: this.irot.bind(this),
        };
        this.bufferMessages = {
            bones: this.bones.bind(this)
        };
    }

    async teleport(ws: GameWebSocket, pos: TeleportData) {
        ws.data.user.character.position.set(pos.x, pos.y, pos.z);
    }

    async move(ws: GameWebSocket, data: MoveData) {
        if (!Number.isInteger(data.x) || !Number.isInteger(data.y)) {
            return;
        }
        if (data.y === 0 && data.x === 0) {
            this.broadcastAnimation(ws.data.user, "walk", "stop");
            this.broadcastAnimation(ws.data.user, "idle", "start");
        } else if (data.y > 0) {
            this.broadcastAnimation(ws.data.user, "walk", "start");
            this.broadcastAnimation(ws.data.user, "idle", "stop");
        }
        ws.data.user.character.setMovement(0, data.y);
        ws.data.user.character.setRotation(data.x, 0);
    }

    broadcastItemUpdate(item: GameItem) {
        const changes = changesToBuffer({
            objects: [item.serializeFull()],
            characters: []
        });
        this.broadcast(changes);
    }

    broadcastAnimation(user: GameUser, animation: string, action: "start"|"stop"|"oneshot") {
        this.broadcast({
            c: "anim",
            uuid: user.character.uuid,
            animation: animation,
            action
        });
    }

    addClient(client: GameWebSocket) {
        this.clients.push(client);
    }

    async addUserTemp(websocketToken: string, avatarId: number) {
        let user = await users.getByWebsocketTokenIfAuthorized(websocketToken);
        const avatar = await avatars.get(avatarId, user.id);
        if (!avatar || avatar.loginUserId !== user.id) {
            throw new UnauthorizedError();
        }
        const animations = await avatar.avatarAnimations;
        const bones = await avatar.avatarBoneSlots;
        let userObject = new GameUser(user, this, avatar, animations, bones);

        await this.addUser(userObject);
        return userObject;
    }

    async join(ws: GameWebSocket, data: JoinData) {
        let token = data.token;
        if (!token) {
            logger.error("Failed to parse token");
            return;
        }
        let user = await this.addUserTemp(token, data.avatar);
        ws.data.user = user;

        if (!user) {
            logger.error("Couldn't find user");
            return;
        }

        const onUserKicked = user.onKicked.subscribe(() => {
            ws.send(JSON.stringify({
                c: "kick"
            }));
        });

        const room = ws.data.cell;

        user.onReceivePrivateMessage.subscribe(message => {
            ws.send(JSON.stringify({
                c: "message",
                ...message
            }));
        });

        let cellData = room.serializeFull();
        cellData.characters = this.populateMe(cellData.characters, user);
        const cellChangesBuffer = changesToBuffer(cellData);
        ws.send(cellChangesBuffer);
        ws.send(JSON.stringify({c: "joinChannel", channel: "local"}));
        ws.send(JSON.stringify({
            c: "message",
            sender: "system",
            channel: {
                name: "local",
                private: false
            },
            text: "Joined channel local",
        }))
        this.broadcastEveryoneElse(ws, changesToBuffer({
            characters: [user.character.serializeFull()],
            objects: []
        }));
        setTimeout(() => {
            this.broadcastAnimation(user, "idle", "start");
        }, 100)
    }

    broadcast(obj: any) {
        let res = null;
        if (obj instanceof Buffer) {
            res = obj;
        } else {
            res = JSON.stringify(obj);
        }
        for (let client of this.clients) {
            client.send(res);
        }
    }

    broadcastEveryoneElse(ws: GameWebSocket, obj: any) {
        for (let client of this.clients) {
            if (client === ws) {
                return;
            }
            if (obj instanceof Buffer) {
                client.send(obj);
            } else {
                client.send(JSON.stringify(obj));
            }
        }
    }

    async jump(ws: GameWebSocket) {
        ws.data.user.character.jump();
        this.broadcastAnimation(ws.data.user, "jump", "oneshot");
    }

    async message(ws: GameWebSocket, data: MessageData) {
        if (!data.message) {
            return;
        }
        let escapedMessage = escape(data.message.text);
        ws.data.user.cell.chatLogger.info(`${ws.data.user.username} says: ${escapedMessage}`);
        ws.data.user.say({
            channel: data.message.channel,
            escapedMessage
        });
    }

    async ipos(ws: GameWebSocket, d: ItemPositionData) {
        const item = this.findItem(d.uuid);
        item.position = new Vector3(d.x, d.y, d.z);
        await this.updateItem(item);
    }

    async irot(ws: GameWebSocket, d: ItemRotationData) {
        const item = this.findItem(d.uuid);
        item.rotation.set(d.x, d.y, d.z, d.w);
        ws.data.user.character
        await this.updateItem(item);
    }

    async bones(ws: GameWebSocket, data: Buffer) {
        const boneCount = data.readUint8(0);
        for (let i = 0; i < boneCount; i++) {
            const offset = 1 + i * 8 * 4;
            const boneSlotId = data.readUint32LE(offset);
            ws.data.user.character.setBone(
                boneSlotId,
                data.readFloatLE(offset + 1),
                data.readFloatLE(offset + 2),
                data.readFloatLE(offset + 3),
                data.readFloatLE(offset + 4),
                data.readFloatLE(offset + 5),
                data.readFloatLE(offset + 6),
                data.readFloatLE(offset + 7),
            );
        }
    }

    async updateItem(item: GameItem) {
        item.updated = true;
        await cellItem.update(item.id, {
            pos_x: item.position.x,
            pos_y: item.position.y,
            pos_z: item.position.z,
            rot_x: item.rotation.x,
            rot_y: item.rotation.y,
            rot_z: item.rotation.z,
            rot_w: item.rotation.w,
        })
    }

    findItem(uuid: string): GameItem {
        const item = this.items.find(i => i.uuid === uuid);
        if (!item) {
            throw new ItemNotFoundError(uuid);
        }
        return item;
    }

    setDefaultCollision() {
        for (let i = 0; i < 10000; i += 3) {
            this.collision[i / 3] = 0.1;
        }
    }

    async loadCollision() {
        return new Promise<void>((async (resolve, reject) => {
            let cellResources = await resources.listCellResources(this.id);
            let hm: Resource | null = null;
            for (let resource of cellResources) {
                if (resource.type === "HEIGHTMAP" && resource.extension === "png") {
                    hm = resource;
                }
            }
            if (!hm) {
                this.setDefaultCollision();
                return resolve();
            }
            let resourcePath = `${config.resourceDir}/${hm.uuid}.${hm.extension}`;
            pngparse.parseFile(resourcePath, (err: any, data: any) => {
                if (err) {
                    this.setDefaultCollision();
                    logger.error(err);
                    return resolve();
                }
                for (let i = 0; i < data.data.length; i += 4) {
                    this.collision[i / 4] = data.data[i] * 0.1;
                }
                resolve();
            });
        }));
    }

    update(delta: number) {
        let cellChanges: CellChanges = {
            characters: [],
            objects: []
        };
        for (let client of this.clients) {
            if (!client.data.user) {
                continue;
            }
            const character = client.data.user.character;
            character.delta.y += this.gravity * delta;
            character.update(delta);
            character.ground = false;

            let collisionX = character.position.x + 50 - 0.5;
            let collisionY = character.position.z + 50 - 0.5;
            let collision = this.getCollision(collisionX, collisionY);
            if (character.position.y < collision) {
                character.ground = true;
                character.position.y = collision;
                character.delta.y = 0;
            }

            if (!character.spawned) {
                cellChanges.characters.push(character.serializeFull());
                character.spawned = true;
                character.updated = false;
            }
            if (character.updated) {
                cellChanges.characters.push(character.serialize());
            }
        }
        for (const client of this.clients) {
            if (!client.data.user || !client.data.user.character) {
                continue;
            }
            const changes = {
                ...cellChanges,
                characters: this.populateMe(cellChanges.characters, client.data.user)
            };
            const buffer = syncProtocol.changesToBuffer(changes);
            client.send(buffer);
            /*
            client.send(JSON.stringify({
                c: "cell",
                ...cellChanges,
                characters: this.populateMe(cellChanges.characters, client.data.user)
            }));
             */
        }
    }

    addUser(user: GameUser) {
        const saySub = user.onSay.subscribe(escapedMessage => {
            if (!escapedMessage.channel.private) {
                this.broadcast({
                    c: "message",
                    sender: user.username,
                    channel: escapedMessage.channel,
                    text: escapedMessage.escapedMessage
                });
            } else {
                const targetClient = this.clients.find(u => u.data.user.username === escapedMessage.channel.name);
                if (targetClient) {
                    const targetUser = targetClient.data.user;
                    const message = {
                        sender: user.username,
                        channel: {
                            name: user.username,
                            private: true
                        },
                        text: escapedMessage.escapedMessage
                    };
                    user.showOwnMessage({
                        ...message,
                        channel: {
                            name: targetUser.username,
                            private: true
                        }
                    });
                    targetUser.receivePrivateMessage(message);
                }
            }
        });
    }

    async loadItems(items: CellObject[]) {
        for (let item of items) {
            this.items.push(new GameItem(item, await item.resources));
        }
    }

    serialize(): SCell {
        return {
            characters: this.clients.map(c => c.data.user.character.serializeFull()),
            objects: this.items.map(i => i.serialize())
        };
    }

    serializeFull(): SCell {
        return {
            characters: this.clients.map(c => c.data.user.character.serializeFull()),
            objects: this.items.map(i => i.serializeFull())
        }
    }

    barycentricCollision(integerX: number, integerY: number, fractionX: number, fractionY: number) {
        if (fractionX < fractionY) //the upper triangle
        {
            return (1 - fractionY) * this._c(integerX, integerY) +
                fractionX * this._c(integerX + 1, integerY + 1) +
                (fractionY - fractionX) * this._c(integerX, integerY + 1);
        }
        else //the lower triangle
        {
            return (1 - fractionX) * this._c(integerX, integerY) +
                fractionY * this._c(integerX + 1, integerY + 1) +
                (fractionX - fractionY) * this._c(integerX + 1, integerY);
        }
    }

    _c(x: number, y: number) {
        return this.collision[x + 100 * y];
    }

    getCollision(x: number, y: number) {
        x = MathUtils.clamp(x, 0, 100);
        y = MathUtils.clamp(y, 0, 100);
        let ix = Math.floor(x);
        let iy = Math.floor(y);
        let dx = x - ix;
        let dy = y - iy;
        return this.barycentricCollision(ix, iy, dx, dy);
    }

    disconnect(ws: GameWebSocket) {
        this.clients = this.clients.filter(c => c !== ws);
        if (ws.data.user) {
            this.broadcastEveryoneElse(ws, {
                c: "destroy",
                character: ws.data.user.character.serialize()
            });
        }
    }

    destroyItem(uuid: string) {
        const item = this.items.find(o => o.uuid === uuid);
        if (!item) {
            return;
        }
        this.items = this.items.filter(o => o.uuid !== uuid);
        this.broadcast({
            c: "destroyObject",
            object: item.serializeFull()
        });
    }

    async spawnItem(item: CellObject) {
        let gameItem = new GameItem(item, await item.resources);
        this.items.push(gameItem);
        this.broadcastItemUpdate(gameItem);
    }

    getItem(uuid: string) {
        const item =  this.items.find(o => o.uuid === uuid);
        if (!item) {
            throw new NotFoundError();
        }
        return item;
    }

    private populateMe(characters: SCharacter[], user: GameUser): SCharacter[] {
        return characters.map(character => ({
            ...character,
            me: user.character.uuid === character.uuid
        }));
    }
};
