import {EngineError} from "./engineError";

export class ItemNotFoundError extends EngineError {
    constructor(uuid: string) {
        super(`Could not find object ${uuid}`);
    }
}