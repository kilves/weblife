import {EngineError} from "./engineError";

export class CharacterNotFoundError extends EngineError {
    constructor(uuid: string) {
        super(`Could not find character ${uuid}`);
    }
}