import Vive from "./lib/vive";

const vive = new Vive({
    debug: true
});

export default vive;
