import * as avatars from "../repository/avatar";
import * as animations from "../repository/animation";
import vive from "../renderer";
import {AppContext, AppState} from "../appStack";
import Router from "koa-router";
import {BadRequestError} from "../error/badRequestError";
import {File} from "formidable";
import fs from "fs/promises";
import {getLogger} from "log4js";
import * as gltfValidator from "gltf-validator";
import {v4 as uuid} from "uuid";
import {config} from "../config";
import {getExtension} from "../util/file";
import {FieldError} from "../error/fieldError";
import {oneId} from "../repository/util";
import {Avatar} from "../model/avatar";
import {BoneSlot} from "../model/boneSlot";
import {Animation} from "../model/animation";
import {AvatarAnimation} from "../model/avatarAnimation";
import {AvatarBoneSlot} from "../model/avatarBoneSlot";
const router = new Router<AppState, AppContext>();
const logger = getLogger("avatar");
logger.level = "info";

router.get("/", async ctx => {
    if (!ctx.loginUser) {
        ctx.status = 401;
        return;
    }
    let userAvatars = await ctx.loginUser.avatars;
    if (!userAvatars) {
        ctx.status = 404;
        return;
    }
    ctx.body = await vive.render(userAvatars, "avatar");
});

router.get("/animations", async ctx => {
    const allAnimations = await animations.listAll();
    ctx.body = await vive.render(allAnimations, "animation");
});

router.get("/:id", async ctx => {
    if (!ctx.loginUser) {
        ctx.status = 401;
        return;
    }
    const id = +ctx.params.id;
    const avatar = await avatars.get(id, ctx.loginUser.id);
    if (!avatar) {
        ctx.status = 404;
        return;
    }
    ctx.body = await vive.render(avatar, "fullAvatar");
});

router.put("/:id", async ctx => {
    if (!ctx.loginUser) {
        ctx.status = 401;
        return;
    }
    const id = +ctx.params.id;
    if (!ctx.request.body.name) {
        throw new FieldError("name", "required field");
    }
    const avatar = await Avatar.findOneBy({
        id,
        loginUser: {id: ctx.loginUser.id}
    });
    if (!avatar) {
        ctx.status = 404;
        return;
    }
    avatar.name = ctx.request.body.name;
    avatar.isVrReady = Object.hasOwn(ctx.request.body, "isVrReady");
    await setAnimations(avatar, getAnimations(ctx.request.body));
    await setBoneSlots(avatar, getBones(ctx.request.body));
    await avatar.save();
    ctx.status = 201;
    ctx.body = { id: avatar.id };
});

router.post("/", async ctx => {
    if (!ctx.loginUser) {
        ctx.status = 401;
        return;
    }
    if (!ctx.request.body.name) {
        throw new FieldError("name", "required field");
    }
    const userId = ctx.loginUser.id;
    if (!ctx.request.files) {
        throw new BadRequestError();
    }
    const file = ctx.request.files.model as File;
    const data = await fs.readFile(file.filepath);
    const extension = getExtension(file.originalFilename);
    try {
        await verifyGltf(data);
        const uid = uuid();
        await fs.mkdir(`${config.resourceDir}/avatars`, {
            recursive: true
        });
        await fs.writeFile(`${config.resourceDir}/avatars/${uid}.${extension}`, data);
        const avatar = Avatar.create({
            uuid: uid,
            name: ctx.request.body.name,
            extension,
            loginUserId: userId,
            isVrReady: Object.hasOwn(ctx.request.body, "isVrReady")
        });
        await avatar.save();
        await setAnimations(avatar, getAnimations(ctx.request.body));
        await setBoneSlots(avatar, getBones(ctx.request.body));
        await avatar.save();
        ctx.status = 201;
        ctx.body = { id: avatar.id };
    } catch (e) {
        logger.error(e);
        throw new BadRequestError("Failed to create avatar");
    }
});

async function setBoneSlots(avatar: Avatar, boneSlots: {[k: number]: string}) {
    const allBoneSlots = await BoneSlot.find();
    const newSlots = allBoneSlots
        .filter(slot => Object.hasOwn(boneSlots, slot.id))
        .map(slot => AvatarBoneSlot.create({
            boneSlotId: slot.id,
            avatarId: avatar.id,
            name: boneSlots[slot.id],
        }));
    avatar.avatarBoneSlots = Promise.resolve(newSlots);
}

async function setAnimations(avatar: Avatar, animations: {[k: string]: string}) {
    const slots = await Animation.find();
    const newAnimations = slots
        .filter(animation => Object.hasOwn(animations, animation.id))
        .map(animation => AvatarAnimation.create({
            animationId: animation.id,
            avatarId: avatar.id,
            name: animations[animation.id],
        }));
    avatar.avatarAnimations = Promise.resolve(newAnimations);
}

function getAnimations(body: any): {[k: string]: string} {
    const animations: {[k: string]: string} = {};
    Object.getOwnPropertyNames(body)
        .filter(name => name.endsWith("-animation"))
        .forEach(name => {
            const id = name.replace("-animation", "");
            animations[id] = body[name];
        });
    return animations;
}

function getBones(body: any) {
    const bones: {[k: number]: string} = {};
    Object.getOwnPropertyNames(body)
        .filter(name => name.startsWith("vr_bone_slot-"))
        .forEach(name => {
            const id = +name.replace("vr_bone_slot-", "");
            bones[id] = body[name];
        });
    return bones;
}

async function verifyGltf(data: Buffer): Promise<any> {
    return new Promise<void>(async (resolve, reject) => {
        const report = await gltfValidator.validateBytes(data);
        if (report.issues.numErrors > 0) {
            logger.error(report.issues);
            return reject("Invalid gltf file");
        }
        return resolve();
    });
}

export default router;