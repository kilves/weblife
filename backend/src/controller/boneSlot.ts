import Router from "koa-router";
import {AppContext, AppState} from "../appStack";
import {BoneSlot} from "../model/boneSlot";
import vive from "../renderer";

const router = new Router<AppState, AppContext>();

router.get("/", async ctx => {
    const boneSlots = await BoneSlot.find();
    ctx.body = await vive.render(boneSlots, "boneSlot");
});

export default router;
