import Router from "koa-router";
import * as cells from "../repository/cell";
import fs from "fs/promises";
import cellObject from "./cellObject";
import vive from "../renderer";
import {AppContext, AppState} from "../appStack";
import {UnauthorizedError} from "../error/unauthorizedError";
import {v4 as uuid} from "uuid";
import {ResourceType} from "../repository/resource";
import {BadRequestError} from "../error/badRequestError";
import {config} from "../config";
import {File} from "formidable";
import server from "../server";
import {Resource} from "../model/resource";

const router = new Router<AppState, AppContext>();

router.use("/:cellName/objects", cellObject.routes());

router.get("/:name", async ctx => {
    if (!ctx.loginUser) {
        ctx.status = 401;
        return;
    }
    let name = ctx.params.name;
    let cell = await cells.get(name);
    if (!cell) {
        ctx.response.status = 404;
        return;
    }
    ctx.body = await vive.render(cell, "cell");
});

router.put("/:name", async ctx => {
    if (!ctx.loginUser) {
        throw new UnauthorizedError();
    }
    if (!ctx.request.files?.heightmap) {
        throw new BadRequestError();
    }
    const cell = await cells.get(ctx.params.name);
    if (!cell) {
        ctx.response.status = 404;
        return;
    }
    const cellResources = await cell.resources;
    const heightmaps = cellResources.filter(r => r.type === ResourceType.HEIGHTMAP) || [];
    let currentUuid = uuid()
    const heightmap = ctx.request.files.heightmap as File;
    if (heightmaps.length === 0) {
        // No heightmap resource found
        const resource = Resource.create({
            uuid: currentUuid,
            extension: "png",
            type: ResourceType.HEIGHTMAP,
            name: heightmap.originalFilename || ""
        });
        const resources = await cell.resources
        cell.resources = Promise.resolve([...resources, resource]);
        await cell.save();
    } else {
        currentUuid = heightmaps[0].uuid;
    }
    const path = `${config.resourceDir}/${currentUuid}.png`
    await fs.copyFile(heightmap.filepath, path);
    const gameRoom = await server.game.findRoom(cell.name);
    await gameRoom.loadCollision();
    ctx.response.status = 200;
    ctx.response.body = {};
});

export default router;
