import {Context, Next} from "koa";
import Router from "koa-router";
const router = new Router();
import * as users from "../repository/user";
import * as tokens from "../repository/login";
import {config} from "../config";
import {UsernameExistsError} from "../error/usernameExistsError";
import {UsernameTooShortError} from "../error/usernameTooShortError";
import {setCookie} from "../util";

router.get("/me", async ctx => {
    let res = await users.getByToken(ctx.cookies.get("token"));
    if (!res?.user) {
        ctx.status = 404;
        return;
    }
    ctx.body = {
        username: res.user.username,
        websocketToken: res.websocketToken
    };
});

router.post("/login", async ctx => {
    /*
    const hasErrors = req
        .verify()
        .body("username", "password").present()
        .body("username").length(4)
        .body("password").length(8)
        .sendErrors();
    if (hasErrors) {
        return;
    }
     */
    const user = await users.login(ctx.request.body.username, ctx.request.body.password);
    setCookie(ctx, "token", user.token);
    ctx.body = {
        username: user.username,
        websocketToken: user.websocketToken
    };
});

router.post("/register", async ctx => {
    /*
    const hasErrors = req
        .verify()
        .body("password", "passwordAgain", "email", "username").present()
        .body("username").length(4)
        .body("password").length(8)
        .body("password").equals("passwordAgain")
        .body("email").isEmail()
        .sendErrors();
    if (hasErrors) {
        return;
    }
     */
    const body = ctx.request.body
    if (body.username.length < 4) {
        throw new UsernameTooShortError("username");
    }
    const user = await users.getByUsername(body.username);
    if (user) {
        throw new UsernameExistsError("username");
    }
    if (body.username === "system") {
        ctx.status = 400;
        ctx.body = {
            username: ["banned username"]
        }
    }
    await users.create(body.username, body.password, body.email);
    ctx.body = {
        verificationRequired: config.registerVerificationRequired
    };
});

router.delete("/login", async ctx => {
    await tokens.remove(ctx.cookies.get("token"));
    ctx.status = 200;
    ctx.body = "";
});

export async function middleware(ctx: Context, next: Next) {
    console.log(ctx.cookies.get("token"));
    const token = ctx.cookies.get("token");
    const user = await users.getByToken(token);
    if (user) {
        ctx.loginUser = user.user;
    }
    await next();
}

export default router;
