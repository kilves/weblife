import Router from "koa-router";
const router = new Router<AppState, AppContext>();

import {AppContext, AppState} from "../appStack";
import {getLogger} from "log4js";
import * as users from "../repository/user";
import * as profilePictures from "../repository/profilePicture";
import {NotFoundError} from "../error/notFoundError";
import {savePng, sendFile} from "../util";
import {config} from "../config";
import {File} from "formidable";
import Jimp from "jimp";
import {v4 as uuid} from "uuid";
import {BadRequestError} from "../error/badRequestError";
import {UnauthorizedError} from "../error/unauthorizedError";
import {ServerError} from "../error/serverError";


const logger = getLogger("profiles");

router.get("/:username/profilePicture.png", async ctx => {
    const user = await users.getByUsername(ctx.params.username);
    if (!user) {
        throw new NotFoundError();
    }
    const pictures = await profilePictures.listByUser(user.id);
    const activePicture = pictures.find(p => p.active);
    if (!activePicture) {
        throw new NotFoundError();
    }
    await sendFile(ctx, `${config.resourceDir}/profilePictures/${activePicture.uuid}`);
});

router.put("/:username", async ctx => {
    const loginUser = ctx.loginUser;
    if (!loginUser) {
        throw new UnauthorizedError();
    }
    const profilePicture = ctx.request.files?.profilePicture as File;
    if (profilePicture) {
        let originalImage = null;
        try {
            originalImage = await Jimp.read(profilePicture.filepath);
        } catch (e) {
            throw new BadRequestError("Image format error");
        }
        const imageUuid = uuid();
        const createdId = await profilePictures.create({
            uuid: imageUuid,
            login_user: loginUser.id,
            extension: originalImage.getExtension()
        });
        originalImage.writeAsync(
            `${config.resourceDir}/profilePictures/${uuid}.${originalImage.getExtension()}`
        ).then(() => {
            ctx.response.body = {};
            ctx.response.status = 201;
        }).catch(async () => {
            await profilePictures.del(createdId);
            throw new ServerError();
        });
    }
});

export default router;
