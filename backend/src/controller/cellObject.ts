import Jimp from "jimp";
import {v4 as uuid} from "uuid";
import * as gltfValidator from "gltf-validator";

import Router from "koa-router";
import * as cells from "../repository/cell";
import * as resources from "../repository/resource";
import {ResourceType} from "../repository/resource";
import * as cellObjects from "../repository/cellObject";
import vive from "../renderer";
import {config} from "../config";
import {File} from "formidable";
import {AppContext, AppState} from "../appStack";
import * as fs from "fs/promises";
import server from "../server";
import {Context} from "koa";
import {UnauthorizedError} from "../error/unauthorizedError";
import {NotFoundError} from "../error/notFoundError";
import {getLogger} from "log4js";
import {getExtension} from "../util/file";
import {CellObject} from "../model/cellObject";

const router = new Router<AppState, AppContext>();

const logger = getLogger("general");

const IMAGE_MIME_WHITELIST = ["image/jpeg", "image/png", "image/tiff"];
const IMAGE_EXTENSION_WHITELIST = ["jpg", "jpeg", "png", "tif", "tiff"];
const MESH_MIME_WHITELIST = ["application/octet-stream"];
const MESH_EXTENSION_WHITELIST = ["gltf", "glb"];

export interface SavedFile {
    uuid: string;
    extension: string;
}

export interface SavedResource extends SavedFile {
    type: ResourceType;
}

router.get("/:itemId/resource", async ctx => {
    logger.info("getting resource");
    if (!ctx.loginUser) {
        ctx.status = 401;
        return;
    }
    let itemResources = await resources.listItemResources(+ctx.params.itemId);
    ctx.body = await vive.render({
        resources: itemResources
    }, "item-resources");
});

router.post("/:itemId/resource", async ctx => {
    logger.info("creating new resource");
    if (!ctx.loginUser) {
        ctx.status = 401;
        return;
    }
    if (!ctx.request.files) {
        ctx.status = 400;
        return;
    }
    const files = ctx.request.files["files[]"];
    if (!Array.isArray(files)) {
        ctx.status = 400;
        return;
    }
    const item = await cellObjects.get(+ctx.params.itemId);
    if (!item) {
        ctx.status = 400;
        return;
    }
    let resources = await saveResources(files);
    await createResources(item.id, resources);
});

// Create object
router.post("/", async ctx => {
    const body = ctx.request.body;
    const room = await getRoom(ctx);
    const gameRoom = await server.game.findRoom(room.name);
    const cellObject = CellObject.create({
        cellId: room.id,
        name: body.name,
        posX: body.x,
        posY: body.y,
        posZ: body.z
    });
    await cellObject.save();
    await cellObject.reload();
    await gameRoom.spawnItem(cellObject);
    ctx.body = "";
});

router.put("/:objectUuid/setModel/:uuid", async ctx => {
    if (!ctx.loginUser) {
        throw new UnauthorizedError();
    }
    const room = await getRoom(ctx);
    const gameRoom = await server.game.findRoom(room.name);
    const item = gameRoom.getItem(ctx.params.objectUuid);
    if (!item) {
        throw new NotFoundError();
    }

    const objects = await cellObjects.listByRoom(gameRoom.id);
    const object = objects.find(o => o.id === item.id);
    if (!object) {
        throw new NotFoundError();
    }
    const objectResources = await object.resources;
    const existingResource = objectResources.find(r => r.type === ResourceType.OBJECT3D);
    if (existingResource) {
        await cellObjects.removeResource(object.id, existingResource.id);
    }
    const resource = await resources.get(ctx.params.uuid);
    if (!resource) {
        throw new NotFoundError();
    }
    await cellObjects.addResource(object.id, resource.id);

    item.resources.push(resource);
    await gameRoom.broadcastItemUpdate(item);
    ctx.response.status = 200;
    ctx.response.body = {};
});

// Delete object
router.delete("/:uuid", async ctx => {
    const room = await getRoom(ctx);
    const gameRoom = await server.game.findRoom(room.name);
    const item = gameRoom.getItem(ctx.params.uuid)
    await cellObjects.destroy(room.id, item.id);
    gameRoom.destroyItem(ctx.params.uuid);
    ctx.body = "";
    ctx.status = 204;
});

async function getRoom(ctx: Context) {
    if (!ctx.loginUser) {
        throw new UnauthorizedError();
    }
    const cell = await cells.get(ctx.params.cellName);
    if (!cell) {
        throw new NotFoundError();
    }
    return cell;
}

/**
 * save files to disk
 * @param files
 * @returns {Promise<{uuid:string, type:string, extension: string}[]>}
 */
async function saveResources(files: File[]) {
    let resources = [];
    for (let file of files) {
        let {uuid, type, extension} = await saveFile(file);
        resources.push({uuid, type, extension});
    }
    return resources;
}

async function createResources(itemId: number, savedResources: SavedResource[]): Promise<void> {
    for (let resource of savedResources) {
        let resourceId = await resources.create(resource.uuid, null, resource.type, resource.extension);
        await cellObjects.addResource(itemId, resourceId);
    }
}

/**
 * save file to disk
 * @param file
 * @returns {Promise<{extension: string, type: string, uuid: string}>}
 */
async function saveFile(file: File): Promise<SavedResource> {
    if (isValidImage(file)) {
        let {uuid, extension} = await saveImage(file);
        return {uuid, type: ResourceType.TEXTURE, extension};
    } else if (isValidMesh(file)) {
        let {uuid, extension} = await saveMesh(file);
        return {uuid, type: ResourceType.OBJECT3D, extension};
    } else {
        throw "Invalid file in request";
    }
}

function isValidFor(file: File, extensionWhitelist: string[], mimeWhitelist: string[]) {
    let extension = getExtension(file.originalFilename);
    if (file.mimetype == null) {
        return false;
    }
    return (
        extensionWhitelist.indexOf(extension) !== -1 &&
        mimeWhitelist.indexOf(file.mimetype) !== -1
    );
}

function isValidImage(file: File) {
    return isValidFor(file, IMAGE_EXTENSION_WHITELIST, IMAGE_MIME_WHITELIST);
}

function isValidMesh(file: File) {
    return isValidFor(file, MESH_EXTENSION_WHITELIST, MESH_MIME_WHITELIST);
}

/**
 * Save image to disk
 * @param file
 * @returns {Promise<{uuid:string, extension:string}>}
 */
async function saveImage(file: File): Promise<SavedFile> {
    return new Promise(async (resolve, reject) => {
        let originalImage = await Jimp.read(file.filepath);

        new Jimp({
            data: originalImage.bitmap.data,
            width: originalImage.bitmap.width,
            height: originalImage.bitmap.height,
        }, async (err, image) => {
            if (err) {
                return reject(err);
            }
            
            let resourceUuid = uuid();
            image.write(`${config.resourceDir}/${resourceUuid}.png`);
            resolve({uuid: resourceUuid, extension: "png"});
        });
    });
}

async function saveMesh(file: File): Promise<SavedFile> {
    const buffer = await fs.readFile(file.filepath);
    await gltfValidator.validateBytes(buffer, {});
    
    let resourceUuid = uuid();
    let extension = getExtension(file.filepath);
    await fs.writeFile(`${config.resourceDir}/${resourceUuid}.${extension}`, buffer);

    return {uuid: resourceUuid, extension: extension};
}

export default router;
