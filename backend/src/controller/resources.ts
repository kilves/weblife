import Router from "koa-router";
const router = new Router<AppState, AppContext>();

import {AppContext, AppState} from "../appStack";
import {getLogger} from "log4js";
import {UnauthorizedError} from "../error/unauthorizedError";
import {BadRequestError} from "../error/badRequestError";
import {File} from "formidable";
import * as resources from "../repository/resource";
import {v4 as uuid} from "uuid";
import {ResourceType} from "../repository/resource";
import {config} from "../config";
import fs from "fs/promises";
import * as gltfValidator from "gltf-validator";
import vive from "../renderer";
import {ResourceBeingUsedError} from "../error/resourceBeingUsedError";


const logger = getLogger("mesh");

router.post("/meshes", async ctx => {
    if (!ctx.loginUser) {
        throw new UnauthorizedError();
    }
    if (!ctx.request.files || !ctx.request.files.model) {
        throw new BadRequestError();
    }
    const file = ctx.request.files.model as File;
    const buffer = await fs.readFile(file.filepath);
    const report = await gltfValidator.validateBytes(buffer);
    if (report.mimeType !== "model/gltf-binary" || report.issues.numErrors > 0) {
        logger.error("GLB file has errors", report);
        throw new BadRequestError();
    }

    const newUuid = uuid();
    const resourceId = await resources.create(newUuid, file.originalFilename, ResourceType.OBJECT3D, "glb", ctx.loginUser.id);
    const path = `${config.resourceDir}/${newUuid}.glb`
    await fs.copyFile(file.filepath, path);
    ctx.response.status = 201;
    ctx.response.body = {};
});

router.delete("/:uuid", async ctx => {
    if (!ctx.loginUser) {
        throw new UnauthorizedError();
    }
    const myResources = await resources.listUserResources(ctx.loginUser.id);
    const resourceToDelete = myResources.filter(r => r.uuid === ctx.params.uuid);
    if (!resourceToDelete) {
        throw new UnauthorizedError();
    }
    try  {
        await resources.del(ctx.params.uuid);
    } catch (e) {
        if ((e as any).code === "23503") {
            throw new ResourceBeingUsedError();
        }
        throw e;
    }
    await fs.rm(`${config.resourceDir}/${ctx.params.uuid}.glb`)
    ctx.response.status = 204;
    ctx.response.body = {};
});

router.get("/my", async ctx => {
    if (!ctx.loginUser) {
        throw new UnauthorizedError();
    }

    const userResources = await resources.listUserResources(ctx.loginUser.id);
    ctx.body = await vive.render(userResources, "resource");
});


export default router;
