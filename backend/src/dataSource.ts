import {DataSource} from "typeorm";
import {Avatar} from "./model/avatar";
import {AvatarAnimation} from "./model/avatarAnimation";
import {Animation} from "./model/animation";
import {LoginUser} from "./model/loginUser";
import {SnakeNamingStrategy} from "typeorm-naming-strategies";
import {Resource} from "./model/resource";
import {ProfilePicture} from "./model/profilePicture";
import {Login} from "./model/login";
import {Cell} from "./model/cell";
import {CellObject} from "./model/cellObject";
import {BoneSlot} from "./model/boneSlot";
import {AvatarBoneSlot} from "./model/avatarBoneSlot";

const dataSource = new DataSource({
    type: "postgres",
    host: process.env.DB_HOST,
    port: Number.parseInt(process.env.DB_PORT || "5432"),
    username: process.env.DB_USER,
    password: process.env.DB_PASS,
    database: process.env.DB_NAME,
    entities: [
        Avatar,
        AvatarAnimation,
        Animation,
        ProfilePicture,
        LoginUser,
        Resource,
        Login,
        Cell,
        CellObject,
        BoneSlot,
        AvatarBoneSlot
    ],
    namingStrategy: new SnakeNamingStrategy(),
});

await dataSource.initialize();
console.log("data source initialized", dataSource.entityMetadatas.length);