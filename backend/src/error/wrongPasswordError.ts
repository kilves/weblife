import {ServerError} from "./serverError";
import {Context} from "koa";

export class WrongPasswordError extends ServerError {
    handle(ctx: Context) {
        ctx.status = 401;
        ctx.body = "Wrong password";
    }
}