import {ServerError} from "./serverError";
import {Context} from "koa";

export class ResourceBeingUsedError extends ServerError {
    handle(ctx: Context) {
        ctx.status = 400;
        ctx.body = {
            message: "Resource already being used",
            errorId: "RESOURCE_BEING_USED"
        };
    }
}