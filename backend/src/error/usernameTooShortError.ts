import {FieldError} from "./fieldError";

export class UsernameTooShortError extends FieldError {
    public status: number = 400;
    public message: string = "Username too short";
}