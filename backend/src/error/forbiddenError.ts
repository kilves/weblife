import {ServerError} from "./serverError";
import {Context} from "koa";

export class ForbiddenError extends ServerError {
    handle(ctx: Context) {
        ctx.status = 403;
        ctx.body = "Cannot access this resource";
    }
}