import {ServerError} from "./serverError";
import {Context} from "koa";

export class BadRequestError extends ServerError {
    handle(ctx: Context) {
        ctx.status = 400;
        ctx.body = "Bad request";
    }
}