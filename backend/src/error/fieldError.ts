import {ServerError} from "./serverError";
import {Context} from "koa";

export class FieldError extends ServerError {
    public status: number = 400;
    public message: string = "";
    constructor(private field: string, message?: string) {
        super();
        if (message) {
            this.message = message;
        }
    }
    handle(ctx: Context) {
        ctx.status = this.status;
        const message = this.message;
        if (this.field) {
            ctx.body = {
                [this.field]: [message]
            };
        } else {
            ctx.body = message;
        }
    }
}