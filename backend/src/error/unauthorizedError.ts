import {ServerError} from "./serverError";
import {Context} from "koa";

export class UnauthorizedError extends ServerError {
    handle(ctx: Context) {
        ctx.status = 401;
        ctx.body = "Cannot access this resource";
    }
}