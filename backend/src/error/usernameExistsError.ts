import {FieldError} from "./fieldError";

export class UsernameExistsError extends FieldError {
    public status: number = 409;
    public message: string = "Username already exists";
}