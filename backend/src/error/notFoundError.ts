import {ServerError} from "./serverError";
import {Context} from "koa";

export class NotFoundError extends ServerError {
    handle(ctx: Context) {
        ctx.status = 404;
        ctx.body = "Not found";
    }
}