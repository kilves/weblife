import {Context} from "koa";
import {getLogger} from "log4js";

const logger = getLogger("server")

export class ServerError extends Error {
    handle(ctx: Context) {
        ctx.status = 500;
        ctx.body = "Internal server error";
        logger.error(this);
    }
}