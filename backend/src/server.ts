import GameServer from "./websocket/websocket";
import {getLogger} from "log4js";

const PORT = 8888;

const server = new GameServer({
    port: PORT
});
server.start();
const logger = getLogger("general");
logger.level = "info";
logger.info(`Game server started on port ${PORT}`);
export default server;