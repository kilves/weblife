import {ServerWebSocket} from "bun";
import GameUser from "../engine/gameUser";
import GameCell from "../engine/gameCell";
import {CommandData} from "../websocket/websocket";

export type GameWebSocket = ServerWebSocket<GameWebsocketData>

export type Command<T extends CommandData> = (ws: GameWebSocket, data: T) => Promise<void>;
export type BufferCommand = (ws: GameWebSocket, data: Buffer) => Promise<void>;

export interface GameWebsocketData {
    alive: boolean;
    user: GameUser;
    cell: GameCell;
    roomName: string;
}