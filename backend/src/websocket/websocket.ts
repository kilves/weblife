import * as cookie from "cookie";
import {config} from "../config";
import * as signedCookie from "cookie-signature";
import Game from "../engine/game";
import GameCell, {CellChanges} from "../engine/gameCell";
import GameUser, {SentMessageChannel} from "../engine/gameUser";
import {getLogger} from "log4js";
import {CharacterNotFoundError} from "../engine/error/characterNotFoundError";
import {Server, ServerWebSocket} from "bun";
import {GameWebSocket, GameWebsocketData} from "../typings/websocket";
import {Vector3} from "three";

const logger = getLogger("general");
logger.level = "info";
const socketLogger = getLogger("socket");
const cellLogger = getLogger("cell");

export interface JoinData extends CommandData {
    token: string;
    cell: string;
    avatar: number;
}

export interface MoveData extends CommandData {
    x: number;
    y: number;
}

export interface TeleportData extends CommandData {
    x: number;
    y: number;
    z: number;
}

export interface MessageData extends CommandData {
    message: {
        text: string;
        channel: SentMessageChannel;
    };
}

export interface CommandData {
    c: "join" | "move" | "say" | "jump" | "teleport"
}

export interface ItemCommandData extends CommandData {
    uuid: string
}

export interface ItemPositionData extends ItemCommandData {
    uuid: string;
    x: number;
    y: number;
    z: number;
}

export interface ItemRotationData extends ItemCommandData {
    x: number;
    y: number;
    z: number;
    w: number;
}

export interface JumpData extends CommandData {

}

export default class GameServer {

    game: Game;
    clients: Set<GameWebSocket> = new Set<GameWebSocket>();
    private timeoutId: any;
    private port: number;
    private logger = getLogger("GameServer");

    constructor(options: { port: number }) {
        this.port = options.port;
        this.game = new Game();
    }

    findClientByCharacterUuid(uuid: string): GameWebSocket {
        for (const value of this.clients) {
            if (uuid === value.data.user.character.uuid) {
                return value;
            }
        }
        throw new CharacterNotFoundError(uuid);
    }

    start() {
        this.setTimeout(20000);
        this.game.start();
        Bun.serve<GameWebsocketData>({
            async fetch(request: Request, server: Server): Promise<Response> {
                const url = new URL(request.url);
                if (server.upgrade(request, {
                    data: {
                        alive: true,
                        user: null,
                        cell: null,
                        roomName: url.pathname.replace("/ws/", "")
                    }
                })) {
                    return new Response("Upgrade", { status: 101 });
                }
                return new Response("Upgrade failed", { status: 500 });
            },
            port: this.port,
            websocket: {
                pong: (ws) => {
                    ws.data.alive = true;
                },
                open: this.onConnection.bind(this),
                close: this.onClose.bind(this),
                message: this.onMessage.bind(this)
            }
        })
    }

    async getToken(c: string) {
        let cookies = cookie.parse(c);
        return signedCookie.unsign(cookies.token.slice(2), config.secret);
    }

    async onMessage(ws: GameWebSocket, message: string | Buffer) {
        if (message instanceof Buffer) {
            const messageId = message.readUint8(0);
            if (messageId === 0x01) {
                await ws.data.cell.bufferMessages.bones(ws, message.subarray(1));
            }
        }
        try {
            let data: CommandData = JSON.parse(message as string);
            if (ws.data.cell.commands.hasOwnProperty(data.c)) {
                ws.data.cell.commands[data.c](ws, data as any).then(() => {

                }).catch((e: any) => {
                    logger.error(e);
                });
            }
        } catch(e) {
            logger.error(e);
        }
    }

    async onClose(ws: GameWebSocket) {
        socketLogger.info(`${ws.remoteAddress} disconnected`);
        ws.data.cell.disconnect(ws);
    }

    async onConnection(ws: GameWebSocket) {
        const roomName = ws.data.roomName;
        ws.data.alive = true;

        ws.data.cell = await this.game.findRoom(roomName);
        ws.data.cell.addClient(ws);
        this.clients.add(ws);

        // ws.ip = req.headers["x-forwarded-for"] || req.headers["x-real-ip"];
        ws.send(JSON.stringify({
            c: "ready"
        }));
        socketLogger.info(`${ws.remoteAddress} connected`);

    }

    setTimeout(amount: number) {
        this.timeoutId = setInterval(() => {
            for (let client of this.clients) {
                if (!client.data.alive) {
                    logger.info(`${client.remoteAddress} timed out`);
                    this.clients.delete(client);
                    return client.close(500, "Timed out");
                }
                client.data.alive = false;
                client.ping("ping");
            }
        }, amount);
    }
};
