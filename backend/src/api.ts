import Router from "koa-router";
const router = new Router({strict: true});

import user, {middleware} from "./controller/user";
import cell from "./controller/cell";
import avatar from "./controller/avatar";
import resources from "./controller/resources";
import profiles from "./controller/profiles";
import boneSlot from "./controller/boneSlot";

router.use(middleware);
router.use("/users", user.routes());
router.use("/cells", cell.routes());
router.use("/avatars", avatar.routes());
router.use("/boneSlots", boneSlot.routes());
router.use("/resources", resources.routes());
router.use("/profiles", profiles.routes());

export default router;
