import {oneId} from "./util";
import {knex} from "../db";
import {Cell} from "../model/cell";

export interface Position {
    x: number;
    y: number;
    z: number;
}

export interface Rotation {
    x: number;
    y: number;
    z: number;
    w: number;
}

export async function get(name: string): Promise<Cell | null> {
    return Cell.findOneBy({
        name
    });
}

export async function addObject(cell: Cell, position: Position, rotation: Rotation) {
    const id = oneId(await knex("cell_item").insert({
        cell: cell.id,
        pos_x: position.x,
        pos_y: position.y,
        pos_z: position.z,
        rot_x: rotation.x,
        rot_y: rotation.y,
        rot_z: rotation.z,
        rot_w: rotation.w
    }).returning("id"));
    return id;
}
