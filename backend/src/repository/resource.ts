import {oneId} from "./util";
import {knex} from "../db";
import {resolveObjectURL} from "buffer";
import user from "../controller/user";
import {Resource} from "../model/resource";
import {Cell} from "../model/cell";


export enum ResourceType {
    HEIGHTMAP = "HEIGHTMAP",
    TEXTURE = "TEXTURE",
    OBJECT3D = "OBJECT"
}

export async function get(uuid: string): Promise<Resource | null> {
    return Resource.findOneBy({
        uuid
    });
}

export async function del(uuid: string) {
    return Resource.delete({
        uuid
    });
}

export async function listCellResources(cellId: number): Promise<Resource[]> {
    const cell = await Cell.findOneBy({
        id: cellId
    });
    if (!cell) {
        console.warn(`No cell found by id ${cellId}`);
        return [];
    }
    return cell.resources;
}

export async function listItemResources(itemId: number): Promise<Resource[]> {
    return knex("item_resource")
        .where("item", itemId)
        .join("resource", "resource.id", "item_resource.resource")
        .select("resource.*");
}

export async function create(uuid: string, name: string|null, type: string, extension: string, userId: number|null = null): Promise<number> {

    const res = Resource.create({
        uuid: uuid,
        type: type,
        extension: extension,
        name: name,
        loginUserId: userId
    });
    return res.id;
}

export function listUserResources(userId: number): Promise<Resource[]>  {
    return knex("resource")
        .where("login_user_id", userId)
        .select("id", "uuid", "extension", "type", "name");
}

export async function update(resource: Resource) {
    await knex("resource").update({
        uuid: resource.uuid,
        type: resource.type,
        extension: resource.extension,
        name: resource.name
    }).where("id", resource.id);
}