import {knex} from "../db";

export interface CellItem {
    id?: number;
    cell?: number;
    item?: number;
    pos_x?: number;
    pos_y?: number;
    pos_z?: number;
    rot_x?: number;
    rot_y?: number;
    rot_z?: number;
    rot_w?: number;
}

export async function update(id?: number, item?: CellItem) {
    await knex("cell_object").update(item).where("id", id);
}
