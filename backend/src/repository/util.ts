export function oneId(ids: any[]) {
    if (ids.length === 0) {
        throw "Resource was not saved";
    }
    return ids[0].id;
}

export function oneField(returnings: any[], fieldName: string) {
    if (returnings.length === 0) {
        throw "Resource was not saved";
    }
    return returnings[0][fieldName];
}
