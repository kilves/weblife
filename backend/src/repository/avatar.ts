import {knex} from "../db";
import {Avatar} from "../model/avatar";
import {LoginUser} from "../model/loginUser";

export interface AvatarAnimation {
    animation: string,
    name: string;
}

export async function addAnimation(avatarId: number, id: string, name: string) {
    return knex("avatar_animation").insert({
        avatar: avatarId,
        animation: id,
        name
    });
}

export async function get(id: number, loginUserId?: number): Promise<Avatar | null> {
    if (!id) {
        return null;
    }
    const avatar = await Avatar.findOneBy({
        id,
    });
    if (!avatar) {
        return null;
    }
    if (loginUserId && avatar.loginUserId !== loginUserId) {
        return null;
    }
    return avatar;
}

export async function create(userId: number, uuid: string, name: string, extension: string) {
    return knex("avatar")
        .insert({
            login_user: userId,
            name, uuid, extension
        })
        .returning("id");
}