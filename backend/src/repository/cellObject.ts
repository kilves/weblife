import {knex} from "../db";
import {oneId} from "./util";
import {Position} from "./cell";
import {CellObject} from "../model/cellObject";

export enum ItemType {
    TEXTURE = "TEXTURE",
    OBJECT = "OBJECT",
    SOUND = "SOUND"
}

export async function listByRoom(cellId: number) {
    return CellObject.findBy({
        cellId
    });
}

export async function create(cellId: number, name: string, position: Position) {
    let ids = await knex("cell_object").insert({
        cell: cellId,
        name: name,
        pos_x: position.x,
        pos_y: position.y,
        pos_z: position.z
    }).returning("id");
    return oneId(ids);
}

export async function destroy(cellId: number, id: number) {

    await knex("cell_object_resource").where({
        cell_object: id
    }).delete();

    await knex("cell_object").where({
        cell: cellId,
        id: id
    }).delete();
}

export async function removeResource(objectId: number, resourceId?: number) {
    await knex("cell_object_resource")
        .where("cell_object", objectId)
        .andWhere("resource", resourceId)
        .delete();
}

export async function addResource(objectId: number, resourceId?: number) {
    let ids = await knex("cell_object_resource").insert({
        cell_object: objectId,
        resource: resourceId
    }).returning("id");
    return oneId(ids);
}

export async function get(id: number) {
    return CellObject.findOneBy({
        id
    });
}
