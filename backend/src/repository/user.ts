import {randomBytes} from "crypto";
import {config} from "../config";
import {UnauthorizedError} from "../error/unauthorizedError";
import {WrongPasswordError} from "../error/wrongPasswordError";
import {LoginUser} from "../model/loginUser";
import {Login} from "../model/login";
import {MoreThan} from "typeorm";
import {addDays} from "date-fns";

export interface LoginResult {
    username: string;
    token: string;
    websocketToken: string;
}

export async function login(username: string, password: string): Promise<LoginResult> {
    const user = await LoginUser.findOneBy({
        username: username
    });
    if (!user) {
        throw new WrongPasswordError();
    }
    let match = await Bun.password.verify(password, user.password, "bcrypt");
    if (!match) {
        throw new WrongPasswordError();
    }
    let token = getNewToken();
    const websocketToken = getNewToken();
    const login = Login.create({
        token,
        websocketToken,
        expires: addDays(new Date(), 1),
        loginUserId: user.id
    });
    await login.save();
    return {
        username: username,
        token: token,
        websocketToken: websocketToken
    };
}

/**
 * Get user based on not expired token
 * @param token
 * @returns promise that resolves to user, if found.
 */
export async function getByToken(token: string | undefined) {
    if (!token) {
        return null;
    }
    const login = await Login.findOneBy({
        token: token,
        expires: MoreThan(new Date())
    });

    if (!login) {
        return null;
    }
    return {
        user: await login.loginUser,
        websocketToken: login.websocketToken
    };
}

export async function getById(id: number) {
    return LoginUser.findOneBy({
        id
    });
}

export async function getByWebsocketToken(token: string | undefined): Promise<LoginUser | undefined> {
    const login = await Login.findOneBy({
        websocketToken: token,
        expires: MoreThan(new Date())
    });
    return login?.loginUser;
}

export async function getIfAuthorized(token: string | undefined) {
    if (token === undefined) {
        throw new UnauthorizedError();
    }
    const user = await getByToken(token);
    if (!user?.user) {
        throw new UnauthorizedError();
    }
    return user.user;
}

export async function getByWebsocketTokenIfAuthorized(websocketToken: string): Promise<LoginUser> {
    if (websocketToken === undefined) {
        throw new UnauthorizedError();
    }
    const user = await getByWebsocketToken(websocketToken);
    if (!user) {
        throw new UnauthorizedError();
    }
    return user;
}

export async function getByUsername(username: string): Promise<LoginUser | null> {
    return LoginUser.findOneBy({
        username
    });
}

export async function create(username: string, password: string, email: string) {
    const user = await LoginUser.insert({
        username,
        password: await Bun.password.hash(password, "bcrypt"),
        email,
        verified: !config.registerVerificationRequired
    });
}

function getNewToken() {
    let bytes = randomBytes(64);
    return bytes.toString("base64");
}
