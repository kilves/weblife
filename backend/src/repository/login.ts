import {knex} from "../db";

export async function remove(token: string | undefined) {
    if (token === undefined) {
        throw "No token given";
    }
    return knex("login").where("token", token).delete();
}
