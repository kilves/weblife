
import {knex} from "../db";
import {oneId} from "./util";


export interface ProfilePicture {
    id: number;
    uuid: string;
    active: boolean;
    extension: string;
}

export interface ProfilePictureCreate {
    uuid: string;
    login_user: number;
    extension: string;
}

export async function listByUser(userId: number): Promise<ProfilePicture[]> {
    return knex("profile_picture")
        .select("id", "uuid", "active", "extension")
        .where("login_user_id", userId);
}

export async function deactivateAllOfUser(userId: number) {
    return knex("profile_picture")
        .update({
            active: false
        })
        .where("login_user_id", userId);
}

export async function create(profilePicture: ProfilePictureCreate) {
    const ret = await knex("profile_picture")
        .insert({
            uuid: profilePicture.uuid,
            login_user: profilePicture.login_user,
            extension: profilePicture.extension,
            active: true,
        })
        .returning("id");
    return oneId(ret);
}

export async function del(id: number) {
    return knex("profile_picture")
        .delete()
        .where("id", id);
}