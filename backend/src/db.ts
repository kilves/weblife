import {config} from "./config";
const pg = require("pg");
pg.types.setTypeParser(1700, parseFloat);
pg.types.setTypeParser(20, parseInt);
import k from "knex";

export const knex =  k(config.database);
