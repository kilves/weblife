import {LoginUser} from "./model/loginUser";

export interface AppContext {
    loginUser?: LoginUser;
}

export interface AppState {

}