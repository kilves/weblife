export interface DatabaseConnection {
    host?: string;
    user?: string;
    password?: string;
    database?: string;
}

export interface DatabaseConfig {
    client: string;
    connection: DatabaseConnection;
}

export interface Config {
    secret: string;
    database: DatabaseConfig;
    registerVerificationRequired?: boolean;
    resourceDir: string;
}

const CONFIG_STRING_TRUTHY = ["yes", "1", "true"];
const CONFIG_STRING_FALSY = ["no", "0", "false"];

function parseBoolean(value: string | undefined): boolean {
    if (!value) {
        return false;
    }
    const lower = value.toLowerCase();
    if (CONFIG_STRING_TRUTHY.indexOf(lower) !== -1) {
        return true;
    } else if (CONFIG_STRING_FALSY.indexOf(lower) !== -1) {
        return false;
    }
    return false;
}

function parsePath(path?: string) {
    if (!path) {
        return "";
    }
    return path;
}

export const config: Config = {
    secret: "secret",
    database: {
        client: "pg",
        connection: {
            host: process.env.PGHOST,
            user: process.env.PGUSER,
            password: process.env.PGPASSWORD,
            database: process.env.PGDATABASE
        }
    },
    registerVerificationRequired: parseBoolean(process.env.REGISTER_VERIFICATION_REQUIRED),
    resourceDir: parsePath(process.env.RESOURCE_DIRECTORY)
};
