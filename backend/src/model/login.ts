import {BaseEntity, Column, Entity, ManyToOne, PrimaryGeneratedColumn} from "typeorm";
import {LoginUser} from "./loginUser";

@Entity()
export class Login extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column("text")
    token: string;

    @Column("text")
    websocketToken: string;

    @Column("timestamp")
    expires: Date;

    @ManyToOne(type => LoginUser, loginUser => loginUser.logins, {
        lazy: true
    })
    loginUser: Promise<LoginUser>;

    @Column("integer")
    loginUserId: number;
}