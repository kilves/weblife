import {BaseEntity, Column, Entity, OneToMany, PrimaryGeneratedColumn} from "typeorm";
import {Avatar} from "./avatar";
import {Resource} from "./resource";
import {Login} from "./login";
import {ProfilePicture} from "./profilePicture";

@Entity()
export class LoginUser extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column("text")
    username: string;

    @Column("text")
    password: string;

    @Column("boolean")
    verified: boolean;

    @Column("text")
    email: string;

    @OneToMany(type => Avatar, avatar => avatar.loginUser, {
        lazy: true
    })
    avatars: Promise<Avatar[]>;

    @OneToMany(type => Resource, resource => resource.loginUser, {
        lazy: true
    })
    resources: Promise<Resource[]>;

    @OneToMany(type => Login, login => login.loginUser, {
        lazy: true,
        cascade: true
    })
    logins: Promise<Login[]>;

    @OneToMany(type => ProfilePicture, profilePicture => profilePicture.loginUser, {
        lazy: true
    })
    profilePictures: Promise<ProfilePicture[]>;
}