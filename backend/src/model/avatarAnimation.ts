import {BaseEntity, Column, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryColumn, PrimaryGeneratedColumn} from "typeorm";
import {Avatar} from "./avatar";
import {Animation} from "./animation";

@Entity()
export class AvatarAnimation extends BaseEntity {
    @Column("text")
    name: string;

    @ManyToOne(type => Avatar, avatar => avatar.avatarAnimations, {
        lazy: true
    })
    avatar: Promise<Avatar>;

    @ManyToOne(type => Animation, animation => animation.avatarAnimations, {
        lazy: true
    })
    animation: Promise<Animation>;

    @PrimaryColumn("text")
    animationId: string;

    @PrimaryColumn("number")
    avatarId: number;
}