import {BaseEntity, Column, Entity, JoinTable, ManyToMany, OneToMany, PrimaryGeneratedColumn} from "typeorm";
import {Avatar} from "./avatar";
import {AvatarBoneSlot} from "./avatarBoneSlot";

@Entity()
export class BoneSlot extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column("text")
    name: string;

    @OneToMany(type => AvatarBoneSlot, boneSlot => boneSlot.avatar, {
        lazy: true
    })
    avatarBoneSlots: Promise<AvatarBoneSlot[]>;
}