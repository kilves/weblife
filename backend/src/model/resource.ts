import {BaseEntity, Column, Entity, JoinTable, ManyToMany, ManyToOne, PrimaryGeneratedColumn} from "typeorm";
import {LoginUser} from "./loginUser";
import {CellObject} from "./cellObject";
import {Cell} from "./cell";

@Entity()
export class Resource extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column("text")
    uuid: string;

    @Column("text")
    extension: string;

    @Column("text")
    type: string;

    @Column("text")
    name: string|null;

    @ManyToOne(type => LoginUser, loginUser => loginUser.resources, {
        lazy: true
    })
    loginUser: Promise<LoginUser>;

    @Column("integer")
    loginUserId: number|null;

    @ManyToMany(type => CellObject, cellObject => cellObject.resources, {
        lazy: true
    })
    @JoinTable({
        name: "cell_object_resource"
    })
    cellObjects: Promise<CellObject[]>;

    @ManyToMany(type => Cell, cell => cell.resources, {
        lazy: true
    })
    @JoinTable({
        name: "cell_resource"
    })
    cells: Promise<Cell[]>;
}