import {BaseEntity, Column, Entity, JoinTable, ManyToMany, ManyToOne, PrimaryGeneratedColumn} from "typeorm";
import {ColumnNumericTransformer} from "../util";
import {Cell} from "./cell";
import {Resource} from "./resource";

@Entity()
export class CellObject extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column("text")
    name: string;

    @Column("numeric", {
        transformer: new ColumnNumericTransformer()
    })
    posX: number;

    @Column("numeric", {
        transformer: new ColumnNumericTransformer()
    })
    posY: number;

    @Column("numeric", {
        transformer: new ColumnNumericTransformer()
    })
    posZ: number;

    @Column("numeric", {
        transformer: new ColumnNumericTransformer()
    })
    rotX: number;

    @Column("numeric", {
        transformer: new ColumnNumericTransformer()
    })
    rotY: number;

    @Column("numeric", {
        transformer: new ColumnNumericTransformer()
    })
    rotZ: number;

    @Column("numeric", {
        transformer: new ColumnNumericTransformer()
    })
    rotW: number;

    @Column("timestamp")
    createdAt: Date;

    @ManyToOne(type => Cell, cell => cell.objects, {
        lazy: true
    })
    cell: Promise<Cell>;

    @Column("int")
    cellId: number;

    @ManyToMany(type => Resource, resource => resource.cellObjects, {
        lazy: true
    })
    @JoinTable({
        name: "cell_object_resource",
    })
    resources: Promise<Resource[]>;
}