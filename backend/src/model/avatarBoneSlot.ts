import {BaseEntity, Column, Entity, ManyToOne, PrimaryColumn} from "typeorm";
import {Avatar} from "./avatar";
import {BoneSlot} from "./boneSlot";

@Entity()
export class AvatarBoneSlot extends BaseEntity {
    @Column("text")
    name: string;

    @ManyToOne(type => Avatar, avatar => avatar.avatarBoneSlots, {
        lazy: true
    })
    avatar: Promise<Avatar>;

    @ManyToOne(type => BoneSlot, boneSlot => boneSlot.avatarBoneSlots, {
        lazy: true
    })
    boneSlot: Promise<BoneSlot>;

    @PrimaryColumn("integer")
    boneSlotId: number;

    @PrimaryColumn("integer")
    avatarId: number;
}