import {BaseEntity, Column, Entity, JoinTable, ManyToMany, ManyToOne, OneToMany, PrimaryGeneratedColumn} from "typeorm";
import {LoginUser} from "./loginUser";
import {AvatarAnimation} from "./avatarAnimation";
import {BoneSlot} from "./boneSlot";
import {AvatarBoneSlot} from "./avatarBoneSlot";

@Entity()
export class Avatar extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column("text")
    name: string;

    @Column("text")
    uuid: string;

    @Column("text")
    extension: string;

    @Column("boolean")
    isVrReady: boolean = false;

    @ManyToOne(type => LoginUser, loginUser => loginUser.avatars, {
        lazy: true,
    })
    loginUser: Promise<LoginUser>;

    @Column("integer")
    loginUserId: number;

    @OneToMany(type => AvatarAnimation, avatarAnimation => avatarAnimation.avatar, {
        lazy: true,
        cascade: ["insert", "update", "remove"]
    })
    avatarAnimations: Promise<AvatarAnimation[]>;

    @OneToMany(type => AvatarBoneSlot, avatarBoneSlot => avatarBoneSlot.avatar, {
        lazy: true,
        cascade: ["insert", "update", "remove"]
    })
    avatarBoneSlots: Promise<AvatarBoneSlot[]>;

}