import {BaseEntity, Column, Entity, ManyToOne, PrimaryGeneratedColumn} from "typeorm";
import {LoginUser} from "./loginUser";

@Entity()
export class ProfilePicture extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(type => LoginUser, loginUser => loginUser.profilePictures, {
        lazy: true
    })
    loginUser: Promise<LoginUser>;

    @Column("text")
    uuid: string;

    @Column("boolean")
    active: boolean;

    @Column("text")
    extension: string;
}