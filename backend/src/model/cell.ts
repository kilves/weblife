import {BaseEntity, Column, Entity, JoinTable, ManyToMany, OneToMany, PrimaryGeneratedColumn} from "typeorm";
import {CellObject} from "./cellObject";
import {Resource} from "./resource";


@Entity()
export class Cell extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column("text")
    name: string;

    @OneToMany(type => CellObject, cellObject => cellObject.cell, {
        lazy: true
    })
    objects: Promise<CellObject[]>;

    @ManyToMany(type => Resource, resource => resource.cells, {
        lazy: true
    })
    @JoinTable({
        name: "cell_resource"
    })
    resources: Promise<Resource[]>;
}