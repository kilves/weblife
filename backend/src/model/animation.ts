import {BaseEntity, Column, Entity, OneToMany, PrimaryColumn} from "typeorm";
import {AvatarAnimation} from "./avatarAnimation";

@Entity()
export class Animation extends BaseEntity {
    @PrimaryColumn("text")
    id: string;

    @Column("text")
    name: string;

    @OneToMany(type => AvatarAnimation, avatarAnimation => avatarAnimation.animation, {
        lazy: true
    })
    avatarAnimations: Promise<AvatarAnimation[]>;
}