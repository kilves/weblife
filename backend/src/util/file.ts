export function getExtension(name: string | null) {
    if (name == null) {
        return "";
    }
    let nameLowercase = name.toLowerCase();
    let nameParts = nameLowercase.split(".");
    return nameParts[nameParts.length - 1];
}