import {view as v} from "../lib/vive";

export default {
    id: "id",
    name: "name",
    animations: v("avatarAnimations", "avatarAnimation"),
    boneSlots: v("avatarBoneSlots", "avatarBoneSlot")
};