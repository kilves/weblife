import {view as v} from "../lib/vive";

export default {
    resources: v("resources", "resource")
};
