import {view as v} from "../lib/vive";

export default {
    items: v("items", "item")
};
