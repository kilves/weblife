import {Moment} from "moment";
import send from "koa-send";
import {Context, ParameterizedContext} from "koa";
import {File} from "formidable";
import Jimp from "jimp";

export function setCookie(ctx: Context, name: string, value: string) {
    ctx.cookies.set(name, value, { maxAge: 86400000, signed: true, sameSite: "strict" });
}

export async function sendFile(ctx: ParameterizedContext, path: string, opts?: send.SendOptions): Promise<string> {
    return send(ctx, path, {
        ...opts,
        root: "/"
    });
}

export class ColumnNumericTransformer {
    to(data: number): number {
        return data;
    }
    from(data: string): number {
        return parseFloat(data);
    }
}


export async function savePng(file: File, target: string): Promise<void> {
    return new Promise(async (resolve, reject) => {
        let originalImage = await Jimp.read(file.filepath);

        new Jimp({
            data: originalImage.bitmap.data,
            width: originalImage.bitmap.width,
            height: originalImage.bitmap.height,
        }, async (err, image) => {
            if (err) {
                return reject(err);
            }

            image.write(`${target}.png`);
            resolve();
        });
    });
}

export function knexTime(m: Moment) {
    return m.format("YYY-MM-DD HH:mm:ss");
}
