import {NextFunction, Request, Response} from "express";

export interface Errors {
    [containerName: string]: {
        [key: string]: string[];
    };
}

export interface BodyVerifier {
    present(): Verifier;
    length(length: number): Verifier;
    equals(other: string): Verifier;
    isEmail(): Verifier;
}

export interface Verifier {
    body(...fields: string[]): BodyVerifier;
    errors(): Errors
    sendErrors(): boolean;
}

declare module "express-serve-static-core" {
    interface Request {
        verify(): Verifier;
    }
}

function _verifier(req: Request, res: Response, next: NextFunction) {

    const errors: Errors = {}

    function addError(containerName: string, field: string, message: string) {
        if (!errors[containerName]) {
            errors[containerName] = {}
        }
        if (!errors[containerName][field]) {
            errors[containerName][field] = [];
        }
        errors[containerName][field].push(message);
    }

    const verifier: Verifier = {
        body(...fields: string[]) {
            const containerName = "body";
            return {
                present(): Verifier {
                    for (let field of fields) {
                        const value = req[containerName][field];
                        if (!value) {
                            addError(containerName, field, "This field is required");
                        }
                    }
                    return verifier;
                },
                length(length: number): Verifier {
                    for (let field of fields) {
                        const value = req[containerName][field];
                        if (value && value.length < length) {
                            addError(containerName, field, `Must be at least ${length} characters`);
                        }
                    }
                    return verifier;
                },
                equals(other: string): Verifier {
                    const otherValue = req[containerName][other];
                    for (let field of fields) {
                        const value = req[containerName][field];
                        let allFieldsPassed = true;
                        if (value !== otherValue) {
                            addError(containerName, field, "Fields don't match");
                            allFieldsPassed = false;
                        }
                        if (!allFieldsPassed) {
                            addError(containerName, other, "Fields don't match");
                        }
                    }
                    return verifier;
                },
                isEmail(): Verifier {
                    for (let field of fields) {
                        const value = req[containerName][field]
                        const match = value.match(/(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/);
                        if (!match) {
                            addError(containerName, field, "Not a valid email");
                        }
                    }
                    return verifier;
                }
            }
        },
        sendErrors(): boolean {
            const names = Object.getOwnPropertyNames(errors);
            if (names.length > 0) {
                res.status(400).send(errors);
                return true;
            }
            return false;
        },
        errors() {
            return errors;
        }
    }
    req.verify = () => {
        return verifier;
    };
    next();
}

export default function () {
    return _verifier;
}