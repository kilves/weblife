import * as path from "path";
import * as fs from "fs";
import {getLogger} from "log4js";
import knex from "knex";
import QueryBuilder = knex.QueryBuilder;
import {ServerError} from "../error/serverError";
import {BaseEntity} from "typeorm";

const logger = getLogger("vive");

export interface ViveOptions {
    viewDirectory?: string;
    viewsPath?: string;
    debug?: boolean;
}

interface Views {
    [key: string]: any;
}

interface InternalOptions {
    viewDirectory: string;
    debug: boolean;
    viewsPath: string;
    views: Views;
}

export default class Vive {

    options: InternalOptions;

    constructor(options: ViveOptions) {
        let opt = this.getOptions(options);

        let dirname = path.dirname(import.meta.dir);
        let viewsPath = path.join(dirname, opt.viewDirectory);
        opt.viewsPath = viewsPath;

        let files = fs.readdirSync(viewsPath);

        let views: Views = {};
        for (let file of files) {
            if (file.endsWith(".ts")) {
                let viewName = file.replace(".ts", "");
                let viewPath = path.join(viewsPath, viewName);
                views[viewName] = require(viewPath).default;
            }
        }
        opt.views = views;
        this.options = opt;
    }

    async render(model: any, viewName: string) {
        let view = this.options.views[viewName];
        if (!["Array", "Object"].includes(model.constructor.name) && !(model instanceof BaseEntity)) {
            throw new ServerError(`vive only supports arrays and objects, not ${model.constructor.name}`);
        }
        logger.debug(`Rendering ${viewName}`);
        if (!view) {
            logger.error(`No ${viewName}.ts found from ${this.options.viewsPath}`);
            return;
        }

        let viewPropertyNames = Object.getOwnPropertyNames(view);
        if (Array.isArray(model)) {
            let elements = [];
            for (const m of model) {
                elements.push(await this.getViewElement(m, view, viewPropertyNames))
            }
            return elements;
        } else {
            return this.getViewElement(model, view, viewPropertyNames);
        }
    }

    private async getViewElement(model: any, view: any, viewPropertyNames: string[]) {
        let result: any = {};
        for (let viewPropertyName of viewPropertyNames) {
            let viewAction = view[viewPropertyName];
            let viewActionType = typeof viewAction;
            if (viewActionType === "string") {
                result[viewPropertyName] = await model[viewAction];
            } else if (viewActionType === "function") {
                result[viewPropertyName] = await viewAction(model, this);
            } else {
                logger.error(`Failed to parse viewNameHere.${viewPropertyName} - expected string, function or vive helper, got ${viewActionType}`);
            }
        }
        return result;
    }

    getOptions(options: ViveOptions): InternalOptions {
        return {
            ...options,
            viewDirectory: options.viewDirectory ? options.viewDirectory : "view",
            debug: options.debug ? options.debug : false,
            views: [],
            viewsPath: "",
        };
    }

    /*
    static view(modelPropertyName: string, viewName: string) {

        this.array = function(v, array, viewName) {
            return array.map(element => v.render(element, viewName));
        };

        this.single = function(v, model, viewName) {
            return model => v.render(model, viewName);
        };

        return (model, v) => {
            let modelProperty = model[modelPropertyName];
            if (Array.isArray(modelProperty)) {
                return this.array(v, modelProperty, viewName);
            } else {
                return this.single(v, modelProperty, viewName);
            }
        };
    }; */
}

async function array(v: Vive, array: any[], viewName: string) {
    let ary = [];
    for (const e of array) {
        ary.push(await v.render(e, viewName));
    }
    return ary;
}

function single(v: Vive, model: any, viewName: string) {
    return (model: any) => v.render(model, viewName);
}

export function view(modelPropertyName: string, viewName: string) {
    return async (model: any, v: Vive) => {
        let modelProperty = await model[modelPropertyName];
        if (Array.isArray(modelProperty)) {
            return array(v, modelProperty, viewName);
        } else {
            return single(v, modelProperty, viewName);
        }
    };
}
